<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */



// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'northeastern-student-life');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'd=a^hi!o6>:p*mi}7vF)n+EmuJsH=R}.CmI}Q^[!O:s9bAJ#%h~.zk+)F[eI|6_w');
define('SECURE_AUTH_KEY',  'nV%`u1kNn$a04y{J4`YmbyppHK&htq3,[L@3Q(Ca)IV9Lvic<Ugb,<3i4SWW81]F');
define('LOGGED_IN_KEY',    '$QUlew4NWVpmt`ws9=lT _sIuc$Z<@vsbnG/AkE__g3E1Bjl@mEmgEgY<O*[Bb]v');
define('NONCE_KEY',        '@*b,V*19sYPp^W(ctr)l:5-d 9|!F9VsAn[(M/I`c-k`M03#g^rr&tv7o2lnKvPR');
define('AUTH_SALT',        '++IfMt)?&-X,j+!T{vaH7}B=hxd_cv}.+cCWuY;&d9;>6o.QA*pAm-7L1%/Mjci8');
define('SECURE_AUTH_SALT', ':|8(i-f>3m.VO}=,IFr0$.=jf?AU4Uy Dn$UxVt!S/*>GMSft1a${srtMDjhfj;^');
define('LOGGED_IN_SALT',   'k<|X]yfWHRLJh!1Tw`3&BA&77:v tfm5AVaKqUbMzJ{||E`L`[l^j!07XO/:Vswx');
define('NONCE_SALT',       ';8P!R3`ziTBWdO#Xv^BZVPVRp/TJU5l]Pum^_6qM`-<%eq=gK{[8W:UYs{AH.$u4');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
ini_set('display_errors','On');

define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', true);
define('DOMAIN_CURRENT_SITE', 'northeastern.dev');
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
