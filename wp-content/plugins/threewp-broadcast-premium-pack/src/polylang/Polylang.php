<?php

namespace threewp_broadcast\premium_pack\polylang;

/**
	@brief				Zupport for the Frédéric Demarle's <a href="https://wordpress.org/plugins/polylang/">Polylang</a> translation plugin.
	@plugin_group		3rd party compatability
	@since				2014-11-12 19:51:37
**/
class Polylang
	extends \threewp_broadcast\premium_pack\base
{
	public function _construct()
	{
		$this->add_filter( 'threewp_broadcast_broadcasting_finished' );
		$this->add_filter( 'threewp_broadcast_broadcasting_started' );
		$this->add_action( 'threewp_broadcast_broadcasting_after_switch_to_blog' );
		$this->add_action( 'threewp_broadcast_broadcasting_before_restore_current_blog' );

		$this->add_action( 'threewp_broadcast_menu' );

		// Broadcast handles this at 5, so we have to do it before BC.
		$this->add_action( 'threewp_broadcast_wp_update_term', 2 );
	}

	public function action_check( $action )
	{
		if ( ! $this->has_polylang() )
			return false;

		if ( ! isset( $action->broadcasting_data->polylang ) )
			return false;

		return true;
	}

	/**
		@brief		Convenience method to clear polylang's language cache.
		@since		2015-04-14 17:17:27
	**/
	public function clear_polylang_languages_cache()
	{
		global $polylang;
		if ( ! $polylang )
			return;
		// Else it thinks we have the languages from the previous blog.
		$polylang->model->clean_languages_cache();
	}

	/**
		@brief		Return a list of languages on this blog.
		@since		2014-11-12 21:39:36
	**/
	public function get_languages_list()
	{
		global $polylang;
		return $polylang->model->get_languages_list();
	}

	/**
		@brief		Returns the translations of a post.
		@details	The PL API has save, but not get.
		@since		2014-11-12 20:16:05
	**/
	public function get_translations( $post_id )
	{
		return PLL()->model->post->get_translations( $post_id );
	}

	/**
		@brief		Is Polylang available?
		@since		2014-11-12 19:58:42
	**/
	public function has_polylang()
	{
		return defined( 'POLYLANG_VERSION' );
	}

	/**
		@brief		menu_settings
		@since		2016-12-13 15:47:29
	**/
	public function menu_settings()
	{
		$form = $this->form();
		$r = '';

		$no_language_action = $form->select( 'no_language_action' )
			// Input description
			->description( __( "What to do when the child blog does not have the post's language.", 'threewp_broadcast' ) )
			// Input label
			->label( __( 'No language action', 'threewp_broadcast' ) )
			// Select option for when there is no language on the blog
			->option( __( "Broadcast the post in the blog's default language", 'threewp_broadcast' ), 'default_language' )
			// Select option for when there is no language on the blog
			->option( __( "Do not broadcast to the blog", 'threewp_broadcast' ), '' )
			->value( $this->get_site_option( 'no_language_action' ) );

		$save = $form->primary_button( 'save' )
			->value( __( 'Save settings', 'threewp_broadcast' ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();

			$value = $no_language_action->get_post_value();
			$this->update_site_option( 'no_language_action', $value );

			$r .= $this->info_message_box()->_( __( 'Options saved!', 'threewp_broadcast' ) );
		}

		$r .= $form->open_tag();
		$r .= $form->display_form_table();
		$r .= $form->close_tag();

		// Settings page header
		echo $this->wrap( $r, __( 'Settings', 'threewp_broadcast' ) );
	}

	public function site_options()
	{
		return array_merge( [
			'no_language_action' => '',
		], parent::site_options() );
	}

	/**
		@brief		Is the language available on this blog?
		@since		2014-11-12 19:55:49
	**/
	public function threewp_broadcast_broadcasting_after_switch_to_blog( $action )
	{
		if ( ! $this->action_check( $action ) )
			return;

		$this->clear_polylang_languages_cache();

		$bcd = $action->broadcasting_data;
		$pll = $bcd->polylang;

		// Assume no translations for this blog.
		$pll->child_translations = [];

		// Does this blog have the language?
		$found = false;
		foreach( $this->get_languages_list() as $language )
			$found |= ( $language->slug == $pll->language );

		if ( ! $found )
		{
			$no_language_action = $this->get_site_option( 'no_language_action' );
			switch ( $no_language_action )
			{
				case 'default_language':
					$pll->no_language_action = $no_language_action;
					// Do not broadcast taxonomies since they will all be in the parent post's language.
					$pll->displaced_taxonomies = $bcd->parent_post_taxonomies;
					$bcd->parent_post_taxonomies = [];
					$pll->old_pref_lang = PLL()->pref_lang;
					PLL()->pref_lang = pll_default_language();
					return;
				case '':
					$action->broadcast_here = false;
			}
		}

		if ( ! $action->broadcast_here )
			return $this->debug( 'This blog does not have language %s enabled.', $pll->language );

		// Assume no child translations.
		$pll->child_translations = [];

		if ( $action->broadcasting_data->link )
		{
			$child_id = $action->broadcasting_data->broadcast_data->get_linked_post_on_this_blog();
			if ( ! $child_id )
				return $this->debug( 'No child on this blog.' );
			// Save these translations for later when we set the new child IDs, if any.
			$pll->child_translations = pll_get_post_translations( $child_id );
		}

		$this->debug( 'Current child translations: %s', $pll->child_translations );
	}

	/**
		@brief		Set the translations of this post.
		@details	This is a two step process.

					1. Manually clear the current listed of translations from the term.
					2. Add the current language to the translated children.

					#1 because the term is synced from the parent blog, and it contains links to the wrong posts.

		@since		2014-11-12 19:53:51
	**/
	public function threewp_broadcast_broadcasting_before_restore_current_blog( $action )
	{
		if ( ! $this->action_check( $action ) )
			return;

		$bcd = $action->broadcasting_data;
		$pll = $bcd->polylang;

		if ( isset( $pll->no_language_action ) )
		{
			// Broadcast as normal, but use the blog default language.
			if ( $pll->no_language_action == 'default_language' )
				$post_language = pll_default_language();
		}
		else
			$post_language = $pll->language;

		$this->debug( 'Post langugage will be: %s', $post_language );

		// Clear the description manually, which is where the translation array is contained.
		$terms = wp_get_object_terms( $pll->child_translations, 'post_translations' );
		foreach( $terms as $term )
			wp_update_term( $term->term_id, 'post_translations', [ 'description' => serialize( [] ) ] );

		$post_id = $action->broadcasting_data->new_post()->ID;

		$pll->child_translations[ $post_language ] = $post_id;

		$languages = $this->get_languages_list();
		foreach( $languages as $language )
			foreach( $pll->translations as $translation_language => $translation_post_id )
				// Only set languages that are active on this blog.
				if ( $translation_language == $language->slug )
				{
					$child_bcd = $pll->bcds[ $translation_post_id ];
					$linked_child = $child_bcd->get_linked_post_on_this_blog();
					if ( $linked_child )
						$pll->child_translations[ $translation_language ] = $linked_child;
				}

		$this->debug( 'Saving post translations: %s', $pll->child_translations );
		// save_post_translations wants post_id => lang_name, which is cute because get_post_translations gives it to us the other way.
		array_flip( $pll->child_translations );
		pll_save_post_translations( $pll->child_translations );

		if ( isset( $pll->no_language_action ) )
		{
			unset( $pll->no_language_action );

			// Maybe the next blog will have the language, so put everything back as it was.
			$bcd->parent_post_taxonomies = $pll->displaced_taxonomies;
			unset( $pll->displaced_taxonomies );

			// Restore the pref lang.
			PLL()->pref_lang = $pll->old_pref_lang;
			unset( $pll->old_pref_lang );
		}
	}

	/**
		@brief		threewp_broadcast_broadcasting_finished
		@since		2015-04-14 17:16:44
	**/
	public function threewp_broadcast_broadcasting_finished( $action )
	{
		$this->clear_polylang_languages_cache();
	}

	/**
		@brief		threewp_broadcast_broadcasting_started
		@since		2014-11-12 19:54:17
	**/
	public function threewp_broadcast_broadcasting_started( $action )
	{
		if ( ! $this->has_polylang() )
			return;

		$bcd = $action->broadcasting_data;

		$post = $bcd->post;
		$translated = pll_is_translated_post_type( $post->post_type );

		if ( ! $translated )
			return;

		$pll = (object)[];
		$bcd->polylang = $pll;

		$pll->language = pll_get_post_language( $post->ID );
		$pll->translations = $this->get_translations( $post->ID );

		global $polylang;
		// Preferred language term ID is incorrect, so get the slug.
		if ( is_object( $polylang->pref_lang ) )
			$polylang->pref_lang = $pll->language;

		// We need to load all of the broadcast datas for each translation.
		$blog_id = get_current_blog_id();
		$pll->bcds = [];
		foreach( $pll->translations as $translated_post_id )
			$pll->bcds[ $translated_post_id ] = ThreeWP_Broadcast()->get_post_broadcast_data( $blog_id, $translated_post_id );
	}

	/**
		@brief		Add ourselves into the menu.
		@since		2016-01-26 14:00:24
	**/
	public function threewp_broadcast_menu( $action )
	{
		if ( ! is_super_admin() )
			return;

		$action->menu_page
			->submenu( 'threewp_broadcast_polylang' )
			->callback_this( 'menu_settings' )
			// Menu item name
			->menu_title( 'Polylang' )
			// Page title
			->page_title( 'Polylang Broadcast' );
	}

	/**
		@brief		If this a language, don't change the description, which contains the exact language and flag info.
		@since		2016-04-13 16:22:28
	**/
	public function threewp_broadcast_wp_update_term( $action )
	{
		if ( $action->taxonomy != 'language' )
			return;
		$this->debug( 'Keeping old language data: %s', $action->old_term->description );
		$action->new_term->description = $action->old_term->description;
	}
}
