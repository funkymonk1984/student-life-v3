<?php

namespace threewp_broadcast\premium_pack\cdn_workaround;

/**
	@brief			Work around faulty CDNs that do not report the correct URL for attachments.
	@plugin_group	Utilities
	@since			2015-11-17 19:37:25
**/
class CDN_Workaround
	extends \threewp_broadcast\premium_pack\base
{
	public function _construct()
	{
		$this->add_filter( 'get_attached_file', 10, 2 );
	}

	/**
		@brief		Maybe override the filename during broadcast.
		@since		2015-11-17 19:31:35
	**/
	public function get_attached_file( $filename, $attachment_id )
	{
		// Only modify the filename if we are broadcasting.
		if ( ! ThreeWP_Broadcast()->is_broadcasting() )
			return $filename;

		// Only modify if the file doesn't exist.
		if ( file_exists( $filename ) )
		{
			ThreeWP_Broadcast()->debug( 'File %s exists', $filename);
			return $filename;
		}

		// Filename doesn't exist, we need its guid.
		$attachment = get_post( $attachment_id );
		$url = $attachment->guid;
		ThreeWP_Broadcast()->debug( 'Overriding path %s with %s', $filename, $url );

		return $url;
	}
}
