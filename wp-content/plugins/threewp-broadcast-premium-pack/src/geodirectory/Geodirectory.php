<?php

namespace threewp_broadcast\premium_pack\geodirectory;

use \gcb;

/**
	@brief			Support for <a href="https://wordpress.org/plugins/geodirectory/">Geodirectory</a> places.
	@plugin_group	3rd party compatability
	@since			2015-05-20 20:27:30
**/
class Geodirectory
	extends \threewp_broadcast\premium_pack\base
{
	use \threewp_broadcast\premium_pack\classes\database_trait;

	/**
		@brief		Are we ready to broadcast the place?
		@details	This is used in conjunction with threewp_broadcast_broadcast_post and geodir_after_save_listing. Only after the geodir is executed is this set to true, therefore allowing Broadcast to send out the post.
		@since		2015-07-29 21:35:55
	**/
	public $ready_for_place = false;

	public function _construct()
	{
		$this->add_action( 'geodir_after_save_listing' );
		$this->add_action( 'threewp_broadcast_broadcasting_after_switch_to_blog' );
		$this->add_action( 'threewp_broadcast_broadcasting_started' );
		$this->add_action( 'threewp_broadcast_broadcasting_before_restore_current_blog' );
		$this->add_action( 'threewp_broadcast_get_post_types' );
		$this->add_action( 'threewp_broadcast_broadcast_post', 5 );
	}

	/**
		@brief		threewp_broadcast_broadcast_post
		@since		2015-07-29 21:20:59
	**/
	public function threewp_broadcast_broadcast_post( $broadcasting_data )
	{
		// We only want to block places.
		if ( ! in_array( $broadcasting_data->post->post_type, $this->get_post_types() ) )
			return $broadcasting_data;

		// And we only block places if we are not yet allowed to broadcast.
		if ( ! $this->ready_for_place )
		{
			$this->debug( 'Not ready for place broadcast.' );
			return false;
		}

		// We are ready.
		return $broadcasting_data;
	}

	/**
		@brief		Decide whether we should broadcast this post to this blog.
		@since		2015-08-01 17:28:59
	**/
	public function threewp_broadcast_broadcasting_after_switch_to_blog( $action )
	{
		if ( ! isset( $action->broadcasting_data->geodirectory ) )
			return;

		// Only do this check if the post location is set.
		$post_location = $action->broadcasting_data->geodirectory->get( 'post_location', false );
		if ( ! $post_location )
			return $this->debug( 'No post location set.' );

		$action->broadcasting_data->geodirectory->forget( 'new_location_id' );

		// Try to find whether this post has the same post location.
		$query = sprintf( "SELECT * FROM `%s` WHERE `country_slug` = '%s' AND `region_slug` = '%s' AND `city_slug` = '%s'",
			$this->gd_table( 'post_locations' ),
			$post_location[ 'country_slug' ],
			$post_location[ 'region_slug' ],
			$post_location[ 'city_slug' ]
		);
		$this->debug( 'Looking for the corresponding post location: %s', $query );
		$results = $this->query( $query );
		$this->debug( 'Found post location: %s', $results );
		if ( count( $results ) < 1 )
		{
			$action->broadcast_here = false;
			return $this->debug( 'The same location was not found. Skipping this blog.' );
		}
		$new_location = reset( $results );
		$action->broadcasting_data->geodirectory->set( 'new_location_id', $new_location[ 'location_id' ] );
		$this->debug( 'The new location ID is: %s', $new_location[ 'location_id' ] );
	}

	/**
		@brief		threewp_broadcast_broadcasting_before_restore_current_blog
		@since		2015-05-20 20:50:00
	**/
	public function threewp_broadcast_broadcasting_before_restore_current_blog( $action )
	{
		$bcd = $action->broadcasting_data;

		if ( ! isset( $bcd->geodirectory ) )
			return;

		$this->restore_attachments( $bcd );
		$this->restore_custom_fields( $bcd );
		$this->restore_event_schedule( $bcd );
		$this->restore_place_detail( $bcd );
		$this->restore_post_categories( $bcd );
		$this->restore_post_icons( $bcd );
		$this->restore_tax_meta( $bcd );
	}

	/**
		@brief		threewp_broadcast_broadcasting_started
		@since		2015-07-28 16:11:20
	**/
	public function threewp_broadcast_broadcasting_started( $action )
	{
		if ( ! $this->has_geodirectory() )
			return;

		$bcd = $action->broadcasting_data;
		$bcd->geodirectory = ThreeWP_Broadcast()->collection();

		$this->save_attachments( $bcd );
		$this->save_custom_fields( $bcd );
		$this->save_event_schedule( $bcd );
		$this->save_place_detail( $bcd );
		$this->save_post_icons( $bcd );
		$this->save_tax_meta( $bcd );
	}

	/**
		@brief		Add the gd_place custom post type.
		@since		2015-07-28 16:24:59
	**/
	public function threewp_broadcast_get_post_types( $action )
	{
		// Add support for custom post types.
		$types = $this->get_post_types();
		$action->post_types += $types;
	}

	// -------------------------------------------------------------------------------------------------------------------
	// --- SAVE (even though S comes after R, it is more logical for save to come first.
	// -------------------------------------------------------------------------------------------------------------------

	/**
		@brief		Save all of the attachments.
		@since		2015-07-29 16:30:48
	**/
	public function save_attachments( $bcd )
	{
		global $wpdb;

		// GeoDirectory has a nice function that retrieves the images of a place.
		$images = geodir_get_images( $bcd->post->ID );
		$this->debug( 'Images are: %s', $images );

		// But it does not contain the necessary attachment ID, so we have to find each attachment separately.
		foreach( (array)$images as $image )
			$bcd->geodirectory->collection( 'images' )->append( $image );

		// The attachment order is specified in the attachments table.
		$query = sprintf( "SELECT * FROM `%s` WHERE `post_id` = '%s'", $this->gd_table( 'attachments' ), $bcd->post->ID );
		$this->debug( 'Retrieving attachments: %s', $query );
		$results = $this->query( $query );
		$results = $this->delete_array_key( $results, 'ID' );
		$this->debug( 'Found attachments: %s', $results );
		$bcd->geodirectory->set( 'attachments', $results );
	}

	/**
		@brief		Save the custom fields.
		@since		2015-07-30 21:41:36
	**/
	public function save_custom_fields( $bcd )
	{
		$query = sprintf( "SELECT * FROM `%s` WHERE `post_type` = '%s'", $this->gd_table( 'custom_fields' ), $bcd->post->post_type );
		$this->debug( 'Retrieving custom fields: %s', $query );
		$results = $this->query( $query );
		$results = $this->delete_array_key( $results, 'id' );
		$this->debug( 'Found custom fields: %s', $results );
		$bcd->geodirectory->set( 'custom_fields', $results );
	}

	/**
		@brief		Save the event schedule.
		@since		2015-08-01 16:40:15
	**/
	public function save_event_schedule( $bcd )
	{
		$query = sprintf( "SELECT * FROM `%s` WHERE `event_id` = '%s'", $this->gd_table( 'event_schedule' ), $bcd->post->ID );
		$this->debug( 'Retrieving event schedule: %s', $query );
		$results = $this->query( $query );
		$results = $this->delete_array_key( $results, 'schedule_id' );
		$this->debug( 'Found event schedule: %s', $results );
		$bcd->geodirectory->set( 'event_schedule', $results );
	}

	/**
		@brief		Search for the location ID.
		@since		2015-08-01 17:09:04
	**/
	public function save_location_id( $bcd, $results )
	{
		$this->debug( 'Looking for the post location ID.' );
		$location_id = false;
		foreach( $results as $result )
		{
			if ( isset( $result[ 'post_location_id' ] ) )
				$location_id = $result[ 'post_location_id' ];
		}

		$this->debug( 'The post location ID is: %s', $location_id );

		if ( $location_id !== false )
		{
			$bcd->geodirectory->set( 'location_id', $location_id );
			// Extract and save the location itself.
			$query = sprintf( "SELECT * FROM `%s` WHERE `location_id` = '%s'", $this->gd_table( 'post_locations' ), $location_id );
			$results = $this->query( $query );
			$post_location = reset( $results );
			$this->debug( 'Saving post location: %s, %s', $query, $post_location );
			$bcd->geodirectory->set( 'post_location', $post_location );
		}
	}

	/**
		@brief		Save the row from the place_detail table.
		@since		2015-07-29 15:47:46
	**/
	public function save_place_detail( $bcd )
	{
		$query = sprintf( "SELECT * FROM `%s` WHERE `post_id` = '%s'", $this->gd_table( $bcd->post->post_type . '_detail' ), $bcd->post->ID );
		$this->debug( 'Retrieving place_detail: %s', $query );
		$results = $this->query( $query );
		$results = $this->delete_array_key( $results, 'id' );
		$this->save_location_id( $bcd, $results );
		$result = reset( $results );
		$this->debug( 'Found place detail: %s', $result );
		$bcd->geodirectory->set( 'place_detail', $result );
	}

	/**
		@brief		Save the post icons.
		@since		2015-07-28 16:27:45
	**/
	public function save_post_icons( $bcd )
	{
		// Extract all of the icons uses.
		$query = sprintf( "SELECT * FROM `%s` WHERE `post_id` = '%s'", $this->gd_table( 'post_icon' ), $bcd->post->ID );
		$this->debug( 'Retrieving post icons: %s', $query );
		$results = $this->query( $query );
		$this->debug( 'Post icons found: %s', $results );
		$results = $this->delete_array_key( $results, 'id' );
		$bcd->geodirectory->set( 'post_icons', $results );
	}

	/**
		@brief		Save the taxonomy meta data.
		@since		2015-07-28 20:45:53
	**/
	public function save_tax_meta( $bcd )
	{
		$gd = $bcd->geodirectory;

		$gd_taxonomies = [ 'gd_place_tags', 'gd_placecategory' ];

		// Find all place tax metas.
		foreach( $gd_taxonomies as $taxonomy )
		{
			if ( ! isset( $bcd->parent_post_taxonomies[ $taxonomy ] ) )
				continue;

			foreach( $bcd->parent_post_taxonomies[ $taxonomy ] as $gd_term )
			{
				$term_id = $gd_term->term_id;
				$meta_key = sprintf( 'tax_meta_gd_place_%s', $term_id );
				$meta_value = get_option( $meta_key, true );
				if ( ! is_array( $meta_value ) )
				{
					$this->debug( 'No tax meta for term %s', $term_id );
					continue;
				}

				// We need to copy that icon data.
				foreach( [ 'ct_cat_default_img', 'ct_cat_icon' ] as $image_type )
					if ( isset( $meta_value[ $image_type ] ) )
						$bcd->add_attachment( $meta_value[ $image_type ][ 'id' ] );

				// And now save the term.
				$this->debug( 'Saving term data for term %s, %s', $term_id, $meta_value );
				$gd->collection( 'tax_meta' )->set( $term_id, $meta_value );
			}
		}
	}

	// -------------------------------------------------------------------------------------------------------------------
	// --- RESTORE
	// -------------------------------------------------------------------------------------------------------------------

	/**
		@brief		Restore the attachments.
		@since		2015-07-29 16:41:55
	**/
	public function restore_attachments( $bcd )
	{
		global $wpdb;
		$new_post_id = $bcd->new_post( 'ID' );
		$wp_upload_dir = wp_upload_dir();

		$table = $this->gd_table( 'attachments' );
		$this->database_table_must_exist( $table );

		// Delete the current images from disk. Unfortunately, we cannot use geodir_get_images because it just don't work when switching blogs.
		$query = sprintf( "SELECT * FROM `%s` WHERE `post_id` = '%s'", $table, $new_post_id );
		$this->debug( 'Retrieving current images: %s', $query );
		$results = $this->query( $query );
		$this->debug( 'Current images: %s', $results );
		foreach( $results as $result )
		{
			$file = $wp_upload_dir[ 'basedir' ] . $result[ 'file' ];
			unlink( $file );
			$this->debug( 'Deleting %s', $file );
		}

		// And now delete them from the database.
		$query = sprintf( "DELETE FROM `%s` WHERE `post_id` = '%s'", $table, $new_post_id );
		$this->debug( 'Deleting current attachments: %s', $query );
		$this->query( $query );

		$images = $bcd->geodirectory->collection( 'images' );

		// And put the new images in.
		$attachments = $bcd->geodirectory->get( 'attachments' );
		foreach( $attachments as $attachment )
		{
			$file = $attachment[ 'file' ];
			// Find the corresponding image
			foreach( $images as $image )
			{
				if ( strpos( $image->src, $file ) === false )
					continue;

				// We have found the image. Get the new path.
				$new_filename = $image->file;
				$new_filename = str_replace( $bcd->post->ID . '_', $new_post_id . '_', $new_filename );
				$target = $wp_upload_dir[ 'path' ] . '/' . $new_filename;
				copy( $image->path, $target );
				$this->debug( 'Filesizes after copy - %s: %s  %s : %s',
					$image->path,
					filesize( $image->path ),
					$target,
					filesize( $target )
				);

				// Update the attachment data with the new path.
				$attachment[ 'post_id' ] = $new_post_id;
				$attachment[ 'file' ] = $wp_upload_dir[ 'subdir' ] . '/' . $new_filename;
				$this->debug( 'Inserting attachment %s', $attachment );
				$wpdb->insert( $table, $attachment );
			}
		}
	}

	/**
		@brief		Restore the custom fields.
		@since		2015-07-30 21:43:28
	**/
	public function restore_custom_fields( $bcd )
	{
		// Delete the current fields.
		$query = sprintf( "DELETE FROM `%s` WHERE `post_type` = '%s'", $this->gd_table( 'custom_fields' ), $bcd->post->post_type );
		$this->debug( 'Deleting current custom fields: %s', $query );
		$this->query( $query );

		// And reinsert them all.
		global $wpdb;
		foreach( $bcd->geodirectory->get( 'custom_fields' ) as $custom_field )
		{
			$this->debug( 'Reinserting custom field: %s', $custom_field );
			$wpdb->insert( $this->gd_table( 'custom_fields' ), $custom_field );

			$column_name = $custom_field[ 'htmlvar_name' ];
			$this->debug( 'Maybe adding detail column %s', $column_name );
			geodir_add_column_if_not_exist( $this->gd_table( $bcd->post->post_type . '_detail' ), $column_name );
		}
	}

	/**
		@brief		Restore the event schedule.
		@since		2015-08-01 16:40:50
	**/
	public function restore_event_schedule( $bcd )
	{
		$new_post_id = $bcd->new_post( 'ID' );

		$query = sprintf( "DELETE FROM `%s` WHERE `event_id` = '%s'", $this->gd_table( 'event_schedule' ), $new_post_id );
		$this->debug( 'Deleting current event_schedule: %s', $query );
		$this->query( $query );

		global $wpdb;
		foreach( $bcd->geodirectory->get( 'event_schedule' ) as $event_schedule )
		{
			$event_schedule[ 'event_id' ] = $new_post_id;
			$this->debug( 'Inserting new event schedule: %s', $event_schedule );
			$wpdb->insert( $this->gd_table( 'event_schedule' ), $event_schedule );
		}
	}

	/**
		@brief		Restore the place detail row in the table.
		@since		2015-07-29 15:52:47
	**/
	public function restore_place_detail( $bcd )
	{
		$table = $this->gd_table( $bcd->post->post_type . '_detail' );
		$this->database_table_must_exist( $table );

		$new_post_id = $bcd->new_post( 'ID' );

		$query = sprintf( "DELETE FROM `%s` WHERE `post_id` = '%s'", $table, $new_post_id );
		$this->debug( 'Deleting current place detail: %s', $query );
		$this->query( $query );

		$place_detail = $bcd->geodirectory->get( 'place_detail' );

		if ( ! $place_detail )
			return $this->debug( 'No place detail.' );

		if ( isset( $place_detail[ 'post_id' ] ) )
			$place_detail[ 'post_id' ] = $new_post_id;


		if ( isset( $place_detail[ 'marker_json' ] ) )
			// Data duplication: Each post icon has its own marker also.
			$place_detail[ 'marker_json' ] = $this->fix_marker_json( $bcd, $place_detail[ 'marker_json' ] );

		if ( isset( $place_detail[ 'default_category' ] ) )
			$place_detail[ 'default_category' ] = $bcd->terms()->get( $place_detail[ 'default_category' ] );

		if ( isset( $place_detail[ 'gd_placecategory' ] ) )
		{
			// Data duplication. They are normal taxonomies also.
			$categories = explode( ',', $place_detail[ 'gd_placecategory' ] );
			foreach( $categories as $index => $category )
				$categories[ $index ] = $bcd->terms()->get( $category );
			$place_detail[ 'gd_placecategory' ] = implode( ',', $categories );
		}

		// Handle multiple locations.
		$new_post_location_id = $bcd->geodirectory->get( 'new_location_id', 0 );
		if ( $new_post_location_id > 0 )
			$place_detail[ 'post_location_id' ] = $new_post_location_id;

		// Fix the featured image.
		if ( isset( $place_detail[ 'featured_image' ] ) )
		{
			if ( $bcd->has_thumbnail )
			{
				$new_thumbnail_id = $bcd->copied_attachments()->get( $bcd->thumbnail_id );
				$attachment = wp_get_attachment_metadata( $new_thumbnail_id );
				// GeoDirectory's featured image requires a forward slash in the beginning.
				$place_detail[ 'featured_image' ] = '/' . $attachment[ 'file' ];
			}
		}

		$this->debug( 'Inserting new place detail: %s', $place_detail );
		global $wpdb;
		$wpdb->insert( $table, $place_detail );
	}

	/**
		@brief		Restore the post categories.
		@since		2015-07-29 15:03:45
	**/
	public function restore_post_categories( $bcd )
	{
		if ( ! isset( $bcd->post_custom_fields[ 'post_categories' ] ) )
			return $this->debug( 'No post categories.' );
		$post_categories = $bcd->post_custom_fields[ 'post_categories' ];
		$post_categories = reset( $post_categories );

		$post_categories = maybe_unserialize( $post_categories );
		if ( ! is_array( $post_categories ) )
			return $this->debug( 'Post categories custom field is not an array.' );

		if ( ! isset( $post_categories[ 'gd_placecategory' ] ) )
			return $this->debug( 'Post categories custom field does not contain placecategories.' );

		$post_categories = $post_categories[ 'gd_placecategory' ];
		$post_categories = explode( '#', $post_categories );
		foreach( $post_categories as $index => $category )
		{
			// Another split from 140,y,d to extract the 140.
			$parts = explode( ',', $category );
			$new_term_id = $bcd->terms()->get( $parts[ 0 ] );
			if ( $new_term_id < 1 )
			{
				$this->debug( 'Unable to get an equivalent term for %s', $parts[ 0 ] );
				continue;
			}
			$parts[ 0 ] = $new_term_id;
			$category = implode( ',', $parts );
			$post_categories[ $index ] = $category;
		}
		$post_categories[ 'gd_placecategory' ] = implode( '#', $post_categories );

		$this->debug( 'Updating post categories with: %s', $post_categories );
		update_post_meta( $bcd->new_post( 'ID' ), 'post_categories', $post_categories );
	}

	/**
		@brief		Restore the post icons.
		@since		2015-07-28 16:27:29
	**/
	public function restore_post_icons( $bcd )
	{
		$table = $this->gd_table( 'post_icon' );
		$this->database_table_must_exist( $table );

		global $wpdb;
		$new_post_id = $bcd->new_post( 'ID' );
		// Delete the current icons.
		$query = sprintf( "DELETE FROM `%s` WHERE `post_id` = '%s'", $table, $new_post_id );
		$this->debug( 'Deleting current post icons: %s', $query );
		$this->query( $query );

		// And insert the new ones.
		foreach( $bcd->geodirectory->get( 'post_icons' ) as $post_icon )
		{
			$new_term_id = $bcd->terms()->get( $post_icon[ 'cat_id' ] );

			// Correct the post ID.
			$post_icon[ 'post_id' ] = $new_post_id;
			// And get the equivalent term.
			$post_icon[ 'cat_id' ] = $new_term_id;

			$post_icon[ 'json' ] = $this->fix_marker_json( $bcd, $post_icon[ 'json' ] );

			$this->debug( 'Inserting new post icon data: %s', $post_icon );
			$wpdb->insert( $table, $post_icon );
		}
	}

	/**
		@brief		Restore the tax meta.
		@since		2015-07-28 20:56:06
	**/
	public function restore_tax_meta( $bcd )
	{
		$gd = $bcd->geodirectory;

		if ( ! $gd->has( 'tax_meta' ) )
			return;
		foreach( $gd->get( 'tax_meta' ) as $old_term_id => $meta_value )
		{
			$new_term_id = $bcd->terms()->get( $old_term_id );
			$meta_key = sprintf( 'tax_meta_gd_place_%s', $new_term_id );

			// Fix the images, if necessary.
			foreach( [ 'ct_cat_default_img', 'ct_cat_icon' ] as $image_type )
				if ( isset( $meta_value[ $image_type ] ) )
				{
					$old_image_id = $meta_value[ $image_type ][ 'id' ];
					$new_image_id = $bcd->copied_attachments()->get( $old_image_id );

					$this->debug( 'Equivalent icon for %s is %s.', $old_image_id, $new_image_id );
					$meta_value[ $image_type ][ 'id' ] = $new_image_id;
					$meta_value[ $image_type ][ 'src' ] = wp_get_attachment_url( $new_image_id );
				}

			$this->debug( 'Updating tax meta for %s: %s', $meta_key, $meta_value );
			update_option( $meta_key, $meta_value );
		}
	}

	// -------------------------------------------------------------------------------------------------------------------
	// --- MISC is always last.
	// -------------------------------------------------------------------------------------------------------------------

	/**
		@brief		Delete a key from all elements in the array.
		@since		2015-07-28 20:10:45
	**/
	public function delete_array_key( $array, $key )
	{
		foreach( $array as $index => $a )
			if ( isset( $array[ $index ][ $key ] ) )
				unset( $array[ $index ][ $key ] );
		return $array;
	}

	/**
		@brief		Fix the json object ID and cats.
		@since		2015-07-29 16:04:12
	**/
	public function fix_marker_json( $bcd, $json )
	{
		$new_post_id = $bcd->new_post( 'ID' );

		$json = json_decode( $json );
		$json->id = $new_post_id;

		// The marker is made of postid_catid.
		$marker = explode( '_', $json->marker_id );

		$new_term_id = $bcd->terms()->get( $marker[ 1 ] );

		$marker[ 0 ] = $new_post_id;
		$marker[ 1 ] = $new_term_id;

		// Reassemble marker.
		$json->marker_id = $new_post_id . '_' . $new_term_id;

		$json->group = 'catgroup' . $new_term_id;

		$json = json_encode( $json );

		$json = stripslashes( $json );

		return $json;
	}

	/**
		@brief		geodir_after_save_listing
		@since		2015-07-29 21:17:28
	**/
	public function geodir_after_save_listing( $post_id )
	{
		$this->debug( 'GeoDirectory is finished with place #%s. Now it is our turn to broadcast it.', $post_id );

		$this->ready_for_place = true;

		ThreeWP_Broadcast()->save_post( $post_id );
		$this->ready_for_place = false;
	}

	/**
		@brief		Returns the complete Geodir table.
		@since		2015-07-28 20:06:15
	**/
	public function gd_table( $type )
	{
		global $wpdb;
		return sprintf( '%sgeodir_%s', $wpdb->prefix, $type );
	}

	/**
		@brief		Return the array of post types that Geodirectory and the CPT plugin uses.
		@since		2015-08-01 13:32:34
	**/
	public function get_post_types()
	{
		$types = get_option( 'geodir_custom_post_types', true );
		if ( ! is_array( $types ) )
			$types = [];
		$types[ 'gd_place' ] = 'gd_place';
		$types[ 'gd_event' ] = 'gd_event';
		return $types;
	}

	/**
		@brief		Is Geodirectory installed?
		@since		2015-07-28 16:10:51
	**/
	public function has_geodirectory()
	{
		return defined( 'GEODIRECTORY_VERSION' );
	}
}
