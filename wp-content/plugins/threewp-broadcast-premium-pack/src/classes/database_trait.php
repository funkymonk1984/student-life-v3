<?php

namespace threewp_broadcast\premium_pack\classes;

/**
	@brief		Database handling methods.
	@since		2017-04-23 14:13:52
**/
trait database_trait
{
	/**
		@brief		Check for the existence of a database table.
		@since		2017-04-23 14:14:02
	**/
	public function database_table_must_exist( $table_name )
	{
		global $wpdb;
		$query = sprintf( "DESCRIBE `%s`", $table_name );
		$results = $wpdb->get_results( $query );

		if ( count( $results ) < 1 )
			wp_die( sprintf( "Broadcast fatal error! The table <em>%s</em> does not exist on this blog, even though it should (%s)", $table_name, get_called_class() ) );
	}
}
