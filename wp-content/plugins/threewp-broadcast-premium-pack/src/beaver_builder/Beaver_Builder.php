<?php

namespace threewp_broadcast\premium_pack\beaver_builder;

/**
	@brief			Adds support for the <a href="https://www.wpbeaverbuilder.com/">Beaver Builder page builder plugin</a>.
	@plugin_group	3rd party compatability
	@since			2016-10-25 18:57:26
**/
class Beaver_Builder
	extends \threewp_broadcast\premium_pack\base
{
	use \threewp_broadcast\premium_pack\classes\copy_options_trait;

	public function _construct()
	{
		$this->add_action( 'fl_builder_after_save_layout' );
		$this->add_action( 'threewp_broadcast_menu' );
	}

	/**
		@brief		Update the children after saving the layout.
		@since		2016-10-25 18:58:28
	**/
	public function fl_builder_after_save_layout( $post_id )
	{
		ThreeWP_Broadcast()->api()->update_children( $post_id, [] );
	}

	/**
		@brief		Return an array of the options to copy.
		@since		2017-05-01 22:48:56
	**/
	public function get_options_to_copy()
	{
		return [
			'_fl_builder_*',
		];
	}

	/**
		@brief		show_copy_options
		@since		2017-05-01 22:47:16
	**/
	public function show_copy_settings()
	{
		echo $this->generic_copy_options_page( [
			'plugin_name' => 'Beaver Builder',
		] );
	}

	/**
		@brief		Add ourselves into the menu.
		@since		2016-01-26 14:00:24
	**/
	public function threewp_broadcast_menu( $action )
	{
		if ( ! is_super_admin() )
			return;

		$action->menu_page
			->submenu( 'threewp_broadcast_beaver_builder' )
			->callback_this( 'show_copy_settings' )
			->menu_title( 'Beaver Builder' )
			->page_title( 'Beaver Builder Broadcast' );
	}
}
