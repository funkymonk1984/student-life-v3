<?php

namespace threewp_broadcast\premium_pack\yoast_seo;

/**
	@brief				Adds support for the <a href="https://wordpress.org/plugins/wordpress-seo/">Yoast SEO</a> plugin.
	@plugin_group		3rd party compatability
	@since				2016-08-10 21:22:10
**/
class Yoast_SEO
	extends \threewp_broadcast\premium_pack\base
{
	public function _construct()
	{
		$this->add_action( 'threewp_broadcast_collect_post_type_taxonomies' );
		$this->add_action( 'threewp_broadcast_menu' );
		$this->add_action( 'threewp_broadcast_wp_update_term' );
	}

	// --------------------------------------------------------------------------------------------
	// ----------------------------------------- Callbacks
	// --------------------------------------------------------------------------------------------

	/**
		@brief		Plugin settings.
		@since		2016-10-30 15:35:24
	**/
	public function settings()
	{
		$form = $this->form2();
		$r = '';

		$keep_canonical_input = $form->checkbox( 'keep_canonical' )
			->checked( $this->get_site_option( 'keep_canonical' ) )
			// Input title
			->description( __( "When broadcasting, keep the existing taxonomy canonical URL on the child blog and prevent it from being overwritten with the value from the parent blog.", 'threewp_broadcast' ) )
			// Input label
			->label( __( 'Keep child taxonomy canonical URL', 'threewp_broadcast' ) );

		$save = $form->primary_button( 'save' )
			// Button
			->value( __( 'Save settings', 'threewp_broadcast' ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();

			$value = $keep_canonical_input->is_checked();
			$this->update_site_option( 'keep_canonical', $value );

			$r .= $this->info_message_box()->_( __( 'Options saved!', 'threewp_broadcast' ) );
		}


		$r .= $form->open_tag();
		$r .= $form->display_form_table();
		$r .= $form->close_tag();

		// Page heading
		echo $this->wrap( $r, __( 'Yoast SEO settings', 'threewp_broadcast' ) );
	}

	/**
		@brief		Site options.
		@since		2016-10-30 15:37:22
	**/
	public function site_options()
	{
		return array_merge( [
			'keep_canonical' => false,
		], parent::site_options() );
	}

	/**
		@brief		Looks like we're going to sync taxonomies. Take note of all yoast meta, if any.
		@since		2016-07-19 20:55:17
	**/
	public function threewp_broadcast_collect_post_type_taxonomies( $action )
	{
		$bcd = $action->broadcasting_data;

		if ( ! isset( $bcd->yoast_seo ) )
			$bcd->yoast_seo = ThreeWP_Broadcast()->collection();

		$meta = get_option( 'wpseo_taxonomy_meta', true );
		$meta = maybe_unserialize( $meta );

		$this->debug( 'Yoast SEO meta is %s', $meta );
		foreach( $bcd->parent_post_taxonomies as $parent_post_taxonomy => $terms )
		{
			$this->debug( 'Collecting Yoast SEO meta fields for %s', $parent_post_taxonomy );
			if ( ! isset( $meta[ $parent_post_taxonomy ] ) )
				continue;
			// Get all of the fields for all terms
			foreach( $terms as $term )
			{
				$term_id = $term->term_id;		// Conv.
				if ( ! isset( $meta[ $parent_post_taxonomy ][ $term_id ] ) )
					continue;

				$the_meta = $meta[ $parent_post_taxonomy ][ $term_id ];
				$this->debug( 'Saving meta %s for term %s (%s)', $the_meta, $term->slug, $term_id );
				$bcd->yoast_seo->set( $term_id, $the_meta );
			}
		}
	}

	/**
		@brief		Add ourselves into the menu.
		@since		2016-01-26 14:00:24
	**/
	public function threewp_broadcast_menu( $action )
	{
		if ( ! is_super_admin() )
			return;

		$action->menu_page
			->submenu( 'threewp_broadcast_yoast_seo' )
			->callback_this( 'settings' )
			->menu_title( 'Yoast SEO' )
			->page_title( 'Yoast SEO' );
	}

	/**
		@brief		Updating the term.
		@since		2016-07-22 16:36:28
	**/
	public function threewp_broadcast_wp_update_term( $action )
	{
		$bcd = $action->broadcasting_data;

		if ( ! isset( $bcd->yoast_seo ) )
			return;

		if ( ! $bcd->yoast_seo->has( $action->old_term->term_id ) )
			return $this->debug( 'No Yoast SEO meta data found for %s', $action->old_term->slug );

		$meta = get_option( 'wpseo_taxonomy_meta', true );
		$meta = maybe_unserialize( $meta );

		$new_meta = $bcd->yoast_seo->get( $action->old_term->term_id );

		if ( ! isset( $meta[ $action->taxonomy ] ) )
			$meta[ $action->taxonomy ] = [];

		if ( ! isset( $meta[ $action->taxonomy ] ) )
			$meta[ $action->taxonomy ][ $action->new_term->term_id ] = $new_meta;

		$old_meta = $meta[ $action->taxonomy ][ $action->new_term->term_id ];

		if ( $this->get_site_option( 'keep_canonical' ) )
		{
			$old_canonical = '';
			if ( isset( $old_meta[ 'wpseo_canonical' ] ) )
				$old_canonical = $old_meta[ 'wpseo_canonical' ];

			$this->debug( 'Saving old canonical URL %s', $old_canonical );
			$new_meta[ 'wpseo_canonical' ] = $old_canonical;
		}

		$this->debug( 'Setting new meta for term %s (%s)', $new_meta, $action->old_term->slug );
		$meta[ $action->taxonomy ][ $action->new_term->term_id ] = $new_meta;

		$this->debug( 'Saving option.' );
		update_option( 'wpseo_taxonomy_meta', $meta );
	}

}
