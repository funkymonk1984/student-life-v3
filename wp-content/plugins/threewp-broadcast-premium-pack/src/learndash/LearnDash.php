<?php

namespace threewp_broadcast\premium_pack\learndash;

/**
	@brief			Adds support for the <a href="https://www.learndash.com/">LearnDash LMS</a> plugin.
	@plugin_group	3rd party compatability
	@since			2017-02-26 15:44:07
**/
class LearnDash
	extends \threewp_broadcast\premium_pack\base
{
	public function _construct()
	{
		$this->add_action( 'threewp_broadcast_broadcasting_before_restore_current_blog' );
		$this->add_action( 'threewp_broadcast_broadcasting_started' );
		$this->add_action( 'threewp_broadcast_get_post_types' );
	}

	/**
		@brief		Replace the ingredients and terms with their equivalents.
		@since		2015-04-05 08:10:43
	**/
	public function threewp_broadcast_broadcasting_before_restore_current_blog( $action )
	{
		$bcd = $action->broadcasting_data;
		$this->maybe_restore_course( $bcd );
		$this->maybe_restore_group( $bcd );
		$this->maybe_restore_lesson( $bcd );
		$this->maybe_restore_quiz( $bcd );
		$this->maybe_restore_topic( $bcd );
	}

	/**
		@brief		Save the nutritional information and ingredient metadata.
		@since		2015-04-09 19:29:30
	**/
	public function threewp_broadcast_broadcasting_started( $action )
	{
		$bcd = $action->broadcasting_data;

		$this->prepare_bcd( $bcd );
	}

	/**
		@brief		Add our types.
		@since		2016-07-27 20:15:57
	**/
	public function threewp_broadcast_get_post_types( $action )
	{
		$action->add_types( 'sfwd-courses', 'sfwd-lessons', 'sfwd-quiz', 'sfwd-essays', 'sfwd-assignment', 'groups', 'sfwd-topic', 'sfwd-certificates', 'sfwd-transactions' );
	}

	// --------------------------------------------------------------------------------------------
	// ----------------------------------------- Restore
	// --------------------------------------------------------------------------------------------

	/**
		@brief		Maybe restore the course data.
		@since		2017-02-26 15:47:51
	**/
	public function maybe_restore_course( $bcd )
	{
		if ( $bcd->post->post_type != 'sfwd-courses' )
			return;

		$this->update_sfwd_custom_field( [
			'broadcasting_data' => $bcd,
			'meta_key' => '_sfwd-courses',
			'meta_values' => [ 'sfwd-courses_course_prerequisite', 'sfwd-courses_certificate' ],
		] );

		$this->update_association( $bcd, 'learndash_group_enrolled_' );
	}

	/**
		@brief		Maybe restore the group data.
		@since		2017-02-26 17:03:12
	**/
	public function maybe_restore_group( $bcd )
	{
		$this->update_association( $bcd, 'learndash_group_users_' );
	}

	/**
		@brief		Maybe restore the lesson data.
		@since		2017-02-26 16:22:22
	**/
	public function maybe_restore_lesson( $bcd )
	{
		if ( $bcd->post->post_type != 'sfwd-lessons' )
			return;

		$this->update_sfwd_custom_field( [
			'broadcasting_data' => $bcd,
			'meta_key' => '_sfwd-lessons',
			'meta_values' => [ 'sfwd-lessons_course' ],
		] );

		$this->update_equivalent_post_id( $bcd, 'course_id' );
	}

	/**
		@brief		Maybe restore the quiz data.
		@since		2017-02-26 16:40:30
	**/
	public function maybe_restore_quiz( $bcd )
	{
		if ( $bcd->post->post_type != 'sfwd-quiz' )
			return;

		$this->update_sfwd_custom_field( [
			'broadcasting_data' => $bcd,
			'meta_key' => '_sfwd-quiz',
			'meta_values' => [ 'sfwd-quiz_course', 'sfwd-quiz_lesson', 'sfwd-quiz_certificate' ],
		] );

		$this->update_equivalent_post_id( $bcd, 'course_id' );
		$this->update_equivalent_post_id( $bcd, 'lesson_id' );
	}

	/**
		@brief		Maybe restore this lesson topic.
		@since		2017-02-26 16:35:29
	**/
	public function maybe_restore_topic( $bcd )
	{
		if ( $bcd->post->post_type != 'sfwd-topic' )
			return;

		$this->update_sfwd_custom_field( [
			'broadcasting_data' => $bcd,
			'meta_key' => '_sfwd-topic',
			'meta_values' => [ 'sfwd-topic_course', 'sfwd-topic_lesson' ],
		] );

		$this->update_equivalent_post_id( $bcd, 'course_id' );
		$this->update_equivalent_post_id( $bcd, 'lesson_id' );
	}

	// --------------------------------------------------------------------------------------------
	// ----------------------------------------- Misc
	// --------------------------------------------------------------------------------------------

	/**
		@brief		Prepare the broadcasting_data object.
		@since		2016-07-27 21:28:24
	**/
	public function prepare_bcd( $bcd )
	{
		if ( ! isset( $bcd->wpurp ) )
			$bcd->learndash = ThreeWP_Broadcast()->collection();
	}

	/**
		@brief		Update the association to other parts of LearnDash.
		@since		2017-02-26 17:04:36
	**/
	public function update_association( $bcd, $assoc_key )
	{
		foreach( $bcd->custom_fields()->child_fields() as $key => $value )
		{
			// Look for the key.
			if ( strpos( $key, $assoc_key ) === false )
				continue;

			// Extract the assoc ID.
			$old_assoc_id = str_replace( $assoc_key, '', $key );
			$new_assoc_id = $bcd->equivalent_posts()->get( $bcd->parent_blog_id, $old_assoc_id, get_current_blog_id() );
			if ( $new_assoc_id > 0 )
			{
				$new_assoc_key = $assoc_key . $new_assoc_id;
				$this->debug( 'Assigning new association meta key: %s', $new_assoc_key );
				$bcd->custom_fields()->child_fields()->update_meta( $new_assoc_key, $value );
			}

			// Delete the old key that isn't being used.
			$bcd->custom_fields()->child_fields()->delete_meta( $key );
		}
	}

	/**
		@brief		Update the post ID in this custom field.
		@since		2017-02-26 16:37:30
	**/
	public function update_equivalent_post_id( $bcd, $meta_key )
	{
		$old_post_id = $bcd->custom_fields()->get_single( $meta_key );
		$new_post_id = $bcd->equivalent_posts()->get( $bcd->parent_blog_id, $old_post_id, get_current_blog_id() );
		$this->debug( 'Updating custom field %s with %s', $meta_key, $new_post_id );
		$bcd->custom_fields()->child_fields()->update_meta( $meta_key, $new_post_id );
	}

	/**
		@brief		Common function to update the serialized sfwd data in the child custom field.
		@since		2017-02-26 16:24:08
	**/
	public function update_sfwd_custom_field( $options )
	{
		$options = (object) $options;

		$data = $options->broadcasting_data->custom_fields()->child_fields()->get( $options->meta_key );
		$data = reset( $data );
		$data = maybe_unserialize( $data );

		foreach( $options->meta_values as $key )
		{
			$equivalent = $options->broadcasting_data->equivalent_posts()->get( $options->broadcasting_data->parent_blog_id, $data[ $key ], get_current_blog_id() );
			$data[ $key ] = intval( $equivalent );
		}

		$options->broadcasting_data->custom_fields()->child_fields()->update_meta( $options->meta_key, $data );
	}
}
