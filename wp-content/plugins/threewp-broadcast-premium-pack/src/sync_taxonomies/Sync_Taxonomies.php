<?php

namespace threewp_broadcast\premium_pack\sync_taxonomies;

use \plainview\sdk_broadcast\collections\collection;

/**
	@brief			Synchronize the taxonomies of target blogs with those from a source blog.
	@plugin_group	Utilities
	@since			2014-04-08 11:46:07
**/
class Sync_Taxonomies
	extends \threewp_broadcast\premium_pack\base
{
	use recordings_trait;		// To split up the code into manageable pieces.

	public function _construct()
	{
		$this->add_action( 'threewp_broadcast_menu' );
		$this->recordings_construct();
	}

	// --------------------------------------------------------------------------------------------
	// ----------------------------------------- Admin
	// --------------------------------------------------------------------------------------------

	public function admin_menu_sync()
	{
		$form = $this->form2();
		$r = '';
		$source_blog_id = get_current_blog_id();

		$form->taxonomies = $form->select( 'taxonomies' )
			// Input title
			->description( __( 'Select the taxonomies on this blog you with to synchronize on the target blogs.', 'threewp_broadcast' ) )
			// Input label
			->label( __( 'Taxonomies', 'threewp_broadcast' ) )
			->multiple()
			->required();
		$form->taxonomies->taxonomy = new collection;

		$options = [];
		$post_types = get_post_types( [], 'objects' );
		foreach( $post_types as $post_type_name => $post_type )
		{
			$post_taxonomies = get_object_taxonomies( [ 'post_type' => $post_type_name ], 'objects' );
			foreach( $post_taxonomies as $taxonomy_name => $taxonomy )
			{
				$label = sprintf( '%s %s', $post_type->labels->singular_name, strtolower( $taxonomy->labels->singular_name ) );
				$value = sprintf( '%s_%s', $post_type->name, $taxonomy->name );
				$options[ $label ] = $value;
				$form->taxonomies->taxonomy->set( $value, [ $post_type, $taxonomy ] );
			}
		}

		// Sort the options.
		ksort( $options );

		// And put them in the select.
		array_flip( $options );
		$form->taxonomies->options( $options )
			->autosize();

		// List of blogs
		$blogs_input = $form->blogs = $form->select( 'blogs' )
			// Input title
			->description( __( 'Select the target blogs that will have their taxonomies synced with this blog.', 'threewp_broadcast' ) )
			// Input label
			->label( __( 'Blogs', 'threewp_broadcast' ) )
			->multiple()
			->required()
			->size( 10 );

		$action = new \threewp_broadcast\actions\get_user_writable_blogs( $this->user_id() );
		$blogs = $action->execute()->blogs;
		foreach( $blogs as $blog )
			if ( $blog->id != $source_blog_id )
				$form->blogs->option( $blog->blogname, $blog->id );

		if ( function_exists( 'BC_Blog_Groupize' ) )
			BC_Blog_Groupize( $blogs_input );

		// Apply button
		$form->primary_button( 'apply' )
			// Button
			->value( __( 'Synchronize selected taxonomies', 'threewp_broadcast' ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$selected_taxonomies = $form->taxonomies->get_post_value();

			foreach( $selected_taxonomies as $selected_taxonomy )
			{
				$data = $form->taxonomies->taxonomy->get( $selected_taxonomy );
				$post_type = $data[ 0 ];
				$taxonomy = $data[ 1 ];
				$this->debug( 'Handling taxonomy %s for post type %s.', $taxonomy->name, $post_type->name );

				$post = (object)[
					'ID' => 0,
					'post_type' => $post_type->name,
					'post_status' => 'publish',
				];

				$bcd = new \threewp_broadcast\broadcasting_data( [
					'parent_post_id' => -1,
					'post' => $post,
				] );
				$bcd->add_new_taxonomies = true;
				unset( $bcd->post->ID );		// This is so that collect_post_type_taxonomies returns ALL the terms, not just those from the non-existent post.
				ThreeWP_Broadcast()->collect_post_type_taxonomies( $bcd );

				foreach( $form->blogs->get_post_value() as $blog_id )
				{
					$this->debug( 'Switching to %s (ID: %s)', $blogs->get( $blog_id )->blogname, $blog_id );
					switch_to_blog( $blog_id );

					ThreeWP_Broadcast()->sync_terms( $bcd, $taxonomy->name );

					$this->debug( 'Switching back to %s (ID: %s)', $blogs->get( $source_blog_id )->blogname, $source_blog_id );
					restore_current_blog();
				}
			}
			if ( ! ThreeWP_Broadcast()->debugging() )
				// Finished syncing taxonomies.
				$r .= $this->message( __( 'Finished synchronizing.', 'threewp_broadcast' ) );
			else
				$this->debug( 'Finished synchronizing.' );
		}

		$r .= $this->p( __( 'The selected taxonomies will be copied to the selected target blogs. If the taxonomies exist they will be updated if the name, description or parent differs.', 'threewp_broadcast' ) );

		$action = new actions\sync_taxonomies_get_info;
		$action->execute();
		$r .= $action->text;

		$r .= $this->p( __( 'Enable Broadcast debug mode to see tons of debug information.', 'threewp_broadcast' ) );

		$r .= $form->open_tag();
		$r .= $form->display_form_table();
		$r .= $form->close_tag();

		echo $r;
	}

	public function admin_menu_tabs()
	{
		$this->load_language();

		$tabs = $this->tabs();

		$tabs->tab( 'sync' )
			->callback_this( 'admin_menu_sync' )
			->name( __( 'Sync', 'threewp_broadcast' ) )
			->sort_order( 25 );

		$this->recording_traits_tabs( $tabs );

		echo $tabs;
	}

	// --------------------------------------------------------------------------------------------
	// ----------------------------------------- Callbacks
	// --------------------------------------------------------------------------------------------

	public function threewp_broadcast_menu( $action )
	{
		$role = ThreeWP_Broadcast()->get_site_option( 'role_taxonomies' );

		if ( ! is_super_admin() AND ! ThreeWP_Broadcast()->user_has_roles( $role ) )
			return;

		$action->menu_page
			->submenu( 'threewp_broadcast_sync_taxonomies' )
			->callback_this( 'admin_menu_tabs' )
			// Menu item for menu
			->menu_title( __( 'Sync Taxonomies', 'threewp_broadcast' ) )
			// Page title for menu
			->page_title( __( 'Broadcast Sync Taxonomies', 'threewp_broadcast' ) );
	}

	// --------------------------------------------------------------------------------------------
	// ----------------------------------------- Misc
	// --------------------------------------------------------------------------------------------

	public function site_options()
	{
		return array_merge( [
			'recordings' => '',
		], parent::site_options() );
	}
}
