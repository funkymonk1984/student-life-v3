<?php

namespace threewp_broadcast\premium_pack\update_attachments;

use \threewp_broadcast\attachment_data;

/**
	@brief			Update existing attachments by copying over the files to the child blogs.
	@plugin_group	Control
	@since			2015-11-16 15:24:56
**/
class Update_Attachments
	extends \threewp_broadcast\premium_pack\base
{
	public function _construct()
	{
		$this->add_action( 'threewp_broadcast_apply_existing_attachment_action' );
		$this->add_action( 'threewp_broadcast_broadcasting_started' );
		$this->add_action( 'threewp_broadcast_get_existing_attachment_actions' );
	}

	/**
		@brief		Update the existing files.
		@since		2015-11-16 15:31:13
	**/
	public function threewp_broadcast_apply_existing_attachment_action( $action )
	{
		$source = $action->source_attachment;
		$target = attachment_data::from_attachment_id( $action->target_attachment->ID );

		// Replace the main file.
		$this->debug( 'Replacing %s with %s', $target->filename_path, $source->filename_path );
		copy( $source->filename_path, $target->filename_path );

		// Update the alt, desc, etc.
		$data = [
			'ID' => $target->post->ID,
			'post_content' => $source->post->post_content,
			'post_title' => $source->post->post_title,
			'post_excerpt' => $source->post->post_excerpt,
		];
		$this->debug( 'Updating image post data: %s', $data );
		wp_update_post( $data );

		// Update the alt?
		$key = '_wp_attachment_image_alt';
		if ( isset( $source->post_custom[ $key ] ) )
		{
			$value = reset( $source->post_custom[ $key ] );
			$this->debug( 'Updating image alt: %s', $value );
			update_post_meta( $target->post->ID, $key, $value );
		}

		// If there are any thumbnails, let's copy them also.
		if ( $source->file_metadata )
		{
			$this->debug( 'Updating metadata.' );
			$metadata = $source->file_metadata;
			$source_dir = dirname( $source->filename_path );
			$target_dir = dirname( $target->filename_path );
			if ( isset( $metadata[ 'sizes' ] ) )
			{
				foreach( $metadata[ 'sizes' ] as $data )
				{
					if ( ! isset( $data[ 'file' ] ) )
						continue;
					$filename = $data[ 'file' ];
					$source_file = $source_dir . '/' . $filename;
					$target_file = $target_dir . '/' . $filename;
					$this->debug( 'Copying %s to %s', $source_file, $target_file );
					copy( $source_file, $target_file );
				}
			}

		}
	}

	/**
		@brief		Prevent Broadcast from deleting the existing attachments.
		@since		2015-11-16 15:30:11
	**/
	public function threewp_broadcast_broadcasting_started( $action )
	{
		$action->broadcasting_data->delete_attachments = false;
	}

	/**
		@brief		threewp_broadcast_get_existing_attachment_actions
		@since		2015-11-16 15:26:12
	**/
	public function threewp_broadcast_get_existing_attachment_actions( $action )
	{
		// What to do with existing attachments
		$s = __( 'Update the files with the contents from the parent blog.', 'threewp_broadcast' );
		$action->add( 'update', $s );
	}
}
