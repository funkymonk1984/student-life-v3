<?php

namespace threewp_broadcast\premium_pack\php_code;

use \Exception;

/**
	@brief			Run custom PHP code on selected blogs.
	@plugin_group	Utilities
	@since			2015-11-23 06:55:55
**/
class PHP_Code
	extends \threewp_broadcast\premium_pack\base
{
	/**
		@brief		The cached blogs and sizes object.
		@see		sizes()
		@since		2015-06-03 16:39:03
	**/
	public $__sizes;

	public function _construct()
	{
		$this->add_action( 'threewp_broadcast_menu' );
	}

	/**
		@brief		Add ourself to the menu.
		@since		2015-11-23 06:57:09
	**/
	public function threewp_broadcast_menu( $action )
	{
		// Only super admin is allowed to see us.
		if ( ! is_super_admin() )
			return;

		$action->menu_page
			->submenu( 'threewp_broadcast_php_code' )
			->callback_this( 'setup' )
			// Menu item name
			->menu_title( __( 'PHP code', 'threewp_broadcast' ) )
			// Menu page title
			->page_title( __( 'Broadcast PHP Code', 'threewp_broadcast' ) );
	}

	/**
		@brief		Run the code in the form.
		@since		2015-11-23 07:56:02
	**/
	public function run_code( $form )
	{
		// Retrieve the code from the form.
		$setup_code = $form->input( 'setup_code' )->get_post_value();
		$loop_code = $form->input( 'loop_code' )->get_post_value();
		$teardown_code = $form->input( 'teardown_code' )->get_post_value();

		// Retrieve the list of blogs on which to run the code.
		$blogs = $form->input( 'blogs' )->get_post_value();

		eval( $setup_code );

		foreach( $blogs as $blog_id )
		{
			switch_to_blog( $blog_id );
			eval( $loop_code );
			restore_current_blog();
		}

		eval( $teardown_code );
	}

	/**
		@brief		Show the run form.
		@since		2015-11-23 06:58:09
	**/
	public function setup()
	{
		$form = $this->form2();
		$form->css_class( 'plainview_form_auto_tabs' );
		$r = '<style> textarea.code{ font-family: Consolas,Monaco,monospace } </style>';

		// CODE
		// ----

		$fs = $form->fieldset( 'fs_code' );
		// PHP Code fieldset label.
		$fs->legend()->label( __( 'Code editor', 'threewp_broadcast' ) );

		$fs->textarea( 'setup_code' )
			->css_class( 'code' )
			// Input description
			->description( __( 'This code that is run prior to beginning the loop.', 'threewp_broadcast' ) )
			// Input label: Code to setup the loop
			->label( __( 'Setup code', 'threewp_broadcast' ) )
			->rows( 10, 80 );

		$fs->textarea( 'loop_code' )
			->css_class( 'code' )
			// Input description
			->description( __( 'This code is run on all selected blogs.', 'threewp_broadcast' ) )
			// Input label: Code that goes in the loop.
			->label( __( 'Loop code', 'threewp_broadcast' ) )
			->rows( 10, 80 );

		$fs->textarea( 'teardown_code' )
			->css_class( 'code' )
			// Input description
			->description( __( 'This code that is run after the loop.', 'threewp_broadcast' ) )
			// Input label: Code that is run after the loop
			->label( __( 'Teardown code', 'threewp_broadcast' ) )
			->rows( 10, 80 );

		$run_button = $fs->primary_button( 'run_code' )
			// Button text
			->value( __( 'Run this code on the selected blogs', 'threewp_broadcast' ) );

		// BLOGS
		// -----

		$fs = $form->fieldset( 'fs_blogs' );
		// Blogs selector fieldset label.
		$fs->legend()->label( __( 'Blogs', 'threewp_broadcast' ) );

		$blogs_select = $this->add_blog_list_input( [
			// Blog selection input description
			'description' => __( 'Select one or more blogs on which to run the code.', 'threewp_broadcast' ),
			'form' => $fs,
			// Blog selection input label
			'label' => __( 'Blogs', 'threewp_broadcast' ),
			'multiple' => true,
			'name' => 'blogs',
		] );

		// HOWTO
		// ------

		$fs = $form->fieldset( 'fs_howto' );
		// PHP generator howto fieldset label.
		$fs->legend()->label( __( 'Howto', 'threewp_broadcast' ) );

		$text = file_get_contents( __DIR__ . '/html/howto.html' );

		$fs->markup( 'm_howto' )
			->p( $text );

		// WIZARD
		// ------

		$fs = $form->fieldset( 'fs_wizard' );
		// PHP generator wizard fieldset label.
		$fs->legend()->label( __( 'Wizard', 'threewp_broadcast' ) );

		$wizards = $fs->select( 'wizards' )
			// Input description
			->description( __( 'Select a PHP code example to place in the code textareas.', 'threewp_broadcast' ) )
			// Input label
			->label( __( 'Select a wizard', 'threewp_broadcast' ) )
			->option( __( 'Select a wizard', 'threewp_broadcast' ), '' );

		$wizards->optgroup( 'optgroup_themes' )
			// Options optgroup label in the wizard select
			->label_( 'Options' )
			->option( __( "Show the current language of the blog", 'threewp_broadcast' ), 'options_show_language' )
			;

		$wizards->optgroup( 'themes' )
			// Themes optgroup label in the wizard select
			->label( __( 'Themes', 'threewp_broadcast' ) )
			// Wizard select option
			->option( __( "Show the current theme of the blog", 'threewp_broadcast' ), 'show_theme' )
			// Wizard select option
			->option( __( "Switch the blog's theme", 'threewp_broadcast' ), 'switch_theme' )
			;

		$wizard_button = $fs->secondary_button( 'add_code' )
			// Button text
			->value( __( 'Add the selected example', 'threewp_broadcast' ) );

		// Handle the posting of the form
		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();

			if ( $run_button->pressed() )
			{
				try
				{
					$this->run_code( $form );
					$this->message( __( 'Your code has been run!', 'threewp_broadcast' ) );
				}
				catch ( Exception $e )
				{
					$this->error_message_box()->_( $e->getMessage() );
				}
			}

			if ( $wizard_button->pressed() )
			{
				$example = $form->select( 'wizards' )->get_post_value();
				if ( $example != '' )
				{
					foreach( [
						'.setup' => 'setup_code',
						'' => 'loop_code',
						'.teardown' => 'teardown_code',
					] as $filesuffix => $input_name )
					{
						$filename = __DIR__ . '/code/' . $example . $filesuffix . '.php';
						if ( ! file_exists( $filename ) )
							continue;

						$input = $form->input( $input_name  );
						$code = $input->get_post_value();
						$code .= file_get_contents( $filename );
						$input->value( $code );
					}
				}
				$this->message( __( "The selected code has been added to the code editor.", 'threewp_broadcast' ) );
			}
		}

		$r .= $form->open_tag();
		$r .= $form->display_form_table();
		$r .= $form->close_tag();

		// Page heading
		echo $this->wrap( $r, __( 'PHP Code', 'threewp_broadcast' ) );
	}
}
