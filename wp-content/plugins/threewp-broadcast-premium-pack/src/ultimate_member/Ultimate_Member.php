<?php

namespace threewp_broadcast\premium_pack\ultimate_member;

use \Exception;

/**
	@brief			Adds support for the <a href="https://wordpress.org/plugins/ultimate-member/">Ultimate Member</a> plugin.
	@plugin_group	3rd party compatability
	@since			2017-05-23 09:50:47
**/
class Ultimate_Member
	extends \threewp_broadcast\premium_pack\classes\Shortcode_Preparser
{
	/**
		@brief		Return the shortcode attribute that stores the item ID.
		@since		2017-01-11 23:04:21
	**/
	public function get_shortcode_id_attribute()
	{
		return 'form_id';
	}

	/**
		@brief		Return the name of the shortcode we are looking for.
		@since		2017-01-11 23:03:36
	**/
	public function get_shortcode_name()
	{
		return 'ultimatemember';
	}
}
