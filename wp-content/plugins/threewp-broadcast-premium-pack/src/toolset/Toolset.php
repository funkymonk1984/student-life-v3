<?php

namespace threewp_broadcast\premium_pack\toolset;

/**
	@brief				Adds support for OnTheGoSystems' Toolset plugins: CRED, Types and Views.
	@plugin_group		3rd party compatability
	@since				2016-07-08 14:21:53
**/
class Toolset
	extends \threewp_broadcast\premium_pack\base
{
	const CONTENT_TEMPLATE_CUSTOM_FIELD = '_views_template';

	public function _construct()
	{
		$this->add_action( 'threewp_broadcast_broadcasting_before_restore_current_blog' );
		$this->add_action( 'threewp_broadcast_broadcasting_finished' );
		$this->add_action( 'threewp_broadcast_broadcasting_started' );
		$this->add_filter( 'toolset_filter_register_menu_pages', 100 );
		$this->add_action( 'types_save_post_hook' );
	}

	// --------------------------------------------------------------------------------------------
	// ----------------------------------------- Callbacks
	// --------------------------------------------------------------------------------------------

	/**
		@brief		threewp_broadcast_broadcasting_before_restore_current_blog
		@since		2016-03-10 13:43:45
	**/
	public function threewp_broadcast_broadcasting_before_restore_current_blog( $action )
	{
		if ( $this->has_types() )
			$this->maybe_restore_type( $action );
		if ( isset( $action->broadcasting_data->toolset_content_template ) )
			$this->maybe_restore_content_template( $action );
	}

	/**
		@brief		Restore the children.
		@since		2016-03-09 08:07:38
	**/
	public function threewp_broadcast_broadcasting_finished( $action )
	{
		$bcd = $action->broadcasting_data;

		if ( ! isset( $bcd->toolset_types ) )
			return $this->debug( 'Nothing to do.' );

		if ( ! $bcd->toolset_types->has( 'wpcf_post_relationship' ) )
			return;

		// Collect an array of all of the blogs where we are to broadcast the children.
		$blogs = [];
		foreach( $bcd->blogs as $blog )
			$blogs []= $blog->id;

		// Broadcast each child to the blogs.
		foreach( $bcd->toolset_types->get( 'wpcf_post_relationship' ) as $post_id => $ignore )
			ThreeWP_Broadcast()->api()->broadcast_children( $post_id, $blogs );
	}

	public function threewp_broadcast_broadcasting_started( $action )
	{
		if ( $this->has_types() )
			$this->maybe_save_type( $action );
		if ( $this->has_views() )
			$this->maybe_save_content_template( $action );
	}

	/**
		@brief		Add us to the menu.
		@since		2016-07-06 13:21:23
	**/
	public function toolset_filter_register_menu_pages( $pages )
	{
		if ( $this->has_views() )
			$pages[] = [
			'slug'			=> 'broadcast_content_templates',
			// Menu item for menu
			'menu_title'	=> __( 'Broadcast content templates', 'threewp_broadcast' ),
			// Page title for menu
			'page_title'	=> __( 'Broadcast content templates', 'threewp_broadcast' ),
			'callback'		=> [ $this, 'broadcast_content_templates' ],
			];

		if ( $this->has_types() )
			$pages[] = [
			'slug'			=> 'broadcast_field_groups',
			// Menu item for menu
			'menu_title'	=> __( 'Broadcast field groups', 'threewp_broadcast' ),
			// Page title for menu
			'page_title'	=> __( 'Broadcast field groups', 'threewp_broadcast' ),
			'callback'		=> [ $this, 'broadcast_field_groups' ],
			];

		if ( $this->has_types() )
			$pages[] = [
			'slug'			=> 'broadcast_types',
			// Menu item for menu
			'menu_title'	=> __( 'Broadcast post types', 'threewp_broadcast' ),
			// Page title for menu
			'page_title'	=> __( 'Broadcast post types', 'threewp_broadcast' ),
			'callback'		=> [ $this, 'broadcast_post_types' ],
			];

		if ( $this->has_cred() )
			$pages[] = [
			'slug'			=> 'broadcast_post_forms',
			// Menu item for menu
			'menu_title'	=> __( 'Broadcast post forms', 'threewp_broadcast' ),
			// Page title for menu
			'page_title'	=> __( 'Broadcast post forms', 'threewp_broadcast' ),
			'callback'		=> [ $this, 'broadcast_post_forms' ],
			];

		if ( $this->has_cred() )
			$pages[] = [
			'slug'			=> 'broadcast_user_forms',
			// Menu item for menu
			'menu_title'	=> __( 'Broadcast user forms', 'threewp_broadcast' ),
			// Page title for menu
			'page_title'	=> __( 'Broadcast user forms', 'threewp_broadcast' ),
			'callback'		=> [ $this, 'broadcast_user_forms' ],
			];

		if ( $this->has_views() )
			$pages[] = [
			'slug'			=> 'broadcast_views',
			// Menu item for menu
			'menu_title'	=> __( 'Broadcast views', 'threewp_broadcast' ),
			// Page title for menu
			'page_title'	=> __( 'Broadcast views', 'threewp_broadcast' ),
			'callback'		=> [ $this, 'broadcast_views' ],
			];

		return $pages;
	}

	/**
		@brief		This action tells Broadcast that Types is done saving the children.
		@since		2016-03-08 23:14:28
	**/
	public function types_save_post_hook( $post_id )
	{
		ThreeWP_Broadcast()->save_post( $post_id );
	}

	// --------------------------------------------------------------------------------------------
	// ----------------------------------------- Save
	// --------------------------------------------------------------------------------------------

	/**
		@brief		Save the content template.
		@since		20131007
	**/
	public function maybe_save_content_template( $action )
	{
		$bcd = $action->broadcasting_data;

		// Is there a _views_template custom field?
		if ( ! $bcd->custom_fields()->has( self::CONTENT_TEMPLATE_CUSTOM_FIELD ) )
			return $this->debug( 'No views template custom field found.' );

		$content_template_id = $bcd->custom_fields()->get_single( self::CONTENT_TEMPLATE_CUSTOM_FIELD );
		$this->debug( 'The template ID is %s', $content_template_id );

		// Create the views data in the broadcasting data
		$bcd->toolset_content_template = new \stdClass;

		// Conv
		$views = $bcd->toolset_content_template;

		// Save the template (post) data
		$views->post = get_post( $content_template_id );
	}

	/**
		@brief		Maybe save the Types data.
		@since		2016-03-08 23:11:29
	**/
	public function maybe_save_type( $action )
	{
		if ( ! $this->has_types() )
			return $this->debug( 'Types not installed.' );

		$bcd = $action->broadcasting_data;

		$bcd->toolset_types = ThreeWP_Broadcast()->collection();

		// Look through the custom fields to see whether this post has a parent somewhere.
		foreach( $bcd->custom_fields->original as $key => $value )
		{
			if ( strpos( $key, '_wpcf_belongs_' ) !== 0 )
				continue;
			$this->debug( 'Saving link to parent post %s', $parent_post_id );
			$parent_post_id = reset( $value );
			$bcd->toolset_types->set( 'belongs_key', $key );
			$bcd->toolset_types->set( 'belongs_value', $parent_post_id );
			$bcd->toolset_types->set( 'belongs_bcd', ThreeWP_Broadcast()->get_post_broadcast_data( get_current_blog_id(), $parent_post_id ) );
		}

		if ( ! isset( $bcd->_POST[ 'wpcf_post_relationship' ] ) )
			return $this->debug( 'No wpcf post relationship found.' );

		// We have to look at the _POST, since the relationship info isn't saved in the custom fields of the parent post, but the postmeta of the child post(s).
		foreach( $bcd->_POST[ 'wpcf_post_relationship' ] as $parent_post_id => $child_posts )
			if ( $parent_post_id == $bcd->post->ID )
				$this->save_parent_post( $bcd );
	}

	/**
		@brief		Broadcast the parent post.
		@since		2016-03-10 09:48:51
	**/
	public function save_parent_post( $bcd )
	{
		$relationships = $bcd->_POST[ 'wpcf_post_relationship' ];
		$this->debug( 'The relationship array is: %s', $relationships );

		$post_id = $bcd->post->ID;
		if ( ! isset( $relationships[ $post_id ] ) )
			return $this->debug( 'This post has no relationships.' );

		// We want the relationships for just this parent post.
		$relationships = $relationships[ $post_id ];

		$bcd->toolset_types->set( 'wpcf_post_relationship', $relationships );
	}

	// --------------------------------------------------------------------------------------------
	// ----------------------------------------- Restore
	// --------------------------------------------------------------------------------------------

	/**
		@brief		Restore the type.
		@since		2016-07-08 14:47:33
	**/
	public function maybe_restore_type( $action )
	{
		$bcd = $action->broadcasting_data;

		if ( ! isset( $bcd->toolset_types ) )
			return $this->debug( 'Nothing to do.' );

		if ( ! $bcd->toolset_types->has( 'belongs_key' ) )
			return;

		$meta_key = $bcd->toolset_types->get( 'belongs_key' );
		$parent_bcd = $bcd->toolset_types->get( 'belongs_bcd' );
		$new_parent_post_id = $parent_bcd->get_linked_child_on_this_blog();
		$this->debug( 'Replacing link to old parent %s with new parent %s.', $bcd->toolset_types->get( 'belongs_value' ), $new_parent_post_id );
		$bcd->custom_fields()->child_fields()->update_meta( $meta_key, $new_parent_post_id );
	}

	/**
		@brief		Restore the view.
		@since		20131007
	**/
	public function maybe_restore_content_template( $action )
	{
		$bcd = $action->broadcasting_data;
		$post = $bcd->toolset_content_template->post;

		$this->debug( 'Looking for a post named %s, with the %s post type.', $post->post_name, $post->post_type );

		// Find the post on this blog with the content template name.
		$args = array(
			'name' => $post->post_name,
			'numberposts' => 1,
			'post_type'=> $post->post_type,
		);
		$template = get_posts( $args );

		$this->debug( '%s posts found.', count( $template ) );
		// Was the template with the exact same name found on this blog?
		if ( count( $template ) != 1 )
		{
			// There is no equivalent template. Remove the custom field.
			$bcd->custom_fields()
				->child_fields()
				->delete_meta( self::CONTENT_TEMPLATE_CUSTOM_FIELD );
			return $this->debug( 'No equivalent content template found.' );
		}

		// We want the first (and only) result.
		$template = reset( $template );
		$this->debug( 'Equivalent content template ID is %s', $template->ID );

		// Update the ID of the view on this blog.
		$bcd->custom_fields()
			->child_fields()
			->update_meta( self::CONTENT_TEMPLATE_CUSTOM_FIELD, $template->ID );
	}

	// --------------------------------------------------------------------------------------------
	// ----------------------------------------- Misc functions
	// --------------------------------------------------------------------------------------------

	/**
		@brief		Broadcast post forms to other blogs.
		@since		2016-07-08 20:42:30
	**/
	public function broadcast_content_templates()
	{
		$this->broadcast_generic_post( [
			'post_type' => 'view-template',
			// 'post_status' => 'private',
			'label_plural' => 'content templates',
			'label_singular' => 'content template',
		] );
	}

	/**
		@brief		Broadcast field groups manually.
		@since		2016-07-07 14:26:28
	**/
	public function broadcast_field_groups()
	{
		$form = $this->form2();
		$post_type = 'wp-types-group';
		$r = '';

		$items_select = $form->select( 'field_groups' )
			->description( 'Select the field groups to broadcast to the selected blogs.' )
			->label( 'Field groups to broadcast' )
			->multiple()
			->size( 10 )
			->required();

		// Display a select with all of the types on this blog.
		$items = get_posts( [
			'posts_per_page' => -1,
			'post_type' => $post_type,
		] );
		$items = $this->array_rekey( $items, 'post_name' );
		foreach( $items as $item )
			$items_select->option( sprintf( '%s (%s)', $item->post_title, $item->ID ), $item->post_name );

		$blogs_select = $this->add_blog_list_input( [
			// Blog selection input description
			'description' => __( 'Select one or more blogs to which to copy the selected items above.', 'threewp_broadcast' ),
			'form' => $form,
			// Blog selection input label
			'label' => __( 'Blogs', 'threewp_broadcast' ),
			'multiple' => true,
			'required' => true,
			'name' => 'blogs',
		] );

		$fs = $form->fieldset( 'fs_actions' );
		// Fieldset label
		$fs->legend->label( __( 'Actions', 'threewp_broadcast' ) );

		$nonexisting_action = $fs->select( 'nonexisting_action' )
			// Input title
			->description( __( 'What to do if the field group does not exist on the target blog.', 'threewp_broadcast' ) )
			// Input label
			->label( __( 'If the field group does not exist', 'threewp_broadcast' ) )
			->options( [
				// What to do if the Toolset field group does not exist.
				__( 'Create the field group', 'threewp_broadcast' ) => 'create',
				// What to do if the Toolset field group does not exist.
				__( 'Skip this blog', 'threewp_broadcast' ) => 'skip',
			] )
			->value( 'create' );

		$existing_action = $fs->select( 'existing_action' )
			->description( 'What to do if the field group already exists on the target blog.' )
			->label( 'If the field group exists' )
			->options( [
				// What to do if the Toolset field group already exists
				__( 'Skip this blog', 'threewp_broadcast' ) => 'skip',
				// What to do if the Toolset field group already exists
				__( 'Overwrite the existing field group', 'threewp_broadcast' ) => 'overwrite',
			] )
			->value( 'overwrite' );

		$submit = $form->primary_button( 'copy' )
			->value( 'Copy' );

		if ( $form->is_posting() )
		{
			$form->post()->use_post_values();

			$source_fields = get_option( 'wpcf-fields' );

			// We need to find out which group fields are used.
			$post_fields = ThreeWP_Broadcast()->collection();
			foreach( $items_select->get_post_value() as $item_slug )
			{
				$item = $items[ $item_slug ];
				$item_id = $item->ID;
				$_wp_types_group_fields = get_post_meta( $item_id, '_wp_types_group_fields', true );
				$_wp_types_group_fields = maybe_unserialize( $_wp_types_group_fields );
				$this->debug( 'Item %s has field groups: %s', $item_slug, $_wp_types_group_fields );
				$post_fields->set( $item_slug, $_wp_types_group_fields );
			}

			foreach ( $blogs_select->get_post_value() as $blog_id )
			{
				// Don't copy the type to ourself.
				if ( $blog_id == get_current_blog_id() )
					continue;
				switch_to_blog( $blog_id );

				$blog_items = get_posts( [
					'posts_per_page' => -1,
					'post_type' => $post_type,
				] );
				$blog_items = $this->array_rekey( $blog_items, 'post_name' );

				foreach( $items_select->get_post_value() as $item_slug )
				{
					$broadcast = false;
					if ( ! isset( $blog_items[ $item_slug ] ) )
					{
						$this->debug( 'Item %s not found on blog %s.', $item_slug, $blog_id );
						if ( $nonexisting_action->get_post_value() == 'create' )
						{
							$this->debug( 'Creating item %s.', $item_slug );
							$broadcast = true;
						}
					}
					else
					{
						$this->debug( 'Item %s found on blog %s.', $item_slug, $blog_id );
						if ( $existing_action->get_post_value() == 'overwrite' )
						{
							$this->debug( 'Overwriting item %s.', $item_slug );
							$broadcast = true;
						}
					}

					if ( $broadcast )
					{
						$original_post_id = $items[ $item_slug ]->ID;
						restore_current_blog();
						$this->debug( 'Broadcasting item %s.', $original_post_id );
						ThreeWP_Broadcast()->api()->broadcast_children( $original_post_id, [ $blog_id ] );
						switch_to_blog( $blog_id );

						// Update the fields, if necessary.
						$target_fields = get_option( 'wpcf-fields' );
						if ( ! is_array( $target_fields ) )
							$target_fields = [];
						$this->debug( 'Current target fields: %s', $target_fields );

						$item_fields = $post_fields->get( $item_slug );
						$item_fields = explode( ',', $item_fields );
						$item_fields = array_filter( $item_fields );

						foreach( $item_fields as $item_field )
							$target_fields[ $item_field ] = $source_fields[ $item_field ];

						$this->debug( 'New target fields after merge: %s', $target_fields );
						update_option( 'wpcf-fields', $target_fields );
					}
				}

				restore_current_blog();
			}
			$r .= $this->info_message_box()->_( 'The selected items have been copied to the selected blogs.' );
		}

		$r .= $form->open_tag();
		$r .= $form->display_form_table();
		$r .= $form->close_tag();

		echo $this->wrap( $r, 'Broadcast field groups' );
	}

	/**
		@brief		Broadcast post forms to other blogs.
		@since		2016-07-08 20:42:30
	**/
	public function broadcast_post_forms()
	{
		$this->broadcast_generic_post( [
			'post_type' => 'cred-form',
			'post_status' => 'private',
			'label_plural' => 'post forms',
			'label_singular' => 'post form',
		] );
	}

	/**
		@brief		Generic broadcasting function.
		@since		2016-07-08 21:31:04
	**/
	public function broadcast_generic_post( $options )
	{
		$options = (object) array_merge( [
			'post_status' => 'publish',
		], $options );

		$form = $this->form2();
		$r = '';

		$items_select = $form->select( 'items' )
			// Select the generic post to broadcast
			->description( __( 'Select the %s to broadcast to the selected blogs.', $options->label_plural, 'threewp_broadcast' ) )
			// POSTTYPE to broadcast
			->label( __( '%s to broadcast', ucfirst( $options->label_plural ), 'threewp_broadcast' ) )
			->multiple()
			->size( 10 )
			->required();

		// Display a select with all of the items on this blog.
		$items = get_posts( [
			'posts_per_page' => -1,
			'post_status' => $options->post_status,
			'post_type' => $options->post_type,
		] );
		$items = $this->array_rekey( $items, 'post_name' );
		foreach( $items as $item )
			$items_select->option( sprintf( '%s (%s)', $item->post_title, $item->ID ), $item->post_name );

		$blogs_select = $this->add_blog_list_input( [
			// Blog selection input description
			'description' => __( 'Select one or more blogs to which to copy the selected items above.', 'threewp_broadcast' ),
			'form' => $form,
			// Blog selection input label
			'label' => __( 'Blogs', 'threewp_broadcast' ),
			'multiple' => true,
			'required' => true,
			'name' => 'blogs',
		] );

		$fs = $form->fieldset( 'fs_actions' );
		// Fieldset label
		$fs->legend->label( 'Actions' );

		$nonexisting_action = $fs->select( 'nonexisting_action' )
			->description_( __( 'What to do if the %s does not exist on the target blog.', 'threewp_broadcast' ), $options->label_singular )
			->label_( __( 'If the %s does not exist', 'threewp_broadcast' ), $options->label_singular )
			->options( [
				// Create the item
				sprintf( __( 'Create the %s', 'threewp_broadcast' ), $options->label_singular ) => 'create',
				__( 'Skip this blog', 'threewp_broadcast' ) => 'skip',
			] )
			->value( 'create' );

		$existing_action = $fs->select( 'existing_action' )
			// if the ITEM
			->description( __( 'What to do if the %s already exists on the target blog.', 'threewp_broadcast' ), $options->label_singular )
			// If the ITEM exists
			->label( __( 'If the %s exists', 'threewp_broadcast' ), $options->label_singular )
			->options( [
				__( 'Skip this blog', 'threewp_broadcast' ) => 'skip',
				// Overwrite the existing ITEM
				sprintf( __( 'Overwrite the existing %s', 'threewp_broadcast' ), $options->label_singular ) => 'overwrite',
			] )
			->value( 'overwrite' );

		$submit = $form->primary_button( 'copy_items' )
			// Copy ITEM button
			->value_( __( 'Copy %s', 'threewp_broadcast' ), $options->label_plural );

		if ( $form->is_posting() )
		{
			$form->post()->use_post_values();

			foreach ( $blogs_select->get_post_value() as $blog_id )
			{
				// Don't copy the item to ourself.
				if ( $blog_id == get_current_blog_id() )
					continue;
				switch_to_blog( $blog_id );

				$blog_items = get_posts( [
					'posts_per_page' => -1,
					'post_status' => $options->post_status,
					'post_type' => $options->post_type,
				] );
				$blog_items = $this->array_rekey( $blog_items, 'post_name' );

				foreach( $items_select->get_post_value() as $item_slug )
				{
					$broadcast = false;
					if ( ! isset( $blog_items[ $item_slug ] ) )
					{
						$this->debug( 'Item %s not found on blog %s.', $item_slug, $blog_id );
						if ( $nonexisting_action->get_post_value() == 'create' )
						{
							$this->debug( 'Creating item %s.', $item_slug );
							$broadcast = true;
						}
					}
					else
					{
						$this->debug( 'Item %s found on blog %s.', $item_slug, $blog_id );
						if ( $existing_action->get_post_value() == 'overwrite' )
						{
							$this->debug( 'Overwriting item %s.', $item_slug );
							$broadcast = true;
						}
					}

					if ( $broadcast )
					{
						$original_post_id = $items[ $item_slug ]->ID;
						restore_current_blog();
						$this->debug( 'Broadcasting item %s.', $original_post_id );
						ThreeWP_Broadcast()->api()->broadcast_children( $original_post_id, [ $blog_id ] );
						switch_to_blog( $blog_id );
					}
				}

				restore_current_blog();
			}
			$r .= $this->info_message_box()->_( __( 'The selected items have been copied to the selected blogs.', 'threewp_broadcast' ) );
		}

		$r .= $form->open_tag();
		$r .= $form->display_form_table();
		$r .= $form->close_tag();

		// Broadcast THE ITEMS
		echo $this->wrap( $r, sprintf( __( 'Broadcast %s', 'threewp_broadcast' ), $options->label_plural ) );
	}

	/**
		@brief		Broadcast Post Types to other blogs manually.
		@since		2016-06-13 13:33:09
	**/
	public function broadcast_post_types()
	{
		$form = $this->form2();
		$r = '';

		$types_select = $form->select( 'types' )
			// Input title
			->description( __( 'Select the Post Types to broadcast to the selected blogs.', 'threewp_broadcast' ) )
			// Input label
			->label( __( 'Post Types to broadcast', 'threewp_broadcast' ) )
			->multiple()
			->size( 10 )
			->required();

		// Display a select with all of the types on this blog.
		$types = get_option( 'wpcf-custom-types', true );
		foreach( $types as $type_id => $type )
			$types_select->option( $type[ 'labels' ][ 'name' ], $type_id );

		$blogs_select = $this->add_blog_list_input( [
			// Blog selection input description
			'description' => __( 'Select one or more blogs to which to copy the selected items above.', 'threewp_broadcast' ),
			'form' => $form,
			// Blog selection input label
			'label' => __( 'Blogs', 'threewp_broadcast' ),
			'multiple' => true,
			'required' => true,
			'name' => 'blogs',
		] );

		$fs = $form->fieldset( 'fs_actions' );
		// Fieldset label
		$fs->legend->label( __( 'Actions', 'threewp_broadcast' ) );

		$nonexisting_action = $fs->select( 'nonexisting_action' )
			// Input title
			->description( __( 'What to do if the Post Type does not exist on the target blog.', 'threewp_broadcast' ) )
			// Input label
			->label( __( 'If the Post Type does not exist', 'threewp_broadcast' ) )
			->options( [
				__( 'Create the Post Type', 'threewp_broadcast' ) => 'create',
				__( 'Skip this blog', 'threewp_broadcast' ) => 'skip',
			] )
			->value( 'create' );

		$existing_action = $fs->select( 'existing_action' )
			->description( 'What to do if the Post Type already exists on the target blog.' )
			->label( 'If the Post Type exists' )
			->options( [
				__( 'Skip this blog', 'threewp_broadcast' ) => 'skip',
				__( 'Overwrite the existing Post Type', 'threewp_broadcast' ) => 'overwrite',
			] )
			->value( 'overwrite' );

		$submit = $form->primary_button( 'copy_types' )
			// Button
			->value( __( 'Copy Post Types', 'threewp_broadcast' ) );

		if ( $form->is_posting() )
		{
			$form->post()->use_post_values();

			foreach ( $blogs_select->get_post_value() as $blog_id )
			{
				// Don't copy the type to ourself.
				if ( $blog_id == get_current_blog_id() )
					continue;
				switch_to_blog( $blog_id );

				$blog_types = get_option( 'wpcf-custom-types', true );
				if ( ! is_array( $blog_types ) )
					$blog_types = [];

				foreach( $types_select->get_post_value() as $type_id )
				{
					if ( ! isset( $blog_types[ $type_id ] ) )
					{
						$this->debug( 'Type %s not found on blog %s.', $type_id, $blog_id );
						if ( $nonexisting_action->get_post_value() == 'create' )
						{
							$this->debug( 'Creating post type %s.', $type_id );
							$blog_types[ $type_id ] = $types[ $type_id ];
						}
					}
					else
					{
						$this->debug( 'Type %s found on blog %s.', $type_id, $blog_id );
						if ( $existing_action->get_post_value() == 'overwrite' )
						{
							$this->debug( 'Overwriting post type %s.', $type_id );
							$blog_types[ $type_id ] = $types[ $type_id ];
						}
					}
				}

				$this->debug( 'The Post Types on this blog are now: %s', $blog_types );
				update_option( 'wpcf-custom-types', $blog_types );

				restore_current_blog();
			}
			$r .= $this->info_message_box()->_( __( 'The selected Post Types have been copied to the selected blogs.', 'threewp_broadcast' ) );
		}

		$r .= $form->open_tag();
		$r .= $form->display_form_table();
		$r .= $form->close_tag();

		// Page title
		echo $this->wrap( $r, __( 'Broadcast Post Types', 'threewp_broadcast' ) );
	}

	/**
		@brief		Broadcast user forms to other blogs.
		@since		2016-07-08 20:42:30
	**/
	public function broadcast_user_forms()
	{
		$this->broadcast_generic_post( [
			'post_type' => 'cred-user-form',
			'post_status' => 'private',
			'label_plural' => 'user forms',
			'label_singular' => 'user form',
		] );
	}

	/**
		@brief		Broadcast views to other blogs manually.
		@since		2016-06-13 13:33:09
	**/
	public function broadcast_views()
	{
		$this->broadcast_generic_post( [
			'post_type' => 'view',
			// 'post_status' => 'private',
			'label_plural' => 'views',
			'label_singular' => 'view',
		] );
	}

	/**
		@brief		Is Cred installed?
		@since		2016-07-08 20:17:15
	**/
	public function has_cred()
	{
		return defined( 'CRED_FE_VERSION' );
	}

	/**
		@brief		Is the types plugin installed?
		@since		2016-07-08 14:27:43
	**/
	public function has_types()
	{
		return defined( 'WPCF_VERSION' );
	}

	/**
		@brief		Is the views plugin installed?
		@since		2016-07-08 14:31:34
	**/
	public function has_views()
	{
		return defined( 'WPV_VERSION' );
	}
}
