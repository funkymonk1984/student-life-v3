<?php get_header(); ?>
	<div id="content">
		<div id="inner-content" class="container">
			<div class="row">
				<main id="main" class="col-xs-12 col-sm-12 col-md-8 col-lg-8" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<header class="article-header">
							<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
						</header> <?php // end article header ?>
						<section class="entry-content cf" itemprop="articleBody">
							<?php the_content(); ?>
						</section>
						<footer class="article-footer"></footer>
					</article>
					<?php endwhile; endif; ?>
				</main>
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
<?php get_footer(); ?>
