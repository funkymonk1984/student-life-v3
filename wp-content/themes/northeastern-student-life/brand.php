<?php $brand = get_field('branding_color','option'); ?>

<style>



	/* Brand Drivers */

	.brand-color {
		color:<?php echo $brand; ?> !important;
	}

	.brand-color-hover:hover {
		color:<?php echo $brand; ?> !important;
	}

	.brand-background {
		background-color: <?php echo $brand; ?> !important;
	}




	/* Dynamic Site Elements */

	.sidebar a,
	.menu-item a:hover {
		color:<?php echo $brand; ?> !important;
	}




	/* Events */

	.tribe-events-button,
	.tribe-events-calendar td.tribe-events-present div[id*="tribe-events-daynum-"],
	.tribe-events-grid .tribe-grid-header .tribe-week-today {
		background-color: <?php echo $brand; ?> !important;
	}

	.tribe-events-month-event-title a,
	.tribe-events-tooltip h4,
	.tribe-events-nav-previous a,
	.tribe-events-nav-next a,
	.tribe-events-event-categories a {
		color:<?php echo $brand; ?> !important;
	}

	/*.sub-page header {
		background-color: <?php echo $brand; ?> !important;
	}*/

</style>