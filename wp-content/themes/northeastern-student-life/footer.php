			<footer class="footer brand-background" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
				<!-- <div class="footer-logo container-fluid padding-0">
					<div class="container">
						<img alt="Northeastern Logo" src="<?php the_field('footer_logo', 'option'); ?>">
					</div>
				</div> -->
				<div id="inner-footer" class="container-fluid padding-0">
					<div class="container">
						<div class="row">
							<div class="footer-widget col">
						
								<?php if ( is_active_sidebar( 'footer-widget-1' ) ) : ?>
									<?php dynamic_sidebar( 'footer-widget-1' ); ?>
								<?php endif; ?>
						
							</div>
							<div class="footer-widget col">
						
								<?php if ( is_active_sidebar( 'footer-widget-2' ) ) : ?>
									<?php dynamic_sidebar( 'footer-widget-2' ); ?>
								<?php endif; ?>
						
							</div>
							<div class="footer-widget col">
						
								<?php if ( is_active_sidebar( 'footer-widget-3' ) ) : ?>
									<?php dynamic_sidebar( 'footer-widget-3' ); ?>
								<?php endif; ?>
						
								<div class="widget">
						
									<?php if( have_rows('social', 'option') ): ?>
										<h4 class="widgettitle">Social</h4>
									 	<?php while ( have_rows('social','option') ) : the_row(); ?>
									 		<div class="social row pull-left">
										 		<a class="pull-left" target="_blank" href="<?php the_sub_field('social_account_link'); ; ?>">
										        	<i class="fa <?php the_sub_field('social_account_icon'); ; ?>" aria-hidden="true"></i>
										        </a>
									        </div>
									    <?php endwhile; ?>
									    <div class="clearfix"></div>
									<?php endif; ?>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="universal-footer">
						<div class="wrapper container padding-0">
							<div class="left col-sm-12 col-md-4 col-lg-4">
								<a class="logo" href="http://www.northeastern.edu"></a>
							</div>
							<div class="right hidden-xs hidden-sm hidden-md col-sm-12 col-md-8 col-lg-8">
								<ul class="info">
									<li>
										<a href="http://myneu.neu.edu/cp/home/displaylogin" onclick="that=this;_gaq.push(['_trackEvent','outbound','click',that.href]);setTimeout(function() { location.href=that.href }, 200);return false;">MyNEU</a>
									</li>
									<li>
										<a href="http://www.northeastern.edu/findfacultystaff" onclick="that=this;_gaq.push(['_trackEvent','outbound','click',that.href]);setTimeout(function() { location.href=that.href }, 200);return false;">Find Faculty &amp; Staff</a>
									</li>
									<li>
										<a href="http://www.northeastern.edu/neuhome/adminlinks/findaz.html" onclick="that=this;_gaq.push(['_trackEvent','outbound','click',that.href]);setTimeout(function() { location.href=that.href }, 200);return false;">Find A-Z</a>
									</li>
									<li>
										<a href="http://www.northeastern.edu/emergency/index.html" onclick="that=this;_gaq.push(['_trackEvent','outbound','click',that.href]);setTimeout(function() { location.href=that.href }, 200);return false;">Emergency Information</a>
									</li>
									<li>
										<a href="http://www.northeastern.edu/search/index.html" onclick="that=this;_gaq.push(['_trackEvent','outbound','click',that.href]);setTimeout(function() { location.href=that.href }, 200);return false;">Search</a>
									</li>
									<li>
										<a href="http://assistive.usablenet.com/tt/http://www.northeastern.edu/index.html" onclick="that=this;_gaq.push(['_trackEvent','outbound','click',that.href]);setTimeout(function() { location.href=that.href }, 200);return false;">Text Only</a>
									</li>
									<li>
										<a href="http://www.northeastern.edu/privacy/index.html" onclick="that=this;_gaq.push(['_trackEvent','outbound','click',that.href]);setTimeout(function() { location.href=that.href }, 200);return false;">Privacy</a>
									</li>
									
								</ul>
								<div class="clearfix"></div>
								<p class="contact">360 Huntington Ave., Boston, Massachusetts 02115 · 617.373.2000 · TTY 617.373.3768<br> © <span class="the-year"><?php echo date("Y"); ?></span>&nbsp;Northeastern University</p>
							</div>
							<div class="clear"></div>
						</div>
					</div>
				</div>
				
				<!-- <div class="copyright container-fluid padding-0">
					<p class="text-center source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>.</p>
				</div> -->

			</footer>
		</div>

		<?php // all js scripts are loaded in library/bones.php ?>

		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min.js"</script>

		<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
		
		<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
		
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
		
		<?php wp_footer(); ?>

	</body>
</html> <!-- end of site. what a ride! -->
