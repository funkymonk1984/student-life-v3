

$category       = get_field( 'events_calendar_categories' );
?>
<?php if( $category ): $slug = $category->slug;?>
        <?php get_header(); ?>
        <style>
        <?php // todo remove this into an include that registered on calendar instantiation ?>

        #container .header:first-child{
                display: none;
        }

        #container .footer:last-child{
                display: none;
        }
        </style>
</head>
<?php
echo do_shortcode( '[tribe_events view="list" category="' . $category->slug .'"]' );
?>
        <?php get_footer(); ?>
        <script>
        new pym.Child({ polling: 100 });
        </script>
<?php endif // $category ?>
<?php //calendar-iframe-events-page.php ?>
