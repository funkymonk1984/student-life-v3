<?php if( have_rows('home_content') ): ?>
 
    <?php while ( have_rows('home_content') ) : the_row(); ?>
 
        <?php if( get_row_layout() == 'home_explore' ): ?>
 
        	<?php include('home-content/home-explore.php'); ?>
        
        <?php elseif( get_row_layout() == 'home_image' ): ?>

        	<?php include('home-content/home-image.php'); ?> 
        
        <?php //elseif( get_row_layout() == 'home_social' ): ?> 
        	
        <?php elseif( get_row_layout() == 'home_staff' ): ?> 
        	
        	<?php include('home-content/home-staff.php'); ?>

        <?php endif; ?>

    <?php endwhile; ?>

<?php endif; ?>