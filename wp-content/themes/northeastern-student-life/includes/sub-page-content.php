<?php if( have_rows('sub_page_content') ): ?>

    <?php while ( have_rows('sub_page_content') ) : the_row(); ?>

        <?php if( get_row_layout() == 'full_width_text_block' ): ?>

        	<?php include('sub-page-content/full-width-text.php'); ?>

        <?php elseif( get_row_layout() == 'assymetrical_text_and_image_block' ): ?>
        	
        	<?php include('sub-page-content/assymetrical-text-image.php');?>

        <?php elseif( get_row_layout() == 'simple_text_columns' ): ?>
        	
        	<?php include('sub-page-content/simple-text-columns.php');?>

        <?php elseif( get_row_layout() == 'simple_full_bleed_image'): ?>
        	
        	<?php include('sub-page-content/simple-full-bleed-image.php');?>

        <?php elseif( get_row_layout() == 'dynamic_text_image_cta_blocks'): ?>
            
            <?php include('sub-page-content/dynamic-text-image-cta-blocks.php');?>

        <?php elseif( get_row_layout() == 'simple_carousel' ): ?>
            
            <?php include('sub-page-content/simple-carousel.php');?>

        <?php elseif( get_row_layout() == 'full_bleed_call_to_action'): ?>
            
            <?php include('sub-page-content/full-bleed-call-to-action.php');?>

        <?php elseif( get_row_layout() == 'video_embed' ): ?>
            
            <?php include('sub-page-content/video-embed.php');?>

        <?php elseif( get_row_layout() == 'simple_gallery' ): ?>
            
            <?php include('sub-page-content/simple-gallery.php');?>

        <?php elseif( get_row_layout() == 'simple_alert_box' ): ?>
            
            <?php include('sub-page-content/simple-alert-box.php');?>

        <?php elseif( get_row_layout() == 'simple_checklist' ): ?>
            
            <?php include('sub-page-content/simple-checklist.php');?>

        <?php elseif( get_row_layout() == 'frequently_asked_questions'): ?>
            
            <?php include('sub-page-content/frequently-asked-questions.php');?>

        <?php elseif( get_row_layout() == 'google_map_embed' ): ?>
            
            <?php include('sub-page-content/google-map-embed.php');?>

        <?php elseif( get_row_layout() == 'events_calendar' ): ?>
            
            <?php include('sub-page-content/events-calendar.php');?>

        <?php elseif( get_row_layout() == 'flexible_staff_module'): ?>
            
            <?php include('sub-page-content/flexible-staff-module.php');?>

        <?php elseif( get_row_layout() == 'simple_wysiwyg'): ?>
            
            <?php include('sub-page-content/simple-wysiwyg.php');?>

        <?php elseif( get_row_layout() == 'flexible_carousel'): ?>
            
            <?php include('sub-page-content/flexible-carousel.php');?>

        <?php endif; ?>

    <?php endwhile; ?>

<?php endif; ?>