<section class="home-explore container-fluid">

	<div class="container text-center" style="margin-bottom:2.4rem;">
		<h2 class="">Explore</h2>
		<h4 class="lead">
			Meet new people, find a new hobby, and be a part of something on campus. Northeastern is home to almost 400 student organizations. Browse through the list and explore all Northeastern has to offer.
		</h4>
	</div>

	<?php if( have_rows('student_life_organizations', 'option') ): $i == 0; ?>

		<?php while ( have_rows('student_life_organizations', 'option') ) : the_row(); $i++; ?>

			<?php if( get_row_layout() == 'student_life_organization_block'): ?>

				<div style="background-image:url('<?php the_sub_field('student_life_organization_block_image'); ?>');" id="student-life-organization-block-<?=$i;?>" class="student-life-organization-block container">
					
					<div class="content">
							
						<h3 class="white text-center styled-header student-life-organization-block-title">
							<?php the_sub_field('student_life_organization_block_title'); ?>
						</h3>

						<?php if( have_rows('student_life_organizations') ): ?>

							<ul class="text-left student-life-organization-block-list">

								<?php while ( have_rows('student_life_organizations') ) : the_row(); ?>

								<li class="">
									<a target="_blank" href="<?php the_sub_field('student_life_organization_link'); ?>">
										<i class='fa fa-angle-right pull-left' aria-hidden='true'></i>
										<?php the_sub_field('student_life_organization_title'); ?>
									</a>
								</li>

								<?php endwhile; ?>

							</ul>
							<div class="clearfix"></div>  

						<?php endif; ?>

						<p class="lead hidden-xs student-life-organization-block-quote">
							<?php the_sub_field('student_life_organization_block_quote'); ?>
						</p>

					</div>
					<div class="overlay" style="background-color:<?php the_sub_field('student_life_organization_block_brand'); ?>;"></div>
				</div>

			<?php endif; ?>

		<?php endwhile; ?>

	<?php endif; ?>

</section>