<section class="home-staff container-fluid">
	<div class="container">
		<div class="row justify-content-center">

			<?php if( have_rows('staff','option') ): ?>

			 	<?php while ( have_rows('staff','option') ) : the_row(); ?>

			 		<div class="staff-block-outer col-xs-6 col-sm-4 col-md-3 col-lg-2">
				 		<div class="staff-block">
				 			<div class="image" style="background-image:url('<?php the_sub_field('staff_photo'); ?>');"></div>
				        	<h4 class="lead text-center">
				        		<?php the_sub_field('staff_first_name'); ?><br />
				        		<span style="font-weight:700;">
				        			<?php the_sub_field('staff_last_name'); ?>
				        		</span>
				        	</h4>
				        </div>
			        </div>

			    <?php endwhile; ?>

			<?php endif; ?>

			
		</div>
		<a href="<?php the_sub_field('home_staff_button'); ?>" class="btn float-right">See All &raquo;</a>
		<div class="clearfix"></div>
	</div>
</section>