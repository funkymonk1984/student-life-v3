<?php if( have_rows('hero_modules') ): ?>

    <?php while ( have_rows('hero_modules') ) : the_row(); ?>

        <?php if( get_row_layout() == 'home_hero' ): ?>

        	<?php include('hero-modules/home-hero.php'); ?>

        <?php elseif( get_row_layout() == 'sub_page_hero' ): ?>
        	
        	<?php include('hero-modules/sub-page-hero.php');?>

        <?php elseif( get_row_layout() == 'simple_text_hero' ): ?>
        	
        	<?php include('hero-modules/simple-text-hero.php');?>

        <?php endif; ?>

    <?php endwhile; ?>

<?php endif; ?>