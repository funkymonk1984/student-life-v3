<?php if( get_row_layout() == 'sub_page_hero' ): ?>
	
	<div class="hero-unit brand-background sub-page-hero container-fluid text-center padding-0" style="background-image:url('<?php the_sub_field('sub_page_hero_image'); ?>');">
	
		<div class="content">
	
			<?php if( get_sub_field('sub_page_hero_title') ): ?>
	
				<h1 class="white">
					<?php the_sub_field('sub_page_hero_title'); ?>
	
				</h1>
			<?php endif; ?>
	
			<?php if( get_sub_field('sub_page_hero_subtitle') ): ?>
	
				<p class="lead white hidden-xs hidden-sm">
					<?php the_sub_field('sub_page_hero_subtitle'); ?>
				</p>
	
			<?php endif; ?>
		</div>
		
		<?php if( get_sub_field('sub_page_hero_image') ): ?>
			<div class="overlay"></div>
		<?php endif; ?>
	
	</div>

<?php endif; ?>