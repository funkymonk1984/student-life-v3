<?php if( get_row_layout() == 'simple_text_hero' ): ?>
	
	<div class="jumbotron jumbotron-fluid brand-background simple-text-hero">
	
		<div class="container">
	
			<?php if( get_sub_field('sub_page_hero_title') ) { ?>
	
				<h1 class="display-3 white">
					<?php the_sub_field('sub_page_hero_title'); ?>
				</h1>

			<?php } else { ?>

				<h1 class="display-3 white bold">
					<?php the_title();?>
				</h1>

			<?php } ?>

			<?php if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('<p id="breadcrumbs">','</p>');
			} ?>
	
		</div>
	
	</div>

<?php endif; ?>