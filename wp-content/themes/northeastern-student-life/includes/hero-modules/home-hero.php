<?php if( get_row_layout() == 'home_hero' ): ?>

	<?php if( get_sub_field('home_hero_video_image_toggle') == 'video' ): ?>

		<div class="easyhtml5video video-hero">
			<video muted="muted" loop="loop" autoplay="autoplay" style="width:100%" title="Northeastern unviersity Student Life">

				<?php if( get_sub_field('home_video_webm') ): ?>
					<source src="<?php the_sub_field('home_video_webm'); ?>" type="video/webm" />
				<?php endif; ?>
				
				<?php if( get_sub_field('home_video_mp4') ): ?>
					<source src="<?php the_sub_field('home_video_mp4'); ?>" type="video/mp4" />
				<?php endif; ?>
				
				<?php if( get_sub_field('home_video_mov') ): ?>
					<source src="<?php the_sub_field('home_video_mov'); ?>" type="video/mov" />
				<?php endif; ?>
			</video>

			<?php if( have_rows('home_hero_video_text') ): ?>

				<div class="video-overlay overlay">
					<div class="content carousel slide" data-ride="carousel">
						<div class="carousel-inner" role="listbox">

						    <?php while ( have_rows('home_hero_video_text') ) : the_row(); ?>

								<div class="carousel-item text-center">
									<div class="carousel-caption d-none d-md-block">
								        <h2 class="handwritten">

								        	<?php the_sub_field('home_hero_video_text_header'); ?>

								        </h2>
								        <h3 class="handwritten">

								        	<?php the_sub_field('home_hero_video_text_subheader'); ?>

								        </h3>
							        </div>
						        </div>

						    <?php endwhile; ?>

						</div>
					</div>
				</div>

			<?php endif; ?>
		</div>
	
	<?php endif; ?>

	<?php if( get_sub_field('home_hero_video_image_toggle') == 'image' ): ?>

		<div class="image-hero">
	    	<div class="text-overlay overlay fade-in-on-page-load">
		    	<div class="inner container">
	    			
	    			<?php if( get_sub_field('home_image_text_title') ): ?>
	    				<h2 class="text-center handwritten"><?php the_sub_field('home_image_text_title'); ?></h2>
	    			<?php endif; ?>

	    			<?php if( get_sub_field('home_image_text_sub_title') ): ?>
	    				<h3 class="text-center handwritten"><?php the_sub_field('home_image_text_sub_title'); ?></h3>
	    			<?php endif; ?>

		    	</div>
	    	</div>
	    </div>

		<script>

			jQuery(document).ready(function($) {

				$(".image-hero").backstretch([
			    	<?php if( have_rows('home_hero_images') ): $i = 0; ?>
					    <?php while ( have_rows('home_hero_images') ) : the_row(); $i++; ?>
							<?php if ($i > 1) { echo ", "; } ?><?php echo '"';?><?php the_sub_field("home_hero_image");?><?php echo '"';?>
					    <?php endwhile; ?>
						<?php else : ?>
					<?php endif; ?>

				], {duration: 3000, fade: 750});
			});

		</script>

	<?php endif; ?>

	<?php if( get_sub_field('home_hero_event_toggle') ): ?>

		<div class="hero-inner container-fluid padding-0">
			<div class="events-wrapper brand-background fade-in-on-page-load">
				<div class="container-fluid">
					<div class="event-wrapper-outer">
						<div class="event-wrapper row container-fluid d-flex align-content-center justify-content-center">

							<?php global $post; ?>

							<?php $events = tribe_get_events( array(
							    'posts_per_page' => 3,
							) ); ?>
							 
							<?php foreach ( $events as $event ) : setup_postdata( $event ); ?>
							
								<div class="event col hidden-md-down">
									<div class="float-left d-flex align-content-start">
										<i class="fa fa-calendar" aria-hidden="true"></i>
									</div>
									<div class='float-left'>
								    	<h4 class='lead'>
								    		<a href='<?php echo get_permalink( $event->ID ); ?>'><?php echo $event->post_title ?></a>
								    	</h4>
								    	<h4 class='lead'><?php echo mysql2date('F jS', $event->post_date) ?></h4>
							    		<p class='hidden-lg-down'><?php echo $event->post_excerpt ?></p>
							    	</div>
							    </div>
							
							<?php endforeach; 
							wp_reset_postdata();?>
							
							<div class="event-button col">
		        				<a href="<?php the_field('events_overview_page', 'options'); ?>" class="margin-0-auto btn transparent btn-default btn-lg">
									<span class="hidden-md-down">See All</span> Events</span> &raquo;
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	<?php endif; ?>

<?php endif; ?>
