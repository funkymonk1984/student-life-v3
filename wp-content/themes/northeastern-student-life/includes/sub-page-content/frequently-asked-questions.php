<?php

//-----------------------------------------------------
// FAQ's
//-----------------------------------------------------

?>

<section class="entry-content frequently-asked-questions" itemprop="articleBody">
  <div class="container">

    <?php if( get_sub_field('frequently_asked_questions_title') ): ?>
      <h2 class="text-center">
        <?php the_sub_field('frequently_asked_questions_title'); ?>
      </h2>
    <?php endif; ?>

    <?php if( get_sub_field('frequently_asked_questions_sub_title') ): ?>
      <h4 class="lead text-center">
        <?php the_sub_field('frequently_asked_questions_sub_title'); ?>
      </h4>
    <?php endif; ?>

    <?php if( have_rows('frequently_asked_questions_repeater') ): $j=0; ?>
      
      <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
      
        <?php while ( have_rows('frequently_asked_questions_repeater') ) : the_row(); $j++ ?>

        <div class="card">
          <div class="card-header" role="tab" id="headingOne">
          <h5 class="mb-0">
            <a class="brand-color" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?=$j;?>" aria-expanded="true" aria-controls="collapse-<?=$j;?>">
              <?php the_sub_field('frequently_asked_questions_question'); ?>
            </a>
          </h5>
          </div>

          <div id="collapse-<?=$j;?>" class="collapse" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-block">
              <?php the_sub_field('frequently_asked_questions_answer'); ?>
            </div>
          </div>
        </div>

        <?php endwhile; ?>

      </div>

    <?php endif; ?>

  </div>

</section>