<?php

//-----------------------------------------------------
// Simple Full Bleed Image
//-----------------------------------------------------

?>

<section class="entry-content simple-full-bleed-image" itemprop="articleBody">

  <div class="container-fluid simple-full-bleed-image-inner" style="background-image:url('<?php the_sub_field("simple_full_bleed_image");?>');">

  </div>
	
</section>