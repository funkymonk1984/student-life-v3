<?php

//-----------------------------------------------------
// Simple Checklist
//-----------------------------------------------------

?>

<section class="entry-content simple-checklist" itemprop="articleBody">
	<div class="container">
		<div class="row">
		<div class="col-5">
			
			<?php if( get_sub_field('simple_checklist_title') ): ?>
				<h3>
					<?php the_sub_field('simple_checklist_title'); ?>
				</h3>
			<?php endif; ?>
			
			<?php if( get_sub_field('simple_checklist_body') ): ?>
				<p>
					<?php the_sub_field('simple_checklist_body'); ?>
				</p>
			<?php endif; ?>

		</div>
		<div class="col-7">
			<?php $icon = get_sub_field('simple_checklist_icon'); ?>
			<?php if( have_rows('simple_checklist_repeater') ): ?>
				
				<ul>
					<?php while ( have_rows('simple_checklist_repeater') ) : the_row(); ?>
						<li>
							<i class="brand-color fa <?=$icon;?>" aria-hidden="true"></i> <?php the_sub_field('simple_checklist_item'); ?>
						</li>
					<?php endwhile; ?>
				</ul>

			<?php endif; ?>
		</div>
	</div>
</section>