<?php

//-----------------------------------------------------
// Simple Text Columns
//-----------------------------------------------------

?>

<section class="entry-content simple-text-columns" itemprop="articleBody">

	<div class="container">

        <?php if( have_rows('simple_text_column_repeater') ): $i=0; ?>
            <div class="row">
            
                <?php while ( have_rows('simple_text_column_repeater') ) : the_row(); $i++;  ?>

                    <div class="col">
                   
                       <?php if( get_sub_field('simple_text_column_title') ): ?>
                           <h4 class="lead brand-color"><?php the_sub_field('simple_text_column_title'); ?></h4>
                       <?php endif; ?>

                       <?php if( get_sub_field('simple_text_column_body') ): ?>
                           <p><?php the_sub_field('simple_text_column_body'); ?></p>
                       <?php endif; ?>

                   </div>

                <?php endwhile; ?>
            
            </div>

        <?php endif; ?>
	
    </div>
	
</section>