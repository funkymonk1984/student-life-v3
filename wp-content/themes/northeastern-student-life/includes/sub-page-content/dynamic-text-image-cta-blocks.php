<?php

//-----------------------------------------------------
// Dynamic Text/Image/CTA Blocks
//-----------------------------------------------------

?>

<section class="entry-content dynamic-text-image-cta-blocks" itemprop="articleBody">

	<div class="container">

    <?php $dynamicTextImageCtaBlockToggle = get_sub_field('dynamic_text_image_cta_block_toggle'); ?>

    <?php if( have_rows('dynamic_text_image_cta_block_repeater') ): ?>
        
      <div class="row">
      
          <?php while ( have_rows('dynamic_text_image_cta_block_repeater') ) : the_row(); ?>

              <div class="col">

                <?php if( $dynamicTextImageCtaBlockToggle && in_array('image', $dynamicTextImageCtaBlockToggle) ): ?>
                
                  <?php if( get_sub_field('dynamic_text_image_cta_block_image') ): ?>

                    <?php if( $dynamicTextImageCtaBlockToggle && in_array('cta', $dynamicTextImageCtaBlockToggle) ): ?>
                
                      <?php if( get_sub_field('dynamic_text_image_cta_block_cta') ): ?>
                    
                        <a href="<?php the_sub_field('dynamic_text_image_cta_block_cta'); ?>">
                    
                      <?php endif; ?>
                    
                    <?php endif; ?>

                    <div class="image" style="background-image:url('<?php the_sub_field('dynamic_text_image_cta_block_image'); ?>');"></div>

                    <?php if( $dynamicTextImageCtaBlockToggle && in_array('cta', $dynamicTextImageCtaBlockToggle) ): ?>
                
                      <?php if( get_sub_field('dynamic_text_image_cta_block_cta') ): ?>
                    
                        </a>
                    
                      <?php endif; ?>
                    
                    <?php endif; ?>
                
                  <?php endif; ?>
                
                <?php endif; ?>
             
                <?php if( $dynamicTextImageCtaBlockToggle && in_array('title', $dynamicTextImageCtaBlockToggle) ): ?>
               
                  <?php if( get_sub_field('dynamic_text_image_cta_block_title') ): ?>
               
                    <h3 class="brand-color"><?php the_sub_field('dynamic_text_image_cta_block_title'); ?></h3>
               
                  <?php endif; ?>
               
                <?php endif; ?>

                <?php if( $dynamicTextImageCtaBlockToggle && in_array('subtitle', $dynamicTextImageCtaBlockToggle) ): ?>
                
                  <?php if( get_sub_field('dynamic_text_image_cta_block_sub_title') ): ?>
                
                    <h5 class=""><?php the_sub_field('dynamic_text_image_cta_block_sub_title'); ?></h5>
                
                  <?php endif; ?>
                
                <?php endif; ?>

                <?php if( $dynamicTextImageCtaBlockToggle && in_array('body', $dynamicTextImageCtaBlockToggle) ): ?>
                
                  <?php if( get_sub_field('dynamic_text_image_cta_block_body') ): ?>
                
                    <p><?php the_sub_field('dynamic_text_image_cta_block_body'); ?></p>
                
                  <?php endif; ?>
                
                <?php endif; ?>

                <?php if( $dynamicTextImageCtaBlockToggle && in_array('cta', $dynamicTextImageCtaBlockToggle) ): ?>
                
                  <?php if( get_sub_field('dynamic_text_image_cta_block_cta') ): ?>
                
                    <a class="btn" href="<?php the_sub_field('dynamic_text_image_cta_block_cta'); ?>">
                
                      Learn More
                
                    </a>
                
                  <?php endif; ?>
                
                <?php endif; ?>

             </div>

          <?php endwhile; ?>
      
      </div>

    <?php endif; ?>
	
  </div>
	
</section>