<section class="container-fluid flexible-staff-module" itemprop="articleBody">
    
    <div class="container padding-0">
	
		<?php if( get_sub_field('flexible_staff_module_title') ): ?>
	    	<h2 class="text-center">
	    		<?php the_sub_field('flexible_staff_module_title'); ?>
	    	</h2>
		<?php endif; ?>
		
		<?php if( get_sub_field('flexible_staff_module_sub_title') ): ?>
	    	<h4 class="lead text-center" style="margin-bottom:2.4rem;">
	    		<?php the_sub_field('flexible_staff_module_sub_title'); ?>
	    	</h4>
		<?php endif; ?>
	
		<?php if( have_rows('staff','option') ): $i=0 ?>
		
			<div class="row">

			 	<?php while ( have_rows('staff','option') ) : the_row(); $i++ ?>
			
			 		<div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">		
				 		<div class="staff-block">		
				 			<div style="background-image:url('<?php the_sub_field('staff_photo'); ?>');" class="image col-xs-12 col-sm-11 col-md-10 col-lg-9 margin-0-auto" data-toggle="modal" data-target="#myModal<?php echo $i; ?>"></div>			        	
				        	<h4 class="text-center">			        	
				        		<a href="#" data-toggle="modal" data-target="#myModal<?php echo $i; ?>">				        
					        		<?php the_sub_field('staff_first_name'); ?><br />				        
					        		<span style="font-weight:700;">
					        			<?php the_sub_field('staff_last_name'); ?>
					        		</span>			        	
				        		</a>			        	
				        	</h4>
				        	
				        	<div class="modal fade bs-example-modal-lg" id="myModal<?php echo $i; ?><?php // echo $j; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">					
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="text-center modal-title" id="myModalLabel">
												<?php the_sub_field('staff_first_name'); ?> <?php the_sub_field('staff_last_name'); ?>						
									        </h4>						
										</div>						
										
										<div class="modal-body">						
											<div style="background-image:url('<?php the_sub_field('staff_photo'); ?>');" class="image text-center col-xs-8 col-sm-6 col-md-3 col-lg-2"></div>
											<div class="content col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<div class="text-right content hidden-xs col-xs-3 col-sm-3 col-md-2 col-lg-2">
													<p class=""><strong>Title:</strong></p>
													<!-- <p class=""><strong>Phone:</strong></p> -->

													<?php if( get_sub_field('staff_twitter') ): ?>
														<p class=""><strong>Twitter:</strong></p>
													<?php endif; ?>

											        <p class=""><strong>Email:</strong></p>
													<p class=""><strong>Bio:</strong </p>
												</div>
												<div class="content col-xs-12 col-sm-9 col-md-10 col-lg-10">
													<p class="">
											        	<strong><?php the_sub_field('staff_title'); ?></strong>
											        </p>

											        <?php if( get_sub_field('staff_twitter') ): ?>
														<p class="">
															<a target="_blank" href="http://www.twitter.com/<?php $twitter_link = sanitize_title_for_query( get_sub_field('staff_twitter') ); echo esc_attr( $twitter_link ); ?>">
																<?php the_sub_field('staff_twitter'); ?>
															</a>
												        </p>
											        <?php endif; ?>

											        <p class="">
											        	<a href="mailto:<?php the_sub_field('staff_email'); ?>">
											        		<?php the_sub_field('staff_email'); ?>
											        	</a>
											        </p>
													<p class="">
											        	<?php the_sub_field('staff_bio'); ?>
											        </p>
										        </div>
									        </div>
									        <div class="clearfix"></div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn" data-dismiss="modal">Close Window</button>
											<a target="_blank" href="mailto:<?php the_sub_field('staff_email'); ?>" class="btn"><span class="hidden-xs glyphicon glyphicon-envelope" aria-hidden="true"></span> Email <?php the_sub_field('staff_first_name'); ?></a>
										</div>
									</div>
								</div>
							</div>
				        </div>
			        </div>
	
			    <?php endwhile; ?>
	
		    </div>
	
		<?php endif; ?>
	
	</div>
</section>