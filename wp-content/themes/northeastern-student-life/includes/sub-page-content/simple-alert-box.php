<?php

//-----------------------------------------------------
// Simple Alert Box
//-----------------------------------------------------

?>

<section class="entry-content simple-alert-box" itemprop="articleBody">
  <div class="container">
    <div class="alert alert-danger" role="alert">
      
      <?php if( get_sub_field('simple_alert_box_title') ): ?>
        <h3 class="alert-heading">
          <?php the_sub_field('simple_alert_box_title'); ?>
        </h3>
      <?php endif; ?>
      
      <?php if( get_sub_field('simple_alert_box_text') ): ?>
        <p class="mb-0"><?php the_sub_field('simple_alert_box_text'); ?></p>
      <?php endif; ?>
    </div>
  </div>
</section>