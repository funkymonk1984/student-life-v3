<?php

//-----------------------------------------------------
// Flexible Carousel
//-----------------------------------------------------

?>

<section class="entry-content flexible-carousel padding-0" itemprop="articleBody">
  <div class="container-fluid padding-0 brand-background" style="background-image:url('<?php the_sub_field('flexible_carousel_background_image'); ?>');">
    <div class="container">
      
      <div class="header">  
      
        <?php if( get_sub_field('flexible_carousel_title') ): ?>
          <h3 class="white text-center">
            <?php the_sub_field('flexible_carousel_title'); ?>
          </h3>
        <?php endif; ?>
        
        <?php if( get_sub_field('flexible_carousel_sub_title') ): ?>
          <h5 class="white text-center">
            <?php the_sub_field('flexible_carousel_sub_title'); ?>
          </h5>
        <?php endif; ?>
      
      </div>
      
      <div class="row">
        <?php if( have_rows('flexible_carousel_repeater') ): ?>
        
          <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
        
              <?php while ( have_rows('flexible_carousel_repeater') ) : the_row(); ?>
        
                <div class="carousel-item align-items-center">
                  
                  <div class="col-md">
                    <img class="d-block img-fluid" src="<?php the_sub_field('flexible_carousel_image');?> " alt="">
                  </div>

                  <?php if( get_sub_field('flexible_carousel_caption_title') ): ?>
                    
                    <div class="col-md">
                      <div class="carousel-caption d-none d-md-block">
                        <h2 class="brand-color">
                          <?php the_sub_field('flexible_carousel_caption_title'); ?>
                        </h2>
                        <p>
                          <?php the_sub_field('flexible_carousel_caption_sub_title'); ?>
                        </p>
                        <a class="btn" href="#">Learn More</a>
                      </div>
                    </div>

                  <?php endif; ?>
                
                </div>
        
              <?php endwhile; ?>
        
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        <?php endif; ?>
      </div>
    </div>
    
    <?php if( get_sub_field('flexible_carousel_background_image') ): ?>
      <div class="overlay"></div>
    <?php endif; ?>
    
  </div>
</section>