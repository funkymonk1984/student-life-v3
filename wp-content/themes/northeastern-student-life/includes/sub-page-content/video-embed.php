<?php

//-----------------------------------------------------
// Video Embed
//-----------------------------------------------------

?>

<section class="entry-content video-embed" itemprop="articleBody">
  <div class="container">

    <?php if( get_sub_field('video_embed_orientation') == 'video-left' ): ?>
      <div class="row flex-row-reverse">
    <?php endif; ?>

    <?php if( get_sub_field('video_embed_orientation') == 'video-right' ): ?>
      <div class="row">
    <?php endif; ?>

      <div class="col-4">

        <?php if( get_sub_field('video_embed_title') ): ?>
          <h2><?php the_sub_field('video_embed_title'); ?></h2>
        <?php endif; ?>

        <?php if( get_sub_field('video_embed_sub_title') ): ?>
          <h5 class="brand-color"><?php the_sub_field('video_embed_sub_title'); ?></h5>
        <?php endif; ?>

        <?php $videoLink = get_sub_field('video_embed_cta'); ?>
        <a class="btn brand-background" href="echo $videoLink['url']; ?>">
          Learn More
        </a>

      </div>

      <div class="col-8">

        <?php if( get_sub_field('video_embed_url') ): ?>
          
          <div class="embed-responsive embed-responsive-16by9">
            <?php the_sub_field('video_embed_url'); ?>
          </div>

        <?php endif; ?>

      </div>
    </div>
  </div>

</section>