<?php

//-----------------------------------------------------
// Simple Gallery
//-----------------------------------------------------

?>

<section class="entry-content simple-gallery" itemprop="articleBody">
  
  <div class="container">

    <div class="row margin-0">
    
      <?php
        $image_ids = get_sub_field('simple_gallery_photos', false, false);
        $shortcode = '[gallery ids="' . implode(',', $image_ids) . '"]';
        $gallery = do_shortcode( $shortcode );
        echo slb_activate($gallery);
      ?>

    </div>

  </div>

</section>