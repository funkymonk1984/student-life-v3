<?php

//-----------------------------------------------------
// Simple Carousel
//-----------------------------------------------------

?>

<section class="entry-content simple-carousel" itemprop="articleBody">
  <div class="container">

    <?php if( get_sub_field('simple_carousel_orientation') == 'carousel-left' ): ?>
      <div class="row flex-row-reverse">
    <?php endif; ?>

    <?php if( get_sub_field('simple_carousel_orientation') == 'carousel-right' ): ?>
      <div class="row">
    <?php endif; ?>

      <div class="col-4">

        <?php if( get_sub_field('simple_carousel_title') ): ?>
          <h2><?php the_sub_field('simple_carousel_title'); ?></h2>
        <?php endif; ?>

        <?php if( get_sub_field('simple_carousel_body') ): ?>
          <h3 class="brand-color"><?php the_sub_field('simple_carousel_body'); ?></h3>
        <?php endif; ?>

        <?php $carouselLink = get_sub_field('simple_carousel_cta'); ?>
        <a class="btn brand-background" href="echo $carouselLink['url']; ?>">
          Learn More
        </a>

      </div>

      <div class="col-8">

        <?php if( have_rows('simple_carousel_repeater') ): ?>

          <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">

              <?php while ( have_rows('simple_carousel_repeater') ) : the_row(); ?>

                <div class="carousel-item">
                  <img class="d-block img-fluid" src="<?php the_sub_field('simple_carousel_image');?> " alt="">

                  <?php if( get_sub_field('simple_carousel_caption') ): ?>

                    <div class="carousel-caption d-none d-md-block">
                      <h5><?php the_sub_field('simple_carousel_caption'); ?></h5>
                    </div>

                  <?php endif; ?>

                </div>

              <?php endwhile; ?>

            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>

        <?php endif; ?>

      </div>
    </div>
  </div>

</section>