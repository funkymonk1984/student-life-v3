<?php

//-----------------------------------------------------
// Assymentrical Text/Image Block
//-----------------------------------------------------

?>

<section class="sub-page-content entry-content container-fluid" itemprop="articleBody">

	<div class="container">

		<?php $selected = get_sub_field('assymetrical_text_and_image_block_orientation'); ?>
		
		<?php if( in_array('image-left', $selected) ) { ?>
		
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5 pull-left">
		
		<?php } ?>
		
		<?php if( in_array('image-right', $selected) ) { ?>
		
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5 pull-right">
		
		<?php } ?>
    	
    		<img src="<?php the_sub_field('assymetrical_text_and_image_block_image'); ?>">
    	
    	</div>

    	<?php $selected = get_sub_field('assymetrical_text_and_image_block_orientation'); ?>
		
		<?php if( in_array('image-left', $selected) ) { ?>
		
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-7 pull-right">
		
		<?php } ?>
		
		<?php if( in_array('image-right', $selected) ) { ?>
		
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-7 pull-left">
		
		<?php } ?>
    	
    		<?php if( get_sub_field('assymetrical_text_and_image_block_subtitle') ): ?>
	       
	        	<h2>
	       
	        		<?php the_sub_field('assymetrical_text_and_image_block_title'); ?>
	       
	        	</h2>
        	
        	<?php endif; ?>
        	
        	<?php if( get_sub_field('assymetrical_text_and_image_block_subtitle') ): ?>
	        
	        	<h4 class="brand-color lead">
	        
	        		<?php the_sub_field('assymetrical_text_and_image_block_subtitle'); ?>
	        
	        	</h4>
        	
        	<?php endif; ?>
        	
        	<?php if( get_sub_field('assymetrical_text_and_image_block_subtitle') ): ?>
	        
	        	<p>
	        
	        		<?php the_sub_field('assymetrical_text_and_image_block_content'); ?>
	        
	        	</p>
        	
        	<?php endif; ?>
    	
    	</div>
	
	</div>

	<div class="clearfix"></div>

</section>