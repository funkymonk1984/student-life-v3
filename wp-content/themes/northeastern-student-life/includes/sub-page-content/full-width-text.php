<?php

//-----------------------------------------------------
// Full Width Text
//-----------------------------------------------------

?>

<section class="sub-page-content entry-content full-width-text" itemprop="articleBody">

	<div class="container justify-content-center">

    	<?php if( get_sub_field('full_width_text_block_title') ): ?>
        
        	<h2 class="text-center">
        
        		<?php the_sub_field('full_width_text_block_title'); ?>
        
        	</h2>
    	
        <?php endif; ?>

    	<?php if( get_sub_field('full_width_text_block_subtitle') ): ?>
        
        	<h4 class="brand-color lead text-center">
        
        		<?php the_sub_field('full_width_text_block_subtitle'); ?>
        
        	</h4>
    	
        <?php endif; ?>

    	<?php if( get_sub_field('full_width_text_block_content') ): ?>
        
        	<p class="text-center">
        
        		<?php the_sub_field('full_width_text_block_content'); ?>
        
        	</p>

    	<?php endif; ?>

        <?php $link = get_sub_field('full_width_text_block_button'); ?>
        <p class="text-center">
            <a class=" btn" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>">
                <?php echo $link['title'];?>
            </a>
        </p>
	
    </div>
	
</section>