<?php

//-----------------------------------------------------
// Events Calendar
//-----------------------------------------------------

?>

<section class="sub-page-content entry-content events-calendar" itemprop="articleBody">

	<div class="container">
        <h2 class="text-center">Events</h2>
        
        <?php if( get_sub_field('events_calendar_select') == 'tribe_mini_calendar' ): ?>
            <?php echo do_shortcode( '[tribe_mini_calendar]' ); ?>
        <?php endif; ?>

        <?php if( get_sub_field('events_calendar_select') == 'tribe_events_list' ): ?>
            <?php echo do_shortcode( '[tribe_events_list]' ); ?>
        <?php endif; ?>

        <?php if( get_sub_field('events_calendar_select') == 'tribe_featured_venue' ): ?>
            <?php echo do_shortcode( '[tribe_featured_venue]' ); ?>
        <?php endif; ?>

        <?php if( get_sub_field('events_calendar_select') == 'tribe_event_countdown' ): ?>
            <?php echo do_shortcode( '[tribe_event_countdown]' ); ?>
        <?php endif; ?>

        <?php if( get_sub_field('events_calendar_select') == 'tribe_this_week' ): ?>
            <?php echo do_shortcode( '[tribe_this_week]' ); ?>
        <?php endif; ?>
    </div>
	
</section>