<?php

//-----------------------------------------------------
// Simple WYSIWYG
//-----------------------------------------------------

?>

<section class="entry-content simple-wysiwyg" itemprop="articleBody">
  <div class="container">
      
      <?php if( get_sub_field('simple_wysiwyg') ): ?>
        <?php the_sub_field('simple_wysiwyg'); ?>
      <?php endif; ?>

  </div>
</section>