<?php

//-----------------------------------------------------
// Full Bleed Call to Action
//-----------------------------------------------------

?>

<section class="sub-page-content entry-content full-call-to-action brand-background" itemprop="articleBody">

	<div class="container justify-content-center">

    	<?php if( get_sub_field('full_bleed_call_to_action_title') ): ?>
        
        	<h2 class="white text-center">
        
        		<?php the_sub_field('full_bleed_call_to_action_title'); ?>
        
        	</h2>
    	
        <?php endif; ?>

    	<?php if( get_sub_field('full_bleed_call_to_action_sub_title') ): ?>
        
        	<h4 class="white lead text-center">
        
        		<?php the_sub_field('full_bleed_call_to_action_sub_title'); ?>
        
        	</h4>
    	
        <?php endif; ?>

    	<?php if( get_sub_field('full_bleed_call_to_action_content') ): ?>
        
        	<p class="white text-center">
        
        		<?php the_sub_field('full_bleed_call_to_action_content'); ?>
        
        	</p>

    	<?php endif; ?>

        <?php $link = get_sub_field('full_bleed_call_to_action_button'); ?>
        <p class="text-center">
            <a class=" btn" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>">
                <?php echo $link['title'];?>
            </a>
        </p>
	
    </div>
	
</section>