<?php
/*
 Template Name: Explore
*/
?>

<?php get_header(); ?>
	<div id="content">

		<?php include ("includes/hero-modules.php");?>
		
		<div id="inner-content" class="container-fluid padding-0">
			<main id="main" class="" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
				
				<?php if( have_rows('explore_page_content') ): ?>
				    <?php while ( have_rows('explore_page_content') ) : the_row(); ?>
				        <?php if( get_row_layout() == 'explore_featured_organization' ): ?>
				        	<section class="explore-page-content container" itemprop="articleBody">
					        	<div class="container-fluid padding-0">
						        	<?php if( get_sub_field('explore_featured_organization_title') ): ?>
							        	<h2 class="text-center">
							        		<?php the_sub_field('explore_featured_organization_title'); ?>
							        	</h2>
						        	<?php endif; ?>
						        	<?php if( get_sub_field('explore_featured_organization_subtitle') ): ?>
							        	<h4 class="red col-xs-12 col-sm-10 col-md-8 col-lg-8 margin-0-auto lead text-center">
							        		<?php the_sub_field('explore_featured_organization_subtitle'); ?>
							        	</h4>
						        	<?php endif; ?>
					        	</div>
				        	</section>
				        <?php // elseif( get_row_layout() == 'sub_page_text_and_image_block' ): ?>
				        	
				        <?php endif; ?>
				    <?php endwhile; ?>
				<?php endif; ?>
					
				<section class="student-life-organization-filters container-fluid">
					<h3 style="margin-bottom:1em;" class="styled-header text-center">Browse Organizations</h3>

					<?php if( have_rows('student_life_organizations', 'option') ): ?>
					    <?php while ( have_rows('student_life_organizations', 'option') ) : the_row(); ?>
					        <?php if( get_row_layout() == 'student_life_organization_block' ): ?>
				        		
				        		<?php 
									$category = get_sub_field('student_life_organization_block_title');
									$category = strtolower($category);
									$category = preg_replace("/[^a-z0-9_\s-]/", "", $category);
									$category = preg_replace("/[\s-]+/", " ", $category);
									$category = preg_replace("/[\s_]/", "-", $category);
								?>

				        		<button style="background-color:<?php the_sub_field('student_life_organization_block_brand');?>;" data-category="<?php echo $category; ?>" class="styled-header <?php echo $category; ?>-btn btn">
				        			<?php the_sub_field('student_life_organization_block_title');?>
				        		</button>
					        <?php endif; ?>
					        	
					    <?php endwhile; ?>
					    <button class="active all btn">All</button>
					<?php endif; ?>
				</section>

				<section class="student-life-organization-buckets container-fluid padding-0">
					<div class="bucket-outer container">
						<?php if( have_rows('student_life_organizations', 'option') ): ?>
						    <?php while ( have_rows('student_life_organizations', 'option') ) : the_row(); ?>
						        <?php if( get_row_layout() == 'student_life_organization_block' ): ?>
					        		
					        		<?php 
										$category = get_sub_field('student_life_organization_block_title');
										$category = strtolower($category);
										$category = preg_replace("/[^a-z0-9_\s-]/", "", $category);
										$category = preg_replace("/[\s-]+/", " ", $category);
										$category = preg_replace("/[\s_]/", "-", $category);
									?>
					        		
				        			<div class="<?php echo $category; ?>-bucket bucket-inner row">
						        		<div class="image col-xs-12 col-sm-12 col-md-5 col-lg-5">
						        			<img src="<?php the_sub_field('student_life_organization_block_image');?>">
						        		</div>
						        		<div class="content col-xs-12 col-sm-12 col-md-7 col-lg-7">
							        		<h3 class="styled-header" style="color:<?php the_sub_field('student_life_organization_block_brand');?>;">
							        			<?php the_sub_field('student_life_organization_block_title');?>
							        		</h3>
							        		<p>
							        			<?php the_sub_field('student_life_organization_block_description');?>
							        		</p>
						        		</div>
						        		<div class="clearfix"></div>
					        		</div>
						        <?php endif; ?>
						    <?php endwhile; ?>
						    		
		        			<div class="all-bucket bucket-inner" style="display:block;">
				        		<div class="col-xs-12 col-sm-12 col-md-10 col-lg-8 margin-0-auto">
					        		<h3 class="styled-header text-center" style="color:<?php the_sub_field('student_life_organization_block_brand');?>;">
					        			All
					        		</h3>
					        		<p class="text-center">Check out all Student Life organizations, or use the filters above to narrow your search.</p>
				        		</div>
			        		</div>
			        		<div class="clearfix"></div>

						<?php endif; ?>
					</div>
					<div class="clearfix"></div>
				</section>
				<div class="clearfix"></div>

				<section class="student-life-organization container-fluid padding-0">
					<?php if( have_rows('student_life_organizations', 'option') ):  $j=0 ?>
					    <?php while ( have_rows('student_life_organizations', 'option') ) : the_row(); $j++ ?>
					        <?php if( get_row_layout() == 'student_life_organization_block' ): ?>
				        		<div>
					        		<?php 
										$category = get_sub_field('student_life_organization_block_title');
										$category = strtolower($category);
										$category = preg_replace("/[^a-z0-9_\s-]/", "", $category);
										$category = preg_replace("/[\s-]+/", " ", $category);
										$category = preg_replace("/[\s_]/", "-", $category);

										$color = get_sub_field('student_life_organization_block_brand');
										$image = get_sub_field('student_life_organization_image');

									?>	

									<div class="content padding-0">
						        		<?php if( have_rows('student_life_organizations') ): $i=0; ?>
						        			<div class="row" style="margin:0;">
											 	<?php while ( have_rows('student_life_organizations') ) : the_row(); $i++; ?>

											 		<?php if( get_sub_field('student_life_organization_image') ) { ?>
														<?php 
															$image_thumb = get_sub_field('student_life_organization_image'); 
															$image_med = $image_thumb['sizes']['medium'];
															$image_large = $image_thumb['sizes']['large'];
														?>
	                            						<div style="border-top:thick solid <?php echo $color; ?>; background-image:url('<?php echo $image_med; ?>');" data-category="<?php echo $category; ?>" class="<?php echo $category; ?>-block block col-xs-12 col-sm-6 col-md-4 col-lg-3 padding-0">
													   	<?php } else { ?>
													   		 <div style="background-color:<?php echo $color; ?>;" data-category="<?php echo $category; ?>" class="<?php echo $category; ?>-block block col-xs-12 col-sm-6 col-md-4 col-lg-3 padding-0">
													   	<?php } ?>

												        <a href="#" data-toggle="modal" data-target="#myModal<?php echo $i; ?><?php echo $j; ?>">
													        <h5 class="text-center styled-header">
													        	<?php the_sub_field('student_life_organization_title'); ?>
													        </h5>
												        </a>
														
														<?php if( get_sub_field('student_life_organization_description') ): ?>											        
													        <p class="text-center">
													        	<?php //the_sub_field('student_life_organization_description'); ?>
													        </p>
												    	<?php endif; ?>
												        <a href="#" class="overlay"></a>
											        </div>
											    <?php endwhile; ?>
										    </div>
										<?php endif; ?>
									</div>
									<div class="image">
										<img src="<?php the_sub_field('student_life_organization_block_image');?>">
									</div>
								</div>
					        <?php endif; ?>
					    <?php endwhile; ?>
					<?php endif; ?>


					<?php if( have_rows('student_life_organizations', 'option') ):  $j=0 ?>
					    <?php while ( have_rows('student_life_organizations', 'option') ) : the_row(); $j++ ?>
					        <?php if( get_row_layout() == 'student_life_organization_block' ): ?>
				        		<?php 
									$color = get_sub_field('student_life_organization_block_brand');
								?>

				        		<?php if( have_rows('student_life_organizations') ): $i=0; ?>
								 	<?php while ( have_rows('student_life_organizations') ) : the_row(); $i++; ?>

								        <div class="modal fade bs-example-modal-lg" id="myModal<?php echo $i; ?><?php echo $j; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
											<div class="modal-dialog modal-lg" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
														<h4 class="modal-title" id="myModalLabel">
															<?php the_sub_field('student_life_organization_title'); ?>
														</h4>
													</div>
													<div class="modal-body">
														<div style="border-top:thick solid <?php echo $color; ?>;" class="image">
                                                        <?php if( get_sub_field('student_life_organization_image') ) { ?>
															<?php
																$image_thumb = get_sub_field('student_life_organization_image');
																$image_med = $image_thumb['sizes']['medium'];
																$image_large = $image_thumb['sizes']['large'];
															?>
															<img  src="<?php echo $image_large; ?>">
														<?php } ?>
														</div>
														<div class="content">
															<p class="">
													        	<?php the_sub_field('student_life_organization_description'); ?>
													        </p>
												        </div>
													</div>
													<div class="modal-footer">
														<button type="button" class="btn" data-dismiss="modal">Close Window</button>
														<?php if( get_sub_field('student_life_organization_link') ): ?>
															<a target="_blank" href="<?php the_sub_field('student_life_organization_link'); ?>" class="btn">Take Me There!</a>
														<?php endif; ?>
													</div>
												</div>
											</div>
										</div>
								    <?php endwhile; ?>
								<?php endif; ?>
					        <?php endif; ?>
					    <?php endwhile; ?>
					<?php endif; ?>

				</section>
			</main>
		</div>
	</div>
<?php get_footer(); ?>
