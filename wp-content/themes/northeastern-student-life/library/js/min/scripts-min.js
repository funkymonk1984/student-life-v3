jQuery(document).ready(function($) {
    if (!$("body").hasClass("home")) {
        $("body").addClass("sub-page");
    }
    $(".fade-in-on-page-load").delay(2400).fadeIn("slow");
    $(".navbar-toggler-open").click(function() {
        console.log("Drawer");
        $(".drawer-content").animate({
            height:"toggle"
        }, 480, function() {});
        $(this).hide(60);
        $(".navbar-toggler-close").delay(120).show(60);
        $(this).addClass("active");
    });
    $(".navbar-toggler-close").click(function() {
        $(".drawer-content").animate({
            height:"toggle"
        }, 480, function() {});
        $(this).hide(60);
        $(".navbar-toggler-open").delay(120).show(60);
        $(this).removeClass("active");
    });
    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"), results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return "";
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
    var query = getParameterByName("q");
    if (query !== null) {
        $(".block").fadeOut("fast").promise().done(function() {
            $("." + query + "-block").delay(360).fadeIn("fast");
        });
        $(".bucket-inner").fadeOut("fast").promise().done(function() {
            $("." + query + "-bucket").delay(180).fadeIn("fast");
        });
        $("." + query + "-btn").addClass("active");
        $(".hero-unit").hide();
    } else {
        $(".hero-unit").show();
        $(".page-template-page-explore .hero-unit").fadeTo("slow", 1);
    }
    $(".student-life-organization-filters .btn").click(function() {
        var category = $(this).attr("data-category");
        if ($(this).hasClass("all")) {
            $(".block").fadeIn("fast");
            $(".bucket-inner").fadeOut("fast").promise().done(function() {
                $(".all-bucket").delay(180).fadeIn("fast");
            });
        } else {
            $(".block").fadeOut("fast").promise().done(function() {
                $("." + category + "-block").fadeIn("fast");
            });
            $(".bucket-inner").fadeOut("fast").promise().done(function() {
                $("." + category + "-bucket").fadeIn("fast");
            });
        }
        if (query === null) {
            $("html, body").animate({
                scrollTop:$(".student-life-organization-filters").offset().top
            }, 480);
        }
        $(".student-life-organization-filters .btn").removeClass("active");
        $(this).addClass("active");
    });
    $(".styled-header").each(function() {
        var $this = $(this);
        $this.html($this.html().replace(/(\S+)\s*$/, '<span class="gotcha">$1</span>'));
    });
    $(".event p").each(function() {
        if ($(this).text().length > 50) $(this).text($(this).text().substring(0, 50) + "...");
    });
    $(".carousel-item:nth-child(1)").addClass("active");
    $("dl.gallery-item").addClass("col-lg-2 col-md-2 col-sm-2 col-xs-6");
    $(".gallery br").remove();
    $(".gallery").addClass("row");
    function truncateString(string, limit, breakChar, rightPad) {
        if (string.length <= limit) return string;
        var substr = string.substr(0, limit);
        if ((breakPoint = substr.lastIndexOf(breakChar)) >= 0) {
            if (breakPoint < string.length - 1) {
                return string.substr(0, breakPoint) + rightPad;
            }
        }
    }
    $(".home .lwn a").each(function() {
        var text = $(this).html();
        var textShortened = truncateString(text, 24, " ", "...");
        $(this).html(textShortened);
    });
});
//# sourceMappingURL=./scripts-min.js.map