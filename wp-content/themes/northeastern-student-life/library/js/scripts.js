/*
 * Scripts File
 * Author: Sean Davies
*/

jQuery(document).ready(function($) {


  /*
   * Add "sub-page" class to all
   * non-home page screens
  */

  if(!$("body").hasClass("home")){
    $("body").addClass("sub-page");
  }


  //$(".home header").delay(500).fadeIn("slow");
  //$(".carousel").delay(1000).fadeIn("slow");

  $(".fade-in-on-page-load").delay(2400).fadeIn('slow');


  //$('.sub-menu li').prepend('<i class="fa fa-chevron-right" aria-hidden="true"></i>');







  /*
   * Drawer open/close
  */

  $( ".navbar-toggler-open" ).click(function() {

    console.log("Drawer");

    $( ".drawer-content" ).animate({
      height: "toggle"
    }, 480, function() {
      // Animation complete.
    });

    $(this).hide(60);
    $(".navbar-toggler-close").delay(120).show(60);

    $(this).addClass("active");

  });




  /*
   * More drawer open/close stuff
  */

  $( ".navbar-toggler-close" ).click(function() {

    $( ".drawer-content" ).animate({
      height: "toggle"
    }, 480, function() {
      // Animation complete.
    });

    $(this).hide(60);
    $(".navbar-toggler-open").delay(120).show(60);

    $(this).removeClass("active");

  });








  /*
   * Kind of a hacky function to get a query
   * string and use it to show the correct Explore
   * content. This should probably be re-written
  */

  function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }

  var query = getParameterByName('q');

  if (query !== null){

      $(".block").fadeOut( "fast").promise().done(function() {
        $("." + query + "-block").delay(360).fadeIn("fast");
      });

      $(".bucket-inner").fadeOut( "fast").promise().done(function() {
        $("." + query + "-bucket").delay(180).fadeIn("fast");
      });

      $("." + query + "-btn").addClass("active");

      $(".hero-unit").hide();

  } else {

      $(".hero-unit").show();
      $(".page-template-page-explore .hero-unit").fadeTo("slow" , 1);

  }


  /*
   * Explore "filters" - though it's just an illuuusion!
  */

  $( ".student-life-organization-filters .btn" ).click(function() {

    var category = $(this).attr("data-category");

    if ( $( this ).hasClass( "all" ) ) {

      $(".block").fadeIn("fast");

      $(".bucket-inner").fadeOut( "fast").promise().done(function() {
        $(".all-bucket").delay(180).fadeIn("fast");
      });

    } else {

      $(".block").fadeOut( "fast").promise().done(function() {
        $("." + category + "-block").fadeIn("fast");
      });

      $(".bucket-inner").fadeOut( "fast").promise().done(function() {
        $("." + category + "-bucket").fadeIn("fast");
      });

    }

    // Behaves slightly different if a filter is active on load
    if (query === null){

      $('html, body').animate({
          scrollTop: $(".student-life-organization-filters").offset().top
      }, 480);
    }

    $(".student-life-organization-filters .btn" ).removeClass("active");
    $(this).addClass("active");

  });





  // $(".hero-unit").fadeTo("slow" , 1);
  // $(".hero-unit .content").delay(480).fadeTo("slow" , 1);



  /*
    * Add class to a text element, it'll bold the last word
  */

  $('.styled-header').each(function() {
      var $this = $(this);
      $this.html($this.html().replace(/(\S+)\s*$/, '<span class="gotcha">$1</span>'));
  });


  $(".event p").each (function () {
    if ($(this).text().length > 50)
      $(this).text($(this).text().substring(0,50) + '...');
  });




  /*
    * Dynamic carousel needs first slide made active manually
  */

  $(".carousel-item:nth-child(1)").addClass("active");






  $("dl.gallery-item").addClass('col-lg-2 col-md-2 col-sm-2 col-xs-6');
  $(".gallery br").remove();
  $(".gallery").addClass('row');



  /*
    * Several random functions to clean up
    * the events feed
  */

  // $(".home #lw").addClass("row");

  // $(".home .lwe").addClass("col-sm-6 col-md-4 col-lg-4");
  // $(".sub-page .lwe").addClass("col-sm-12 col-md-6 col-lg-6");

  // $(".lwn span").addClass("text-left");
  // //$(".sub-page .lwn span").addClass("pull-right");

  // $(".lwn a").addClass("text-left");
  // $(".lwn a").addClass("excerpt text-left");
  // //$(".sub-page .lwn a").addClass("pull-left");

  // $(".sub-page .lwd").addClass("text-left");

  // $(".sub-page .lwd span").addClass("hidden-xs");

  // $(".home .lwe").eq(2).addClass("hidden-sm");

  // $(".home .lwe").prepend( "<i class='fa fa-calendar pull-left' aria-hidden='true'></i>" );

  // $(".home .lwe i").addClass("hidden-sm hidden-md");

  // $(".lwn span").each(function() {
  //     var item = $(this);
  //     item.next().after(item);
  // });




  /*
    * Shorten the crazy long event titles
  */

  function truncateString (string, limit, breakChar, rightPad) {
    if (string.length <= limit) return string;

    var substr = string.substr(0, limit);
    if ((breakPoint = substr.lastIndexOf(breakChar)) >= 0) {
        if (breakPoint < string.length -1) {
            return string.substr(0, breakPoint) + rightPad;
        }
    }
  }

  $( ".home .lwn a" ).each(function() {
    var text = $(this).html();
    var textShortened = truncateString(text, 24 , ' ', '...');
    $(this).html(textShortened);
  });




}); /* end of as page load scripts */
