// jQuery(document).ready(function($) {
//     if (!$("body").hasClass("home")) {
//         $("body").addClass("sub-page");
//     }
//     $(".home header").delay(500).fadeIn("slow");
//     $(".carousel").delay(1e3).fadeIn("slow");
//     $(".events-wrapper").delay(1500).fadeIn("slow");
//     $(".navbar-toggler").click(function() {
//         console.log("Drawer");
//         $(".drawer-content").animate({
//             height:"toggle"
//         }, 480, function() {});
//         $(this).hide(60);
//         $(".glyphicon-remove.navbar-toggle").delay(120).show(60);
//         $(this).addClass("active");
//     });
//     $(".glyphicon-remove.navbar-toggle").click(function() {
//         $(".drawer-content").animate({
//             height:"toggle"
//         }, 480, function() {});
//         $(this).hide(60);
//         $("button.navbar-toggle").delay(120).show(60);
//         $(this).removeClass("active");
//     });
//     if ($(window).width() < 960) {
//         $(".student-life-organization-block-title").css({
//             cursor:"pointer"
//         });
//         $(".student-life-organization-block-title").click(function() {
//             $(this).siblings(".student-life-organization-block-list").slideDown(120);
//             $(this).parents().siblings().children(".student-life-organization-block-list").slideUp(120);
//         });
//     } else {}
//     function getParameterByName(name, url) {
//         if (!url) url = window.location.href;
//         name = name.replace(/[\[\]]/g, "\\$&");
//         var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"), results = regex.exec(url);
//         if (!results) return null;
//         if (!results[2]) return "";
//         return decodeURIComponent(results[2].replace(/\+/g, " "));
//     }
//     var query = getParameterByName("q");
//     if (query !== null) {
//         $(".block").fadeOut("fast").promise().done(function() {
//             $("." + query + "-block").delay(360).fadeIn("fast");
//         });
//         $(".bucket-inner").fadeOut("fast").promise().done(function() {
//             $("." + query + "-bucket").delay(180).fadeIn("fast");
//         });
//         $("." + query + "-btn").addClass("active");
//         $(".hero-unit").hide();
//     } else {
//         $(".hero-unit").show();
//         $(".page-template-page-explore .hero-unit").fadeTo("slow", 1);
//     }
//     $(".student-life-organization-filters .btn").click(function() {
//         var category = $(this).attr("data-category");
//         if ($(this).hasClass("all")) {
//             $(".block").fadeIn("fast");
//             $(".bucket-inner").fadeOut("fast").promise().done(function() {
//                 $(".all-bucket").delay(180).fadeIn("fast");
//             });
//         } else {
//             $(".block").fadeOut("fast").promise().done(function() {
//                 $("." + category + "-block").fadeIn("fast");
//             });
//             $(".bucket-inner").fadeOut("fast").promise().done(function() {
//                 $("." + category + "-bucket").fadeIn("fast");
//             });
//         }
//         if (query === null) {
//             $("html, body").animate({
//                 scrollTop:$(".student-life-organization-filters").offset().top
//             }, 480);
//         }
//         $(".student-life-organization-filters .btn").removeClass("active");
//         $(this).addClass("active");
//     });
//     $(".styled-header").each(function() {
//         var $this = $(this);
//         $this.html($this.html().replace(/(\S+)\s*$/, '<span class="gotcha">$1</span>'));
//     });
//     $(".item:nth-child(1)").addClass("active");
//     $(".home #lw").addClass("row");
//     $(".home .lwe").addClass("col-sm-6 col-md-4 col-lg-4");
//     $(".sub-page .lwe").addClass("col-sm-12 col-md-6 col-lg-6");
//     $(".lwn span").addClass("text-left");
//     $(".lwn a").addClass("text-left");
//     $(".lwn a").addClass("excerpt text-left");
//     $(".sub-page .lwd").addClass("text-left");
//     $(".sub-page .lwd span").addClass("hidden-xs");
//     $(".home .lwe").eq(2).addClass("hidden-sm");
//     $(".home .lwe").prepend("<i class='fa fa-calendar pull-left' aria-hidden='true'></i>");
//     $(".home .lwe i").addClass("hidden-sm hidden-md");
//     $(".lwn span").each(function() {
//         var item = $(this);
//         item.next().after(item);
//     });
//     function truncateString(string, limit, breakChar, rightPad) {
//         if (string.length <= limit) return string;
//         var substr = string.substr(0, limit);
//         if ((breakPoint = substr.lastIndexOf(breakChar)) >= 0) {
//             if (breakPoint < string.length - 1) {
//                 return string.substr(0, breakPoint) + rightPad;
//             }
//         }
//     }
//     $(".home .lwn a").each(function() {
//         var text = $(this).html();
//         var textShortened = truncateString(text, 24, " ", "...");
//         $(this).html(textShortened);
//     });
//     var links = $(".tribe-teaser-container a");
//     $.each(links, function() {
//         $(this).attr("target", "_blank");
//     });
//     var referralSiteQuery = getParameterByName("slref"), referralSiteLocal = localStorage.slref;
//     if (referralSiteQuery) {
//         $("#header-dest").load("https://sl.tidly.co/" + referralSiteQuery + "  #inner-header");
//         localStorage.slref = referralSiteQuery;
//     } else if (referralSiteLocal) {
//         $("#header-dest").load("https://sl.tidly.co/" + referralSiteLocal + "  #inner-header");
//     } else {
//         $("#header-dest").load("https://sl.tidly.co/  #inner-header");
//     }
// });