<?php
/*
 Template Name: About
*/
?>


<?php get_header(); ?>
	<div id="content">

		<?php include ("includes/hero-modules.php");?>

		<div id="inner-content" class="container-fluid padding-0">
			<main id="main" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php include ("includes/sub-page-content.php");?>

				<?php endwhile; else : ?>
				<?php endif; ?>
			</main>
			<?php // get_sidebar(); ?>
		</div>
	</div>
<?php get_footer(); ?>
