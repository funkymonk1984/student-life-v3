<?php
/*
 Template Name: Sub Home
*/
?>


<?php get_header(); ?>

	<div id="content">

		<div class="hero">
			
			<?php include('includes/hero-modules.php'); ?>

		</div>

		<div id="inner-content" class="container-fluid padding-0">
			
			<main id="main" class="" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

				<?php include ("includes/sub-page-content.php");?>
			
			</main>
		
		</div>
	
	</div>

<?php get_footer(); ?>
