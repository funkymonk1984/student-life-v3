<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 * @version  4.3
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural = tribe_get_event_label_plural();

$event_id = get_the_ID();

?>

<div id="tribe-events-content" class="tribe-events-single padding-0">
	
	<div class="jumbotron jumbotron-fluid brand-background simple-text-hero">
		<div class="container">
			<h1 class="display-4 white bold">
				<?php the_title(); ?>
			</h1>
			<?php if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('<p id="breadcrumbs">','</p>');
			} ?>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-7">
			
				<?php the_post_thumbnail('full', array('class' => 'img-fluid'));?>

				<?php while ( have_posts() ) :  the_post(); ?>
					
					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

						<?php do_action( 'tribe_events_single_event_before_the_content' ) ?>
						
						<div class="tribe-events-single-event-description tribe-events-content">
							<?php the_content(); ?>
						</div>
						
						<?php // do_action( 'tribe_events_single_event_after_the_content' ) ?>
					
					</div>
					
					<?php if ( get_post_type() == Tribe__Events__Main::POSTTYPE && tribe_get_option( 'showComments', false ) ) comments_template() ?>
				
				<?php endwhile; ?>
			
			</div>

			<div class="col-5">
				
				<!-- Notices -->
				<?php tribe_the_notices() ?>

				<div class="tribe-events-schedule tribe-clearfix">
				
					<?php echo tribe_events_event_schedule_details( $event_id, '<h2 class="lead">', '</h2>' ); ?>
				
					<?php /* if ( tribe_get_cost() ) : ?>
				
						<h3 class="tribe-events-cost">
							Cost: <?php echo tribe_get_cost( null, true ) ?>
						</h3>
				
					<?php endif; */ ?>
				
				</div>

				<!-- Event header -->
				<div id="tribe-events-header" <?php tribe_events_the_header_attributes() ?>>
				
					<!-- Navigation -->
				
					<h3 class="lead tribe-events-visuallyhidden">
						<?php printf( esc_html__( '%s Navigation', 'the-events-calendar' ), $events_label_singular ); ?>
					</h3>
				
					<ul class="tribe-events-sub-nav">
						<li class="tribe-events-nav-previous">
				
							<?php tribe_the_prev_event_link( '<span>&laquo;</span> %title%' ) ?>
				
						</li>
				
						<li class="tribe-events-nav-next">
				
							<?php tribe_the_next_event_link( '%title% <span>&raquo;</span>' ) ?>
				
						</li>
					</ul>
				</div>

				<?php while ( have_posts() ) :  the_post(); ?>
				
					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						
						<!-- .tribe-events-single-event-description -->
						<?php // do_action( 'tribe_events_single_event_after_the_content' ) ?>

						<!-- Event meta -->
						<?php do_action( 'tribe_events_single_event_before_the_meta' ) ?>
						
						<?php tribe_get_template_part( 'modules/meta' ); ?>
						
						<?php do_action( 'tribe_events_single_event_after_the_meta' ) ?>
					
					</div> <!-- #post-x -->
					
					<?php if ( get_post_type() == Tribe__Events__Main::POSTTYPE && tribe_get_option( 'showComments', false ) ) comments_template() ?>
				
				<?php endwhile; ?>

				<a class="float-right white btn" href="<?php echo esc_url( tribe_get_events_link() ); ?>"> 
					<?php printf( '&laquo; ' . esc_html_x( 'All %s', '%s Events plural label', 'the-events-calendar' ), $events_label_plural ); ?>
				</a>

			</div>
		</div>
		<!-- Event footer -->
		<div id="tribe-events-footer">
			<!-- Navigation -->
			<h3 class="tribe-events-visuallyhidden"><?php printf( esc_html__( '%s Navigation', 'the-events-calendar' ), $events_label_singular ); ?></h3>
			<ul class="tribe-events-sub-nav">
				<li class="tribe-events-nav-previous"><?php tribe_the_prev_event_link( '<span>&laquo;</span> %title%' ) ?></li>
				<li class="tribe-events-nav-next"><?php tribe_the_next_event_link( '%title% <span>&raquo;</span>' ) ?></li>
			</ul>
			<!-- .tribe-events-sub-nav -->
		</div>
		<!-- #tribe-events-footer -->
	</div>


	


	

</div><!-- #tribe-events-content -->
