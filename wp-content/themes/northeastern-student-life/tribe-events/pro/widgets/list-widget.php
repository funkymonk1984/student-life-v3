<?php
/**
 * Events Pro List Widget Template
 * This is the template for the output of the events list widget.
 * All the items are turned on and off through the widget admin.
 * There is currently no default styling, which is highly needed.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/pro/widgets/list-widget.php
 *
 * When the template is loaded, the following vars are set:
 *
 * @version 4.1.1
 *
 * @var string $start
 * @var string $end
 * @var string $venue
 * @var string $address
 * @var string $city
 * @var string $state
 * @var string $province
 * @var string $zip
 * @var string $country
 * @var string $phone
 * @var string $cost
 * @var array  $instance
 *
 * @package TribeEventsCalendarPro
 *


if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

// Retrieves the posts used in the List Widget loop.
$posts = tribe_get_list_widget_events();

if(count($posts) == 1){
	$columnPushClass = 'col-sm-push-4 col-md-push-6';
}

if(count($posts) == 2){
	$columnPushClass = 'col-md-push-3';
}

// The URL for this widget's "View More" link.
$link_to_all = tribe_events_get_list_widget_view_all_link( $instance );

// Check if any posts were found.
if ( isset( $posts ) && $posts ) :
?>
<div class = 'tribe-teaser-container'>
<div class='container-fluid'>
<div class='tribe-row-of-events'>
<?php $postCounter = 0; ?>
<?php
foreach ( $posts as $post ) :
if ($postCounter <= 2):
setup_postdata( $post );
do_action( 'tribe_events_widget_list_inside_before_loop' ); ?>

<!-- Event  -->
<div class="tribe-event-container <?php tribe_events_event_classes() ?>">
<?php tribe_get_template_part( 'pro/widgets/modules/single-event', null, $instance ) ?>
</div>

<?php do_action( 'tribe_events_widget_list_inside_after_loop' ) ?>
<?php $postCounter++;?>
<?php endif; ?>
<?php endforeach ?>

<div class = 'col-xs-12 col-sm-4 col-md-3 <?php echo $columnPushClass;?> tribe-events-widget-link-container'>
<p class="tribe-events-widget-link">
<?//php esc_attr_e( esc_url( $link_to_all ) ) ?>
<a class = 'btn' href="<?php echo network_site_url(); ?>" rel="bookmark">
<?php esc_html_e( 'See All', 'tribe-events-calendar-pro' ) ?> <span class="visible-xs-* hidden-sm hidden-md hidden-lg">Events</span> &raquo;
</a>
</p>
</div>
</div>
</div>
</div>
<?php
// No Events were found.
else:
?>
<p class = 'no-events-text white text-center'><?php printf( __( 'There are no upcoming %s at this time.', 'tribe-events-calendar-pro' ), tribe_get_event_label_plural_lowercase() ); ?></p>
<?php
endif;

// Cleanup. Do not remove this.
wp_reset_postdata();


 */?>

<?php
/**
 * Events Pro List Widget Template
 * This is the template for the output of the events list widget.
 * All the items are turned on and off through the widget admin.
 * There is currently no default styling, which is highly needed.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/pro/widgets/list-widget.php
 *
 * When the template is loaded, the following vars are set:
 *
 * @version 4.1.1
 *
 * @var string $start
 * @var string $end
 * @var string $venue
 * @var string $address
 * @var string $city
 * @var string $state
 * @var string $province
 * @var string $zip
 * @var string $country
 * @var string $phone
 * @var string $cost
 * @var array  $instance
 *
 * @package TribeEventsCalendarPro
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

// Retrieves the posts used in the List Widget loop.
$posts = tribe_get_list_widget_events();

// The URL for this widget's "View More" link.
$link_to_all = tribe_events_get_list_widget_view_all_link( $instance );

// Check if any posts were found.
if ( isset( $posts ) && $posts ) : ?>
	
	<div class="row">

		<?php foreach ( $posts as $post ) :
			setup_postdata( $post );
			do_action( 'tribe_events_widget_list_inside_before_loop' ); ?>

			<!-- Event  -->
			<div class="col-md <?php tribe_events_event_classes() ?>">
				<?php tribe_get_template_part( 'pro/widgets/modules/single-event', null, $instance ) ?>
			</div>

			<?php do_action( 'tribe_events_widget_list_inside_after_loop' ) ?>

		<?php endforeach ?>

	</div>

	<p class="tribe-events-widget-link">
		<a class="btn float-right brand-background" href="<?php esc_attr_e( esc_url( $link_to_all ) ) ?>" rel="bookmark">
			<?php esc_html_e( 'View More&hellip;', 'tribe-events-calendar-pro' ) ?>
		</a>
	</p>

<?php
// No Events were found.
else:
?>
	<p><?php printf( __( 'There are no upcoming %s at this time.', 'tribe-events-calendar-pro' ), tribe_get_event_label_plural_lowercase() ); ?></p>
<?php
endif;

// Cleanup. Do not remove this.
wp_reset_postdata();
