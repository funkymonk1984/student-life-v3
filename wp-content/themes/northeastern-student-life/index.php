<?php get_header(); ?>

	<?php // Hi Anthony ?>
	
	<div id="content">

		<?php if( have_rows('blog_content','option') ): ?>
	
		    <?php while ( have_rows('blog_content','option') ) : the_row(); ?>
	
		        <?php if( get_row_layout() == 'blog_hero' ): ?>
	
		        	<div class="hero jumbotron jumbotron-fluid" style="background-image:url('<?php the_sub_field('blog_hero_image'); ?>');">
		        		<div class="content">
	
		        			<?php if( get_sub_field('blog_hero_title') ): ?>
								<h1 class="text-center brand-color">
									<?php the_sub_field('blog_hero_title'); ?>
								</h1>
							<?php endif; ?>
		
						</div>
						<div class="overlay"></div>
					</div>
			        	
		        <?php endif; ?>
		
		    <?php endwhile; ?>
		
		<?php endif; ?>

		<div id="inner-content" class="container">
			<div class="row">
				<main id="main" class="col-xs-12 col-sm-12 col-md-8 col-lg-8" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
				
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				
						<article id="post-<?php the_ID(); ?>" <?php post_class( '	' ); ?> role="article">
							<header class="article-header">
								<div class="row">
									<?php the_post_thumbnail('short-thumbnail', array('class' => 'img-fluid')); ?>
								</div>
								<h2 class="">
									<a class="brand-color" href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
								</h2>
								<p class="byline brand-color entry-meta vcard">
									<?php printf( __( '', 'bonestheme' ).' %1$s %2$s',
									/* the time the post was published */
									'<time class="updated entry-time" datetime="' . get_the_time('Y-m-d') . '" itemprop="datePublished">' . get_the_time(get_option('date_format')) . '</time>',
									/* the author of the post */
									'<span class="by">'.__( 'by', 'bonestheme').'</span> <span class="entry-author author" itemprop="author" itemscope itemptype="http://schema.org/Person">' . get_the_author_link( get_the_author_meta( 'display_name' ) ) . '</span>'
									); ?>
								</p>
							</header>
							<section style="background:transparent;" class="entry-content">
								<?php the_excerpt(); ?>
							</section>
							<footer class="article-footer">
								<!-- <p class="footer-comment-count">
								<?php comments_number( __( '<span>No</span> Comments', 'bonestheme' ), __( '<span>One</span> Comment', 'bonestheme' ), __( '<span>%</span> Comments', 'bonestheme' ) );?>
								</p> -->
								<?php printf( '<p class="footer-category">' . __('Categories', 'bonestheme' ) . ': %1$s</p>' , get_the_category_list(', ') ); ?>
								<?php the_tags( '<p class="footer-tags tags"><span class="tags-title">' . __( 'Tags:', 'bonestheme' ) . '</span> ', ', ', '</p>' ); ?>
							</footer>
						</article>
						<?php endwhile; ?>
						
						<?php bones_page_navi(); ?>
						
						<?php else : ?>
						
						<!-- <article id="post-not-found" class="hentry cf">
							<header class="article-header">
								<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
							</header>
							<section class="entry-content">
								<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
							</section>
							<footer class="article-footer">
								<p><?php _e( 'This is the error message in the index.php template.', 'bonestheme' ); ?></p>
							</footer>
						</article> -->

					<?php endif; ?>
			
				</main>
			
				<?php get_sidebar(); ?>
			
			</div>
		</div>
	</div>
<?php get_footer(); ?>
