				<div id="sidebar1" class="sidebar col-xs-12 col-sm-12 col-md-4 col-lg-4" role="complementary">
					<?php if ( is_active_sidebar( 'primary-sidebar' ) ) : ?>
						<?php dynamic_sidebar( 'primary-sidebar' ); ?>
					<?php else : ?>
						<?php
							/*
							 * This content shows up if there are no widgets defined in the backend.
							*/
						?>
						<div class="no-widgets">
							<p><?php _e( 'This is a widget ready area. Add some and they will appear here.', 'bonestheme' );  ?></p>
						</div>
					<?php endif; ?>
				</div>
