<?php
/*
 Template Name: Home
*/
?>

<?php get_header(); ?>

	<div id="content">
		<div class="hero">
			
			<?php include('includes/hero-modules.php'); ?>

		</div>

		<div id="inner-content" class="container-fluid padding-0">
			
			<main id="main" class="padding-0 container-fluid" role="main">
				
				<?php include ('includes/home-content.php');?>

			</main>

		</div>
	</div>

<?php get_footer(); ?>
