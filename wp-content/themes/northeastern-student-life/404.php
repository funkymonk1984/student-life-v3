<?php get_header(); ?>
	<div id="content">
		<?php if( have_rows('blog_content','option') ): ?>
	
		    <?php while ( have_rows('blog_content','option') ) : the_row(); ?>
	
		        <?php if( get_row_layout() == 'blog_hero' ): ?>
	
		        	<div class="hero jumbotron jumbotron-fluid" style="background-image:url('<?php the_sub_field('blog_hero_image'); ?>');">
		        		<div class="content">
	
		        			<?php if( get_sub_field('blog_hero_title') ): ?>
								<h1 class="text-center display-4">
									<?php _e( 'Ruh Roh! A 404 Error!', 'bonestheme' ); ?>
								</h1>
							<?php endif; ?>
		
						</div>
						<div class="overlay"></div>
					</div>
			        	
		        <?php endif; ?>
		
		    <?php endwhile; ?>
		
		<?php endif; ?>
		<div id="inner-content" class="container">
			<main id="main" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
				<article id="post-not-found" class="hentry">
					<header class="article-header">
						
					</header>
					<section class="entry-content">
						<p class="text-center">
							<?php _e( 'You seem to have stumbled across a page that has gone missing, or no longer exists! Give it another shot, or reach out to our team for some help!', 'bonestheme' ); ?>
						</p>
					</section>
					<!-- <section class="search text-center">
						<p class="text-center">
							<?php get_search_form(); ?>
						</p>
					</section> -->
					<footer class="article-footer">
						<p class="text-center">
							<?php _e( '404', 'bonestheme' ); ?>
						</p>
					</footer>
				</article>
			</main>
		</div>
	</div>
<?php get_footer(); ?>
