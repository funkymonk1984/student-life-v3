<?php
/*
 Template Name: Events
*/
?>

<?php get_header(); ?>
	<div id="content">

		<?php // include ("includes/hero-modules.php");?>
				
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
			<section class="container padding-0 events">
				
				<?php the_content(); ?>

			</section>

		<?php endwhile;?>
		<?php endif; ?>

	</div>
<?php get_footer(); ?>
