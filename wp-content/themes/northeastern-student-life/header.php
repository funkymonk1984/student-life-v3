<!doctype html>

<!--[if lt IE 7]>
	<html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7">
<![endif]-->

<!--[if (IE 7)&!(IEMobile)]>
	<html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8">
<![endif]-->

<!--[if (IE 8)&!(IEMobile)]>
	<html <?php language_attributes(); ?> class="no-js lt-ie9">
<![endif]-->

<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>

		<title><?php wp_title(''); ?></title>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
        <meta name="theme-color" content="#121212">

        <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-touch-icon.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

  		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet"> 
  		<link href="https://fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet"> 
  		<link href="https://fonts.googleapis.com/css?family=Gochi+Hand" rel="stylesheet"> 

		<?php // wordpress head functions ?>
		<?php wp_head(); ?>
		<?php // end of wordpress head ?>

		<?php // drop Google Analytics Here ?>
		<?php // end analytics ?>

		<?php include('brand.php');?>

	</head>

	<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">
	
	<!-- Google Tag Manager -->
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WGQLLJ"
	height="0" width="0"
	style="display:none;visibility:hidden"></iframe></noscript>
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','globalDataLayer','GTM-WGQLLJ');</script>
	<!-- End Google Tag Manager -->

		<div id="container">

			<header class="fade-in-on-page-load header" role="banner" itemscope itemtype="http://schema.org/WPHeader">
				
				<?php if( get_field('global_alert','option') ): ?>
					<div class="alert">
						<p class="text-center"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <?php the_field('global_alert','option'); ?></p>
					</div>
				<?php endif; ?>
				
				<div class="drawer container-fluid padding-0">
					<div class="drawer-inner padding-0">
						<div class="drawer-header padding-0">
							<div class="drawer-header-inner container padding-0">
								<nav class="navbar navbar-toggleable-md navbar-light bg-faded d-flex align-items-center justify-content-start">
									
									<p>Division of Student Affairs</p>
									<button class="navbar-toggler navbar-toggler-open navbar-toggler-right visible-xs" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
										<span class="navbar-toggler-icon"></span>
									</button>

									<span class="navbar-toggler navbar-toggler-close fa fa-close" aria-hidden="true"></span>
									
									<a class="navbar-brand brand-color-hover" href="http://www.northeastern.edu" target="_blank">

										<?php
											/*
											* Logo: 
											* You can override the default logo
											* with one uploaded to the General > Header
											* Custom Fields section. Otherwise, it will
											* show the default Northeastern Logo
											*/
										?>

										<?php if( get_field('northeastern_logo', 'option') ) { ?>
										
											<img alt="Northeastern Logo" src="<?php the_field('northeastern_logo', 'option'); ?>">
										
										<?php } else { ?>
										
											<img alt="Northeastern Logo" src="<?php echo get_template_directory_uri(); ?>/library/images/northeastern-university.png" />
										
										<?php } ?>
										
									</a>
								</nav>
							</div>
						</div>
						<div class="drawer-content container">
							<!-- <div class="drawer-content-inner container-fluid"> -->
								<!-- <div class="drawer-menu"> -->
									<h2 class="lead hidden-md-down">Welcome to the Northeastern Student Life Community</h3>
									<p class="hidden-md-down">Browse our organizations, or contact us to help find a community that matches your needs.</p>
							
									<?php wp_nav_menu(array(
										'container' => false, // remove nav container
										'container_class' => 'organizations', // class of container (should you choose to use it)
										'menu' => __( 'Organizations', 'bonestheme' ), // nav name
										'menu_class' => 'organizations', // adding custom nav class
										'theme_location' => 'global-organizations', // where it's located in the theme
										'before' => '', // before the menu
										'after' => '', // after the menu
										'link_before' => '', // before each link
										'link_after' => '', // after each link
										'depth' => 0, // limit the depth of the nav
										'fallback_cb' => '' // fallback function (if there is one)
									)); ?>
									<div class="clearfix"></div>
							
								<!-- </div> -->
							<!-- </div> -->
						</div>
					</div>
				</div>
				<div id="inner-header" class="container">

					<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
						
						<div style="position: relative;">
							<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
								<span class="navbar-toggler-icon"></span>
							</button>
							
							<?php if(!get_field('use_image_as_department_text')): ?>
								<a class="hidden-sm-down navbar-brand brand-color-hover" href="<?php echo home_url(); ?>">
									<i class="fa <?php the_field('organization_icon', 'option'); ?>" aria-hidden="true"></i><?php the_field('organization_title', 'option'); ?>
								</a>

								<a class="hidden-md-up navbar-brand brand-color-hover" href="<?php echo home_url(); ?>">
									<i class="fa <?php the_field('organization_icon', 'option'); ?>" aria-hidden="true"></i><?php the_field('organization_title_abbreviated', 'option'); ?>
								</a>

								<?php else: ?>
								<a class="navbar-brand brand-color-hover" href="<?php echo home_url(); ?>">
									<img alt="" src="<?php the_field('student_life_logo', 'option'); ?>">
								</a>
							<?php endif; ?>
						</div>
						
						<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
							<div class="navbar-nav">
								
								<?php wp_nav_menu(array(
									'container' => false, // remove nav container
									'container_class' => 'menu cf', // class of container (should you choose to use it)
									'menu' => __( 'The Main Menu', 'bonestheme' ), // nav name
									'menu_class' => 'nav top-nav navbar-nav navbar-right', // adding custom nav class
									'theme_location' => 'main-nav', // where it's located in the theme
									'before' => '', // before the menu
									'after' => '', // after the menu
									'link_before' => '', // before each link
									'link_after' => '', // after each link
									'depth' => 0, // limit the depth of the nav
									'fallback_cb' => '' // fallback function (if there is one)
								)); ?>

							</div>
							<?php if( have_rows('social', 'option') ): ?>
								
								<div class="social hidden-md-down">
									
									<?php while ( have_rows('social','option') ) : the_row(); ?>
								 		<a class="pull-right" target="_blank" href="<?php the_sub_field('social_account_link'); ; ?>">
								        	<i class="brand-color-hover fa <?php the_sub_field('social_account_icon'); ; ?>" aria-hidden="true"></i>
								        </a>
								    <?php endwhile; ?>
								
								</div>
							
							<?php endif; ?>
						</div>
					</nav>

				</div>
			</header>
