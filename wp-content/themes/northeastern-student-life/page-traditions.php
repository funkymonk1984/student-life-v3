<?php
/*
 Template Name: Traditions
*/
?>

<?php get_header(); ?>
	<div id="content">

		<?php include ("includes/hero-modules.php");?>
		
		<div id="inner-content" class="container-fluid padding-0">

			<main id="main" class="padding-0 col-xs-12 col-sm-12 col-md-12 col-lg-12" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
					<?php include ("includes/sub-page-content.php");?>

					<?php if( have_rows('nu_traditions', 'option') ): $j=0 ?>
					    <?php while ( have_rows('nu_traditions', 'option') ) : the_row(); $j++ ?>
					    	<section class="nu-tradition container-fluid">
						        <?php if( get_row_layout() == 'nu_traditions_block' ): ?>
						        	<div class="container-fluid padding-0">
							        	<h3 class="text-center">
							        		<?php the_sub_field('nu_traditions_block_title'); ?>
							    		</h3>
							        	<div class="container">
								        	<?php if( have_rows('nu_tradition') ): $i=0 ?>
											 	<?php while ( have_rows('nu_tradition') ) : the_row(); $i++ ?>
											 		<div class="block row">
											 			<div class="image col-xs-12 col-sm-4 col-md-3 col-lg-3 padding-0">
												        	<img src="<?php the_sub_field('nu_tradition_image'); ?>">
												        </div>
												        <div class="content col-xs-12 col-sm-8 col-md-9 col-lg-9">
													        <h4 class="text-left center-on-mobile" data-toggle="modal" data-target="#myModal<?php echo $i; ?><?php echo $j; ?>">
													        	<a>
													        		<?php the_sub_field('nu_tradition_title'); ?> &raquo;
													        	</a>
													        </h4>

															<?php if( get_sub_field('nu_tradition_subtitle') ): ?>											        
														        <p class="hidden-xs">
														        	<?php the_sub_field('nu_tradition_subtitle'); ?>
														        </p>
													    	<?php endif; ?>
												        </div>
												        <!-- Modal -->
														<!-- <div class="modal fade" id="#myModal<?php echo $i; ?><?php echo $j; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel<?php echo $i; ?><?php echo $j; ?>"> -->
														<div class="modal fade bs-example-modal-lg" id="myModal<?php echo $i; ?><?php echo $j; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
															<div class="modal-dialog modal-lg" role="document">
																<div class="modal-content">
																	<div class="modal-header">
																		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																		<h4 class="modal-title" id="myModalLabel">
																			<?php the_sub_field('nu_tradition_title'); ?>
																		</h4>
																	</div>
																	<div class="modal-body">
																		<div class="image">
																			<img src="<?php the_sub_field('nu_tradition_image'); ?>">
																		</div>
																		<div class="content">
																			<p class="">
																	        	<?php the_sub_field('nu_tradition_description'); ?>
																	        </p>
																        </div>
																	</div>
																	<div class="modal-footer">
																		<button type="button" class="btn" data-dismiss="modal">Close</button>
																		<?php if( get_sub_field('nu_tradition_link') ): ?>
																			<a target="_blank" href="<?php the_sub_field('nu_tradition_link'); ?>" class="btn">Take Me There!</a>
																		<?php endif; ?>
																		<?php if( get_sub_field('nu_tradition_email') ): ?>
																			<a href="mailto:<?php the_sub_field('nu_tradition_email'); ?>" class="btn">Email the Organizaers!</a>
																		<?php endif; ?>
																	</div>
																</div>
															</div>
														</div>
											        </div>
											    <?php endwhile; ?>
											<?php else : ?>
											<?php endif; ?>
										</div>
									</div>
						        <?php endif; ?>
					        </section>
					    <?php endwhile; ?>

					<?php else : ?>
					<?php endif; ?>
					
				<?php endwhile; else : ?>
				<?php endif; ?>
			</main>
			<?php // get_sidebar(); ?>
		</div>
	</div>
<?php get_footer(); ?>
