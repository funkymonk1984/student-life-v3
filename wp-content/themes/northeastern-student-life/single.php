<?php get_header(); ?>
	<div id="content">
		
		<?php if( have_rows('single_post_hero') ): ?>
		
		    <?php while ( have_rows('single_post_hero') ) : the_row(); ?>
		
		        <?php if( get_row_layout() == 'simple_post_hero' ): ?>
		
		        	<div class="jumbotron jumbotron-fluid" style="background-image:url('<?php the_sub_field('simple_post_hero_image'); ?>');">
		
		        		<div class="content container">
				
							<h1 class="white">
							
								<?php if (get_sub_field('simple_post_hero_title')) { ?>
									
									<?php the_sub_field('simple_post_hero_title'); ?>
								
								<?php } else { ?>

									<?php the_title();?>
									
								<?php } ?>
								
							</h1>
		
		
							<?php if( get_sub_field('simple_post_hero_subtitle') ): ?>
		
								<p class="lead white">
									<?php the_sub_field('simple_post_hero_subtitle'); ?>
								</p>
		
							<?php endif; ?>
		
						</div>
		
						<div class="overlay"></div>
					</div>
		
		        <?php endif; ?>
		
		    <?php endwhile; ?>
		
		<?php endif; ?>
		
		<div id="inner-content" class="container">
			<div class="row">
				<main id="main" class="col-xs-12 col-sm-12 col-md-8 col-lg-8" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				
						<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article" itemscope itemprop="blogPost" itemtype="http://schema.org/BlogPosting">
				
							<header class="article-header entry-header">
				
								<h1 class="entry-title single-title" itemprop="headline" rel="bookmark">
									<?php // the_title(); ?>
								</h1>

								<!-- <p class="byline entry-meta vcard">
									<?php printf( __( 'Posted', 'bonestheme' ).' %1$s %2$s',
									// the time the post was published
									'<time class="updated entry-time" datetime="' . get_the_time('Y-m-d') . '" itemprop="datePublished">' . get_the_time(get_option('date_format')) . '</time>',
									// the author of the post
									'<span class="by">'.__( 'by', 'bonestheme' ).'</span> <span class="entry-author author" itemprop="author" itemscope itemptype="http://schema.org/Person">' . get_the_author_link( get_the_author_meta( 'ID' ) ) . '</span>'
									); ?>
								</p> -->
				
							</header> <?php // end article header ?>

							<section style="background:transparent;" class="entry-content cf" itemprop="articleBody">
								<?php the_content(); ?>
							</section> <?php // end article section ?>

							<footer class="article-footer">
								<?php printf( __( 'filed under', 'bonestheme' ).': %1$s', get_the_category_list(', ') ); ?>
								<?php the_tags( '<p class="tags"><span class="tags-title">' . __( 'Tags:', 'bonestheme' ) . '</span> ', ', ', '</p>' ); ?>
							</footer> <?php // end article footer ?>

						</article> <?php // end article ?>
					<?php endwhile; ?>
					<?php else : ?>
				
						<article id="post-not-found" class="hentry">
							<header class="article-header">
								<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
							</header>
							<section class="entry-content">
								<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
							</section>
							<footer class="article-footer">
									<p><?php _e( 'This is the error message in the single.php template.', 'bonestheme' ); ?></p>
							</footer>
						</article>
					<?php endif; ?>

					<?php include ("includes/sub-page-content.php");?>

				</main>
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
<?php get_footer(); ?>
