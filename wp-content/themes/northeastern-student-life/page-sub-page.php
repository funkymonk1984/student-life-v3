<?php
/*
 Template Name: Sub Page
*/
?>

<?php get_header(); ?>

	<div id="content">

		<?php include ("includes/hero-modules.php");?>

		<div id="inner-content" class="container-fluid padding-0">
			<main id="main" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
					
				<?php include ("includes/sub-page-content.php");?>

			</main>

		</div>
	</div>
<?php get_footer(); ?>
