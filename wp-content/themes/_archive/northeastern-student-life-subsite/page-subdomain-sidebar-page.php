<?php
/*
 Template Name: Subdomain Sidebar Page
*/
?>

<?php get_header(); ?>

	<?php include ("includes/sub-domain/sub-domain-style.php");?>
	
	<div id="content">

		<?php include ("includes/sub-domain/sub-domain-content-hero.php");?>

		<div id="inner-content" class="container padding-0">
			<main id="main" class="col-xs-1 col-sm-12 col-md-8 col-lg-8" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
				
				<?php include ("includes/sub-domain/sub-domain-content.php");?>

			</main>
			<?php get_sidebar(); ?>
		</div>
	</div>
<?php get_footer(); ?>
