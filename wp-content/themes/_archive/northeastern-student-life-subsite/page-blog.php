<?php
/*
 Template Name: Blog
*/
?>
<?php get_header(); ?>
	<div id="content">

		<?php include('includes/blog/blog-hero.php');?>

		<div id="inner-content" class="container">
			<main id="main" class="col-xs-12 col-sm-12 col-md-8 col-lg-8" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class( '' ); ?> role="article">

					<header class="article-header">
						<div class="row">
							<?php the_post_thumbnail('full'); ?>
						</div>
						<h1 class="h2 entry-title">
							<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
						</h1>
					</header>

					<section style="background:transparent;" class="entry-content">
						<?php the_excerpt(); ?>
					</section>
				</article>

				<?php endwhile; ?>
				<?php bones_page_navi(); ?>
				<?php else : ?>
				<?php endif; ?>
			</main>
			<?php get_sidebar(); ?>
		</div>
	</div>
<?php get_footer(); ?>
