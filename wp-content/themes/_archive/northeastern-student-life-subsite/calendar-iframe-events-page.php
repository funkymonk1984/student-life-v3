<?php
/*
 Template Name: Calendar: Iframe Events Page
*/

$category = get_field( 'events_calendar_categories' );
echo $category->slug;
?>

<?php wp_head(); ?>

<?php echo do_shortcode( '[tribe_events view="list" category="' . $category->slug .'"]' ); ?>

<?php wp_footer(); ?>
<script>
new pym.Child({ polling: 100 });
setBodyClass("<?php echo $category->slug; ?>");
</script>
