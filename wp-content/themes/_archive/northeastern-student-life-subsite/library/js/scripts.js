/*
 * Scripts File
 * Author: Sean Davies
*/

var studentLifeDictionary = {
  getInvolved: {
    className: 'getInvolvedBodyClass',
    organizations: ['CCS', 'FSL']
  },
  getConnected: {
    className: 'getConnectedBodyClass',
    organizations: ['lgbtqa', 'CIE', 'AAC']
  }
}

function setBodyClass(currentOrganization){
  for(key in studentLifeDictionary){
    var organizations = studentLifeDictionary[key].organizations
    for(var i = 0; i < organizations.length; i++){
      if(organizations[i] == currentOrganization){ //change this to currentOrganization
        console.log(studentLifeDictionary[key].className); //studentLifeDictionary[key].className
        //replace this with a $('body').css(className);
        $('body').addClass(studentLifeDictionary[key].className);
        var _href = $('.tribe-events-back a').attr("href");
        $('.tribe-events-back a').attr("href", _href + currentOrganization + '/events');
        $('.tribe-events-back a').html('&laquo; see all ' + currentOrganization + ' events');

      }
    }
  }
}

jQuery(document).ready(function($) {


  /*
   * Add "sub-page" class to all
   * non-home page screens
  */
  $("body").addClass("getConnectedBodyClass");

  if(!$("body").hasClass("home")){
    $("body").addClass("sub-page");
  }


  $(".home header").delay(500).fadeIn("slow");
  $(".carousel").delay(1000).fadeIn("slow");
  $(".events-wrapper").delay(1500).fadeIn('slow');










  /*
   * Drawer open/close
  */

  $( "button.navbar-toggle" ).click(function() {

    $( ".drawer-content" ).animate({
      height: "toggle"
    }, 480, function() {
      // Animation complete.
    });

    $(this).hide(60);
    $(".glyphicon-remove.navbar-toggle").delay(120).show(60);

    $(this).addClass("active");

  });




  /*
   * More drawer open/close stuff
  */

  $( ".glyphicon-remove.navbar-toggle" ).click(function() {

    $( ".drawer-content" ).animate({
      height: "toggle"
    }, 480, function() {
      // Animation complete.
    });

    $(this).hide(60);
    $("button.navbar-toggle").delay(120).show(60);

    $(this).removeClass("active");

  });




  /*
   * More drawer open/close stuff
  */

  function openCloseDivisionOfStudentLifeBlocks(element){
    $(element).css({
        "cursor":"pointer"
      });

    //TODO add header class
    $(element).click(function() {
      $(this).siblings(".student-life-organization-block-list").slideDown(120);
      $(this).parents().siblings().children(".student-life-organization-block-list").slideUp(120);

    });
  }

  if ($(window).width() < 960) {
    //openCloseDivisionOfStudentLifeBlocks(".header .student-life-organization-block-title");
  }
  else {
     //alert('More than 960');
  }

  //DIVISION OF STUDENT LIFE NAVIGATION IS ALWAYS COLLAPSED
  openCloseDivisionOfStudentLifeBlocks(".footer .student-life-organization-block-title");




  /*
   * Kind of a hacky function to get a query
   * string and use it to show the correct Explore
   * content. This should probably be re-written
  */

  function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }

  var query = getParameterByName('q');

  if (query !== null){

      $(".block").fadeOut( "fast").promise().done(function() {
        $("." + query + "-block").delay(360).fadeIn("fast");
      });

      $(".bucket-inner").fadeOut( "fast").promise().done(function() {
        $("." + query + "-bucket").delay(180).fadeIn("fast");
      });

      $("." + query + "-btn").addClass("active");

      $(".hero-unit").hide();

  } else {

      $(".hero-unit").show();
      $(".page-template-page-explore .hero-unit").fadeTo("slow" , 1);

  }


  /*
   * Explore "filters" - though it's just an illuuusion!
  */

  $( ".student-life-organization-filters .btn" ).click(function() {

    var category = $(this).attr("data-category");

    if ( $( this ).hasClass( "all" ) ) {

      $(".block").fadeIn("fast");

      $(".bucket-inner").fadeOut( "fast").promise().done(function() {
        $(".all-bucket").delay(180).fadeIn("fast");
      });

    } else {

      $(".block").fadeOut( "fast").promise().done(function() {
        $("." + category + "-block").fadeIn("fast");
      });

      $(".bucket-inner").fadeOut( "fast").promise().done(function() {
        $("." + category + "-bucket").fadeIn("fast");
      });

    }

    // Behaves slightly different if a filter is active on load
    if (query === null){

      $('html, body').animate({
          scrollTop: $(".student-life-organization-filters").offset().top
      }, 480);
    }

    $(".student-life-organization-filters .btn" ).removeClass("active");
    $(this).addClass("active");

  });





  // $(".hero-unit").fadeTo("slow" , 1);
  // $(".hero-unit .content").delay(480).fadeTo("slow" , 1);



  /*
    * Add class to a text element, it'll bold the last word
  */

  $('.styled-header').each(function() {
      var $this = $(this);
      $this.html($this.html().replace(/(\S+)\s*$/, '<span class="gotcha">$1</span>'));
  });


  // var social = {
  //   "id": '767583347375833092',
  //   "domId": 'social',
  //   "maxTweets": 3,
  //   "enableLinks": true,
  //   "showUser": false,
  //   "showTime": true,
  //   "dateFunction": dateFormatter,
  //   "showRetweet": false
  // };

  // function dateFormatter(date) {
  //   return date.toTimeString();
  // }

  // twitterFetcher.fetch(social);





  /*
    * Dynamic carousel needs first slide made active manually
  */

  $(".item:nth-child(1)").addClass("active");






  /*
    * Several random functions to clean up
    * the events feed
  */

  $(".home #lw").addClass("row");

  $(".home .lwe").addClass("col-sm-6 col-md-4 col-lg-4");
  $(".sub-page .lwe").addClass("col-sm-12 col-md-6 col-lg-6");

  $(".lwn span").addClass("text-left");
  //$(".sub-page .lwn span").addClass("pull-right");

  $(".lwn a").addClass("text-left");
  $(".lwn a").addClass("excerpt text-left");
  //$(".sub-page .lwn a").addClass("pull-left");

  $(".sub-page .lwd").addClass("text-left");

  $(".sub-page .lwd span").addClass("hidden-xs");

  $(".home .lwe").eq(2).addClass("hidden-sm");

  $(".home .lwe").prepend( "<i class='fa fa-calendar pull-left' aria-hidden='true'></i>" );

  $(".home .lwe i").addClass("hidden-sm hidden-md");

  $(".lwn span").each(function() {
      var item = $(this);
      item.next().after(item);
  });




  /*
    * Shorten the crazy long event titles
  */

  function truncateString (string, limit, breakChar, rightPad) {
    if (string.length <= limit) return string;

    var substr = string.substr(0, limit);
    if ((breakPoint = substr.lastIndexOf(breakChar)) >= 0) {
        if (breakPoint < string.length -1) {
            return string.substr(0, breakPoint) + rightPad;
        }
    }
  }

  function getParameterByName(name, url) {
      if (!url) url = window.location.href;
      name = name.replace(/[\[\]]/g, "\\$&");
      var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
          results = regex.exec(url);
      if (!results) return null;
      if (!results[2]) return '';
      return decodeURIComponent(results[2].replace(/\+/g, " "));
  }

  $( ".home .lwn a" ).each(function() {
    var text = $(this).html();
    var textShortened = truncateString(text, 24 , ' ', '...');
    $(this).html(textShortened);
  });

  var links   = $('.homepage-calendar-bar a, #student-life-events-list-content a'),
      refSite = getParameterByName('refsite');
  $.each(links, function(){
    var currentUrl  = $(this).attr('href'),
        newUrl      = currentUrl + '?slref=' + refSite;
    $(this).attr('href', newUrl);
    $(this).attr('target', '_blank');
  });

  //load the header and footer onto tribe views

  //TODO this might need to get wrapped in a function
  var referralSiteQuery = getParameterByName('slref'),
      referralSiteLocal = localStorage.slref;

  // loadSubSiteTemplatePartial("#header-dest", "https://sl.tidly.co/", "#inner-header");
  // loadSubSiteTemplatePartial("#hero-dest", "https://sl.tidly.co/" + referralSiteQuery + "/events", ".hero-unit");
  // loadSubSiteTemplatePartial("#footer-dest", "https://sl.tidly.co/", ".footer");

  // console.log('the heck is being loaded ' + "https://sl.tidly.co/" + referralSiteQuery + "/events");

  function loadSubSiteTemplatePartial(dest, url, source){
    // If the slref query is populated
    if(referralSiteQuery){
      console.log('has query');
      console.log("the string that is being passed into l o a d: " + url + referralSiteQuery + " " + source);
      // If there's a referral query, use that initially or override the localstorage
      $(dest).load(url + referralSiteQuery + " " + source);

      localStorage.slref = referralSiteQuery;
    }else if(referralSiteLocal){
      console.log('has local but no query');
      // If there isn't a referral query, but we have a captured localstorage value
      $(dest).load(url + referralSiteLocal + " " + source);
    }else{
      console.log('has neither query or local');
      // If referral doesn't exist, default
      $(dest).load(url + " " + source);
    }
  }

  setBodyClass(referralSiteQuery);

  // This function parses a Query string and grabs a value if present
  function getParameterByName(name, url) {
      if (!url) url = window.location.href;
      name = name.replace(/[\[\]]/g, "\\$&");
      var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
          results = regex.exec(url);
      if (!results) return null;
      if (!results[2]) return '';
      return decodeURIComponent(results[2].replace(/\+/g, " "));
  }

}); /* end of as page load scripts */
