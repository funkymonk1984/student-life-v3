<?php
/*
 Template Name: Calendar: Iframe Events Teaser
*/
?>
<?php $category = get_field('events_calendar_categories');?>
<?php if( $category ): $slug = $category->slug;?>
<head>
  <?php wp_head(); ?>
</head>
<div class='calendar-list-styled homepage-calendar-bar'>
<?php echo do_shortcode('[tribe_events_list category="' . $category->slug .'"]');?>
</div>
<script>
new pym.Child({ polling: 100 });
setBodyClass("<?php echo $category->slug; ?>");
</script>
<?php wp_footer(); ?>
<?php endif; // $category ?>
