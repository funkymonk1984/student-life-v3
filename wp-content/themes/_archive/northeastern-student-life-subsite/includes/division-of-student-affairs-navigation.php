<div class="drawer-menu container-fluid">
  <div class="student-life-organization-block">
    <h3 class="student-life-organization-block-title" style="color:#e98243">Get Involved</h3>
    <ul class = 'student-life-organization-block-list'>
      <li><a target="_blank" href="https://www.northeastern.edu/communityservice/">&raquo; Center of Community Service</a></li>
      <li><a target="_blank" href="http://www.northeastern.edu/csi/fraternity-and-sorority-life/">&raquo; Fraternity and Sorority Life</a></li>
      <li><a target="_blank" href="http://www.northeastern.edu/csi/leadership/">&raquo; Leadership</a></li>
      <li><a target="_blank" href="http://www.northeastern.edu/sjrc/">&raquo; Social Justice Resource Center</a></li>
      <li><a target="_blank" href="http://www.northeastern.edu/curry/step-student-employment-program/">&raquo; STEP Student Employment Program</a></li>
      <li><a target="_blank" href="https://www.northeastern.edu/sabo/">&raquo; Student Activities Business Office (SABO)</a></li>
      <li><a target="_blank" href="http://www.northeastern.edu/csi/">&raquo; Student Clubs and Organizations</a></li>
    </ul>
  </div>
  <div class="student-life-organization-block">
    <h3 class="student-life-organization-block-title" style="color:#009097">Get Connected</h3>
    <ul class="student-life-organization-block-list">
      <li><a target="_blank" href="http://www.northeastern.edu/aac/">&raquo; Asian American Center</a></li>
      <li><a target="_blank" href="">Center for Intercultural Engagement</a></li>
      <li><a target="_blank" href="http://www.northeastern.edu/spirituallife/">&raquo; Center for Spirituality, Dialogue &amp; Service</a></li>
      <li><a target="_blank" href="http://www.northeastern.edu/aai/">&raquo; John D. O'Bryant African American Institute</a></li>
      <li><a target="_blank" href="http://www.northeastern.edu/latino/">&raquo; Latino/a Student Cultural Center</a></li>
      <li><a target="_blank" href="http://www.northeastern.edu/lgbtqa">LGBTQA Center</a></li>
      <li><a target="_blank" href="http://www.northeastern.edu/ogs/">&raquo; Office of Global Services</a></li>
      <li><a target="_blank" href="http://www.northeastern.edu/parents/">&raquo; Parent &amp; Family Programs</a></li>
    </ul>
  </div>
  <div class="student-life-organization-block">
    <h3 class="student-life-organization-block-title" style="color:#90bc3f;">Get Situated</h3>
    <ul class="student-life-organization-block-list">
      <li><a target="_blank" href="http://www.northeastern.edu/housing/">&raquo; Housing &amp; Residence Life</a></li>
      <li><a target="_blank" href="http://www.northeastern.edu/huskycard/">&raquo; Husky Card Office</a></li>
      <li><a target="_blank" href="http://www.northeastern.edu/housing/living-learning-communities/">&raquo; Living/Learning Communities (LLC)</a></li>
      <li><a target="_blank" href="http://www.northeastern.edu/orientation/">&raquo; New Student Orientation</a></li>
      <li><a target="_blank" href="http://www.northeastern.edu/offcampus/">&raquo; Off Campus Student Services</a></li>
      <li><a target="_blank" href="http://www.northeastern.edu/resnet/">&raquo; Resnet Resource Center</a></li>
    </ul>
  </div>
  <div class="student-life-organization-block">
    <h3 class="student-life-organization-block-title" style="color:#ebb330">Get Support</h3>
    <ul class="student-life-organization-block-list">
      <li><a target="_blank" href="http://www.northeastern.edu/drc/">&raquo; Disability Research Center (DRC)</a></li>
      <li><a target="_blank" href="http://www.northeastern.edu/ldp/">&raquo; Learning Disabilities Program</a></li>
      <li><a target="_blank" href="http://www.northeastern.edu/open/">&raquo; Office of Prevention and Education at Northeastern (OPEN)</a></li>
      <li><a target="_blank" href="http://www.northeastern.edu/osccr/">&raquo; Office of Student Conduct &amp; Conflict Resolution (OSCCR)</a></li>
      <li><a target="_blank" href="http://www.northeastern.edu/titleix/">&raquo; Title IX</a></li>
      <li><a target="_blank" href="http://www.northeastern.edu/vision/">&raquo; Violence Support, Intervention &amp; Outreach Network (VISION)</a></li>
      <li><a target="_blank" href="http://www.northeastern.edu/wecare/">&raquo; We Care</a></li>
    </ul>
  </div>
  <div class="student-life-organization-block">
    <h3 class="student-life-organization-block-title" style="color:#5086B9">Get Healthy</h3>
    <ul class="student-life-organization-block-list">
      <li><a target="_blank" href="http://www.gonu.com/">&raquo; Athletics</a></li>
      <li><a target="_blank" href="http://www.campusrec.neu.edu/">&raquo; Campus Recreation</a></li>
      <li><a target="_blank" href="http://www.nudining.com/">&raquo; Dining Services</a></li>
      <li><a target="_blank" href="http://www.northeastern.edu/nushp/">&raquo; Northeastern University Student Health Plan (NUSHP)</a></li>
      <li><a target="_blank" href="http://www.gonu.com/sports/2011/3/22/GEN_0322113239.aspx?id=1960/">&raquo; Sports Performance</a></li>
      <li><a target="_blank" href="http://www.northeastern.edu/uhcs">&raquo; University Health &amp; Counseling Services (UHCS)</a></li>
    </ul>
  </div>
</div>
