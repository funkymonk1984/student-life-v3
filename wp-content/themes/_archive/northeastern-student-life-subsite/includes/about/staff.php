<div class="container padding-0">
<!--TODO replace with WP query-->
     <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3">
       <div class="staff-block margin-vertical-2">
         <div style="background-image:url('<?php the_sub_field('photo'); ?>');" class="image col-xs-12 col-sm-11 col-md-10 col-lg-9 margin-0-auto" data-toggle="modal" data-target="#myModal<?php echo $i; ?>"></div>
             <h4 class="text-center">
               <a href="#" data-toggle="modal" data-target="#myModal<?php echo $i; ?>">
                 <?php the_sub_field('first_name'); ?><br />
                 <span style="font-weight:700;">
                   <?php the_sub_field('last_name'); ?>
                 </span>
               </a>
             </h4>
             <div class="modal fade bs-example-modal-lg" id="myModal<?php echo $i; ?><?php // echo $j; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
           <div class="modal-dialog modal-lg" role="document">
             <div class="modal-content">
               <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 <h4 class="text-center modal-title" id="myModalLabel">
                   <?php the_sub_field('first_name'); ?> <?php the_sub_field('last_name'); ?>
                     </h4>
               </div>
               <div class="modal-body">
                 <div style="background-image:url('<?php the_sub_field('photo'); ?>');" class="image text-center col-xs-8 col-sm-6 col-md-3 col-lg-2">
                 </div>
                 <div class="content col-xs-12 col-sm-12 col-md-12 col-lg-12">
                   <div class="text-right content hidden-xs col-xs-3 col-sm-3 col-md-2 col-lg-2">
                     <p class=""><strong>Title:</strong></p>
                     <?php if( have_rows('contact_item') ): ?>
                       <?php while ( have_rows('contact_item') ) : the_row(); ?>
                         <p><strong><?php the_sub_field('contact_item_label'); ?></strong>:</p>
                       <?php endwhile; ?>
                     <?php endif; ?>
                     <p class=""><strong>Bio:</strong </p>
                   </div>
                   <div class="content col-xs-12 col-sm-9 col-md-10 col-lg-10">
                     <p class=""><strong><?php the_sub_field('title'); ?></strong></p>
                     <?php if( have_rows('contact_item') ): ?>
                       <?php while ( have_rows('contact_item') ) : the_row(); ?>
                         <p><?php the_sub_field('contact_item_value'); ?></p>
                       <?php endwhile; ?>
                     <?php endif; ?>
                     <p class=""><?php the_sub_field('bio'); ?></p>
                   </div>
                 </div>
                 <div class="clearfix"></div>
               </div>
               <div class="modal-footer">
                 <button type="button" class="btn" data-dismiss="modal">Close Window</button>
                 <?php if( have_rows('contact_item') ): ?>
                   <?php while ( have_rows('contact_item') ) : the_row(); ?>
                      <?php if(get_sub_field('contact_item_label') == 'Email'): ?>
                        <a target="_blank" href="mailto:<?php the_sub_field('contact_item_value'); ?>" class="btn"><span class="hidden-xs glyphicon glyphicon-envelope" aria-hidden="true"></span> Email <?php the_sub_field('first_name'); ?></a>
                      <?php endif; ?>
                   <?php endwhile; ?>
                 <?php endif; ?>
               </div>
             </div>
           </div>
         </div>
           </div>
         </div>
</div>
