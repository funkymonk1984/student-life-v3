<section class="sub-page-content sub-domain-full-width-text-block" itemprop="articleBody">
	<div class="container">
    	
    	<?php if( get_sub_field('sub_domain_full_width_text_block_title') ): ?>
        	<h2 class="text-center">
        		<?php the_sub_field('sub_domain_full_width_text_block_title'); ?>
        	</h2>
    	<?php endif; ?>

    	<?php if( get_sub_field('sub_domain_full_width_text_block_subtitle') ): ?>
        	<h4 class="brandable red col-xs-12 col-sm-10 col-md-8 col-lg-8 margin-0-auto lead text-center">
        		<?php the_sub_field('sub_domain_full_width_text_block_subtitle'); ?>
        	</h4>
    	<?php endif; ?>

    	<?php if( get_sub_field('sub_domain_full_width_text_block_content') ): ?>
        	<p class="text-center">
        		<?php the_sub_field('sub_domain_full_width_text_block_content'); ?>
        	</p>
    	<?php endif; ?>

        <?php if( have_rows('sub_domain_full_width_text_block_buttons') ): ?>
                
            <div class="row text-center margin-vertical-2">
            
                <?php while ( have_rows('sub_domain_full_width_text_block_buttons') ) : the_row(); ?>
                    
                    <a class="brandable btn" href="<?php the_sub_field('sub_domain_full_width_text_block_destination'); ?>">
                        <?php the_sub_field('sub_domain_full_width_text_block_button_text'); ?>
                    </a>

                <?php endwhile; ?>

            </div>

        <?php endif; ?>
    	
	</div>
</section>