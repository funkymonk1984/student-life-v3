<section class="sub-page-content sub-domain-full-bleed-image-block" itemprop="articleBody">
	<div class="container-fluid padding-0" style="background-image:url(<?php the_sub_field('sub_domain_full_bleed_image_block_image'); ?>);">
    	<div class="content text-center">
        	
            <?php if( get_sub_field('sub_domain_full_bleed_image_block_title') ): ?>
            	<h2 class="brandable text-center">
            		<?php the_sub_field('sub_domain_full_bleed_image_block_title'); ?>
            	</h2>
        	<?php endif; ?>

        	<?php if( get_sub_field('sub_domain_full_bleed_image_block_text') ): ?>
            	<h4 class="red col-xs-12 col-sm-10 col-md-8 col-lg-8 margin-0-auto lead text-center">
            		<?php the_sub_field('sub_domain_full_bleed_image_block_text'); ?>
            	</h4>
        	<?php endif; ?>

            <?php if( have_rows('sub_domain_full_bleed_image_block_buttons') ): ?>
                
                <div class="row margin-vertical-2">
                
                    <?php while ( have_rows('sub_domain_full_bleed_image_block_buttons') ) : the_row(); ?>
                        
                        <a class="brandable btn" href="<?php the_sub_field('sub_domain_full_bleed_image_block_button_destination'); ?>">
                            <?php the_sub_field('sub_domain_full_bleed_image_block_button_text'); ?>
                        </a>

                    <?php endwhile; ?>

                </div>

            <?php endif; ?>

        </div>
        <div class="overlay"></div>
	</div>
</section>