<section class="sub-page-content sub-domain-wysiwyg" itemprop="articleBody">
	<div class="row">
    	
    	<?php if( get_sub_field('sub_domain_wysiwyg_title') ): ?>
        	<h3 class="brandable text-left">
        		<?php the_sub_field('sub_domain_wysiwyg_title'); ?>
        	</h3>
    	<?php endif; ?>

    	<?php if( get_sub_field('sub_domain_wysiwyg_editor') ): ?>
        	<?php the_sub_field('sub_domain_wysiwyg_editor'); ?>
        <?php endif; ?>
    	
	</div>
</section>