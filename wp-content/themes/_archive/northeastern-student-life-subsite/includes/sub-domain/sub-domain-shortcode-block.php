<section class="sub-page-content sub-domain-shortcode-block" itemprop="articleBody">
	<div class="container">
    	
    	<?php if( get_sub_field('sub_domain_shortcode_title') ): ?>
            <h3 class="brandable text-left">
                <?php the_sub_field('sub_domain_shortcode_title'); ?>
            </h3>
        <?php endif; ?>

        <?php if( get_sub_field('sub_domain_shortcode_include') ): ?>
        	<div class="row">
                <?php the_sub_field('sub_domain_shortcode_include'); ?>
            </div>
    	<?php endif; ?>
    	
	</div>
</section>