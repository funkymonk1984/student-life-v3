<?php if( have_rows('sub_domain_content') ): ?>
    <?php while ( have_rows('sub_domain_content') ) : the_row(); ?>
        <?php if( get_row_layout() == 'sub_domain_hero' ): ?>
        	
        	<div class="hero-unit container-fluid text-center padding-0" style="background-image:url('<?php the_sub_field('sub_domain_hero_image'); ?>');">
        		<div class="content">
        			
        			<?php if( get_sub_field('sub_domain_hero_title') ): ?>
						<h1 class="brandable white">
							<?php the_sub_field('sub_domain_hero_title'); ?>
						</h1>
					<?php endif; ?>

					<?php if( get_sub_field('sub_domain_hero_subtitle') ): ?>
						<p class="lead white hidden-xs hidden-sm">
							<?php the_sub_field('sub_domain_hero_subtitle'); ?>
						</p>
					<?php endif; ?>

					<?php if( have_rows('sub_domain_hero_buttons') ): ?>
                
		                <div class="row margin-vertical-2">
		                
		                    <?php while ( have_rows('sub_domain_hero_buttons') ) : the_row(); ?>
		                        
		                        <a class="brandable btn" href="<?php the_sub_field('sub_domain_hero_button_destination'); ?>">
		                            <?php the_sub_field('sub_domain_hero_button_text'); ?>
		                        </a>

		                    <?php endwhile; ?>

		                </div>

		            <?php endif; ?>

				</div>
				
				<?php if( get_sub_field('sub_domain_hero_image') ): ?>
					<div class="overlay"></div>
				<?php endif; ?>

			</div>

        <?php endif; ?>
    <?php endwhile; ?>
<?php endif; ?>