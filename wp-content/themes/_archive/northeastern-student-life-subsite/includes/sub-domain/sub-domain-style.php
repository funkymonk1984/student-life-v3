<?php if( get_field('branding','option') ): ?>
	<style>
		
		.brandable {
			color: <?php the_field('branding','option'); ?> !important;
		}

		body #content a {
			color: <?php the_field('branding','option'); ?> !important;
			opacity: .8;
		}

		body #content a:hover {
			opacity: 1;
		}

		body #content .brandable.btn {
			background-color: <?php the_field('branding','option'); ?> !important;
			color:#FFF !important;
		}

		body #content .btn.transparent {
		    color: #FFF !important;
		}

		body #content .btn.transparent:hover {
		    color: <?php the_field('branding','option'); ?> !important;
		}

		.sub-page #container header {
		    background: <?php the_field('branding','option'); ?> !important;
		}

		.home .menu-item {
			border-color: <?php the_field('branding','option'); ?> !important;
		}

		.sub-page .menu-item {
			border-color: #FFF !important;
			color:#FFF !important;
		}

		.menu-item a:hover {
			color: <?php the_field('branding','option'); ?> !important;
		}

		.navbar-toggle .icon-bar {
		    background-color: <?php the_field('branding','option'); ?> !important;
		}

		.drawer-handle .glyphicon-remove {
			color: <?php the_field('branding','option'); ?> !important;
		}

	</style>
<?php endif; ?>