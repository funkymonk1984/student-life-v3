<section class="sub-page-content sub-domain-blog-grid" itemprop="articleBody">
	<div class="container">
		
		<?php if( get_sub_field('sub_domain_blog_grid_title') ): ?>
			<h2 class="text-center">
				<?php the_sub_field('sub_domain_blog_grid_title'); ?>
			</h2>
		<?php endif; ?>
		
		<?php if( get_sub_field('sub_domain_blog_grid_subtitle') ): ?>
			<h4 class="red brandable text-center">
				<?php the_sub_field('sub_domain_blog_grid_subtitle'); ?>
			</h4>
		<?php endif; ?>

		<?php $category = get_sub_field('sub_domain_blog_grid_category'); ?>
		<?php $numberofposts = get_sub_field('sub_domain_blog_grid_quantity'); ?>
		
		<?php query_posts('cat=' . $category . '&showposts=' . $numberofposts); ?>
		
		<div class="row margin-vertical-2">

			<?php while (have_posts()) : the_post(); ?>
				
				<div class="col-sm-12 col-md-4 col-lg-4">
					<div class="image">
						<?php the_post_thumbnail('full'); ?>
					</div>
					<h4>
						<a href="<?php the_permalink(); ?>" />
							<?php the_title(); ?>
						</a
					</h4>
					<p>
						<small><?php the_excerpt(); ?></small>
					</p>
				</div>

			<?php endwhile;?>

		</div>

		<?php wp_reset_query(); ?>

		<div class="clearfix"></div>
	</div>
</section>