<section class="sub-page-content entry-content container-fluid" itemprop="articleBody">
	<div class="container">

		<?php $selected = get_sub_field('sub_domain_text_and_image_block_orientation'); ?>
		<?php if( in_array('image-left', $selected) ) { ?>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5 pull-left">
		<?php } ?>
		<?php if( in_array('image-right', $selected) ) { ?>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5 pull-right">
		<?php } ?>
    		<img src="<?php the_sub_field('sub_domain_text_and_image_block_image'); ?>">
    	</div>

    	<?php $selected = get_sub_field('sub_domain_text_and_image_block_orientation'); ?>
		<?php if( in_array('image-left', $selected) ) { ?>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-7 pull-right">
		<?php } ?>
		<?php if( in_array('image-right', $selected) ) { ?>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-7 pull-left">
		<?php } ?>

		<?php if( get_sub_field('sub_domain_text_and_image_block_subtitle') ): ?>
        	<h2>
        		<?php the_sub_field('sub_domain_text_and_image_block_title'); ?>
        	</h2>
    	<?php endif; ?>
    	<?php if( get_sub_field('sub_domain_text_and_image_block_subtitle') ): ?>
        	<h4 class="brandable red lead">
        		<?php the_sub_field('sub_domain_text_and_image_block_subtitle'); ?>
        	</h4>
    	<?php endif; ?>
    	<?php if( get_sub_field('sub_domain_text_and_image_block_subtitle') ): ?>
        	<p>
        		<?php the_sub_field('sub_domain_text_and_image_block_content'); ?>
        	</p>
    	<?php endif; ?>

        <?php if( get_sub_field('sub_domain_text_and_image_block_button_text') ): ?>
            <a class="brandable btn" href="<?php the_sub_field('sub_domain_text_and_image_block_destination'); ?>">
                <?php the_sub_field('sub_domain_text_and_image_block_button_text'); ?>
            </a>
        <?php endif; ?>
    	</div>
	</div>
</section>