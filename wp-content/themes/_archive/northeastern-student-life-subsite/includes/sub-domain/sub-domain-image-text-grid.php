<section class="sub-page-content subdomain-image-text-grid container-fluid" itemprop="articleBody">
	<div class="container-fluid padding-0">

        <?php if( have_rows('sub_domain_image_text_blocks') ): ?>
            <?php while ( have_rows('sub_domain_image_text_blocks') ) : the_row(); ?>
                
                <div class="js-text-image-two-column-full-bleed-grid block text-center col-xs-12 col-sm-12 col-md-6 col-lg-6" style="background-image:url('<?php the_sub_field('sub_domain_image_text_blocks_image'); ?>');">
                    <div class="content">
                        <h3 class="brandable">
                            <?php the_sub_field('sub_domain_image_text_blocks_title'); ?>
                        </h3>
                        <p class='col-xs-10 col-sm-10 col-md-8 col-lg-8 margin-1-auto'>
                            <?php the_sub_field('sub_domain_image_text_blocks_text'); ?>
                        </p>
                        <a class="text-center btn transparent href="<?php the_sub_field('sub_domain_image_text_blocks_button_url'); ?>">
                            <?php the_sub_field('sub_domain_image_text_blocks_button_text'); ?>
                        </a>
                    </div>
                    <div class="overlay"></div>
                </div>
                
            <?php endwhile; ?>
        <?php else : ?>
        <?php endif; ?>

	</div>
</section>