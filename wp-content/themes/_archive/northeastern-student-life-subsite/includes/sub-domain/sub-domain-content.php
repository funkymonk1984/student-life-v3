<?php if( have_rows('sub_domain_content') ): ?>
    <?php while ( have_rows('sub_domain_content') ) : the_row(); ?>
       
        <?php if( get_row_layout() == 'sub_domain_full_width_text_block' ): ?>
        	
        	<?php include('sub-domain-full-width-text-block.php'); ?>

        <?php elseif( get_row_layout() == 'sub_domain_text_and_image_block' ): ?>
        	
        	<?php include ('sub-domain-text-and-image-block.php'); ?>
        	
        <?php elseif( get_row_layout() == 'sub_domain_shortcode_block' ): ?>
            
            <?php include ('sub-domain-shortcode-block.php'); ?>

        <?php elseif( get_row_layout() == 'sub_domain_full_bleed_image_block' ): ?>
            
            <?php include ('sub-domain-full-bleed-image-block.php'); ?>

         <?php elseif( get_row_layout() == 'sub_domain_image_text_grid' ): ?>
            
            <?php include ('sub-domain-image-text-grid.php'); ?>

        <?php elseif( get_row_layout() == 'sub_domain_wysiwyg' ): ?>
            
            <?php include ('sub-domain-wysiwyg.php'); ?>

        <?php elseif( get_row_layout() == 'sub_domain_blog_grid' ): ?>
            
            <?php include ('sub-domain-blog-grid.php'); ?>

        <?php endif; ?>
    <?php endwhile; ?>
<?php endif; ?>