<div class="<?php echo $config['columnClass']; ?>">
   <div class="staff-block margin-vertical-2">
     <div style="background-image:url('<?php the_field('directory_photo'); ?>');" class="image col-xs-12 col-sm-11 col-md-10 col-lg-9 margin-0-auto" data-toggle="modal" data-target="#myModal<?php echo $config['directory_item_counter']; ?>"></div>
     <h4 class="text-center">
       <a href="#" data-toggle="modal" data-target="#myModal<?php echo $config['directory_item_counter']; ?>">
         <?php the_field('directory_name'); ?>
       </a>
     </h4>
    <?php
    //if(isset($config['isTeaser'])){
      display( INCLUDES.'/directory-item--modal.php', array(), $config );
    //}
    ?>
  </div>
</div>
