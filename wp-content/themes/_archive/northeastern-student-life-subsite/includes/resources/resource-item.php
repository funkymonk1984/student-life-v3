<?php if(get_sub_field('image')):?>
<?php
$image_thumb = get_sub_field('image');
$image_med = $image_thumb['sizes']['medium'];
$image_large = $image_thumb['sizes']['large'];
?>

<!--  background:
    /* top, transparent red, faked with gradient */
    linear-gradient(
      rgba(255, 255, 255, 0.8),
      rgba(255, 255, 255, 0.8)
    ),
    /* bottom, image */
    url(image.jpg);-->

<div
  style="
  border-top:thick solid <?php echo $color; ?>;
  background:
  linear-gradient(
    rgba(0, 0, 0, 0.5),
    rgba(0, 0, 0, 0.5)
  ),
  url('<?php echo $image_med; ?>');
  background-size:cover;
  "
  data-category="<?php echo $category; ?>"
  class="<?php echo $category; ?>-block block col-xs-12 col-sm-6 col-md-4 col-lg-3 padding-0">
<?php else: ?>
<div style="background-color:<?php echo $color; ?>;" data-category="<?php echo $category; ?>" class="<?php echo $category; ?>-block block col-xs-12 col-sm-6 col-md-4 col-lg-3 padding-0">
<?php endif; ?>

  <a class = 'resource-item--anchor' data-toggle="modal" data-target="#myModal<?php echo $i; ?><?php echo $j; ?>">
    <h5 class="text-center styled-header">
      <?php the_sub_field('title'); ?>
    </h5>
  </a>

  <?php if( get_sub_field('description') ): ?>
  <!--<p class="text-center"><?php //the_sub_field('description'); ?></p>-->
  <?php endif; ?>

  <!--<div href="#" class="overlay"></div>-->
</div>
