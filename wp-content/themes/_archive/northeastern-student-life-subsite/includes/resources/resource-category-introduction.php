<?php
$category = get_the_title();
$category = strtolower($category);
$category = preg_replace("/[^a-z0-9_\s-]/", "", $category);
$category = preg_replace("/[\s-]+/", " ", $category);
$category = preg_replace("/[\s_]/", "-", $category);
?>

<div class="<?php echo $category; ?>-bucket bucket-inner">
  <div class="image col-xs-12 col-sm-12 col-md-5 col-lg-5">
    <img src="<?php the_field('resources_category_image');?>">
  </div>
  <div class="content col-xs-12 col-sm-12 col-md-7 col-lg-7">
    <h3 class="styled-header" style="color:<?php the_field('resources_category_brand');?>;">
      <?php the_title();?>
    </h3>
    <p><?php the_field('resources_category_description');?></p>
  </div>
  <div class="clearfix"></div>
</div>
