<section class="student-life-organization-filters container-fluid">
  <h3 style="margin-bottom:1em;" class="styled-header text-center">Browse Resources</h3>

  <!--TABS FOR CATEGORIES-->
  <?php
  $args = array('post_type' => 'resources');
  $loop = new WP_Query( $args );
  if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();
  ?>

  <?php include(INCLUDES.'/explore/format-organization-title.php'); ?>

  <button style="background-color:<?php the_field('resources_category_brand');?>;" data-category="<?php echo $category; ?>" class="styled-header <?php echo $category; ?>-btn btn">
    <?php the_title();?>
  </button>

  <?php endwhile; endif; wp_reset_query(); ?>

  <button class="active all btn">All</button>

  <!--END TABS FOR CATEGORIES-->

</section>
