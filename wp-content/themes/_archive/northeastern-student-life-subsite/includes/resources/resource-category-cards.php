<section class="student-life-organization container-fluid padding-0">
<?php var_dump($category); ?>

  <!--TODO CPT loop goes here-->
  <?php $loop = new WP_Query( array('post_type' => 'resources') );
  if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>

  <div class="<?php echo $category; ?>-wrapper">
     <?php
     include('includes/explore/format-organization-title.php');
     $color = get_field('resources_category_brand');
     $image = get_field('resources_category_image');
     ?>
     <!--DISPLAY RESOURCE ITEMS TO SCREEN-->
     <div class="content <?php echo $category; ?>-content padding-0">
       <?php if( have_rows('resource_item') ): $i=0; ?>
         <?php while ( have_rows('resource_item') ) : the_row(); $i++; ?>
           <?php include (INCLUDES.'/resources/resource-item.php');?>
         <?php endwhile; ?>
       <?php endif; ?>
     </div>
     <!--END DISPLAY RESOURCE ITEMS TO SCREEN-->
     <div class="category-background-image <?php echo $category; ?>-background-image">
       <img src="<?php the_field('resources_category_image');?>">
     </div>
   </div>

  <?php endwhile; endif; wp_reset_query(); ?>

  <?php $loop = new WP_Query( array('post_type' => 'resources') );
  if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>

    <?php $color = get_field('resources_category_brand'); ?>

    <?php if( have_rows('resource_item') ): $i=0; ?>
    <?php while ( have_rows('resource_item') ) : the_row(); $i++; ?>
        <?php include (INCLUDES.'/resources/resource-item--modal.php');?>
    <?php endwhile; endif; ?>

  <?php endwhile; endif; wp_reset_query(); ?>

</section>
