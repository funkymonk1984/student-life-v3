<div class="modal fade bs-example-modal-lg" id="myModal<?php echo $i; ?><?php echo $j; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">
          <?php the_sub_field('title'); ?>
        </h4>
      </div>
      <div class="modal-body">
        <div class="content">
          <?php $show_image = get_sub_field('show_image')[0]?>
          <?php if(get_sub_field('image') && $show_image == 'yes'):?>
            <?php
            $image_thumb = get_sub_field('image');
            $image_large = $image_thumb['sizes']['large'];
            ?>
            <img src="<?php echo $image_large; ?>">
          <?php endif; ?>
          <p class=""><?php the_sub_field('description'); ?></p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn" data-dismiss="modal">Close Window</button>
        <?php if( get_sub_field('link') ): ?>
          <a target="_blank" href="<?php the_sub_field('link'); ?>" class="btn">Take Me There!</a>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
