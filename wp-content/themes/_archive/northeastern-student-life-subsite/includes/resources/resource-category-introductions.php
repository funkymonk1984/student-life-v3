<section class="student-life-organization-buckets container-fluid padding-0">
  <div class="bucket-outer container">

    <?php $loop = new WP_Query( array('post_type' => 'resources') );
    if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
      <?php if(get_field('resources_category_description') || get_field('resources_category_image')):?>
        <?php include (INCLUDES.'/resources/resource-category-introduction.php');?>
      <?php endif;?>
    <?php endwhile;?>
      <div class="all-bucket bucket-inner" style="display:block;">
        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-8 margin-0-auto">
          <h3 class="styled-header text-center">
            All
          </h3>
          <p class="text-center">Use the filters above to narrow your search.</p>
        </div>
      </div>
      <div class="clearfix"></div>
    <?php endif; wp_reset_query(); ?>
  </div>
  <div class="clearfix"></div>
</section>
