<?php
	$category = get_the_title();
	$category = strtolower($category);
	$category = preg_replace("/[^a-z0-9_\s-]/", "", $category);
	$category = preg_replace("/[\s-]+/", " ", $category);
	$category = preg_replace("/[\s_]/", "-", $category);
?>
