<section class="explore-page-content container" itemprop="articleBody">
	<div class="container-fluid padding-0">
    	
    	<?php if( get_sub_field('explore_featured_organization_title') ): ?>
        	<h2 class="text-center">
        		<?php the_sub_field('explore_featured_organization_title'); ?>
        	</h2>
    	<?php endif; ?>
    	
    	<?php if( get_sub_field('explore_featured_organization_subtitle') ): ?>
        	<h4 class="red col-xs-12 col-sm-10 col-md-8 col-lg-8 margin-0-auto lead text-center">
        		<?php the_sub_field('explore_featured_organization_subtitle'); ?>
        	</h4>
    	<?php endif; ?>
    	
	</div>
</section>