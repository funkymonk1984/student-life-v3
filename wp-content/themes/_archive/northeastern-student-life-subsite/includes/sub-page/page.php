<div id="content">
  <?php display("includes/sub-page/sub-page-content-hero.php");?>
  <div id="inner-content" class="container-fluid padding-0">
    <main id="main" class="padding-0 col-xs-12 col-sm-12 col-md-12 col-lg-12" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <?php the_content(); ?>
        <?php include (INCLUDES."/sub-page/sub-page-content.php");?>

      <?php endwhile; else : ?>
      <?php endif; ?>
    </main>
  </div>
</div>
