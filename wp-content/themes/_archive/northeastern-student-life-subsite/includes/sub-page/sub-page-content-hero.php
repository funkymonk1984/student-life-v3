<?php
$container_fluid = '';
if(!isset($config['is_sidebar_thumb'])){
 $container_fluid = 'container-fluid';
}
?>

<?php if( get_field('sub_page_hero_title') ): ?>
<div class="hero-unit <?php echo $container_fluid; ?> text-center padding-0" style="background-image:url('<?php the_field('sub_page_hero_image'); ?>');">
  <div class="content">

  	<?php if( get_field('sub_page_hero_title') ): ?>
			<h1 class="white">
				<?php the_field('sub_page_hero_title'); ?>
			</h1>
		<?php endif; ?>

		<?php if( get_field('sub_page_hero_subtitle') ): ?>
			<p class="lead white hidden-xs hidden-sm">
				<?php the_field('sub_page_hero_subtitle'); ?>
			</p>
		<?php endif; ?>

	</div>
	<div class="overlay"></div>
</div>
<?php endif; ?>

<!--the below code supports the old method of adding heros to the page, as a sub page layout-->

<?php if( have_rows('sub_page_content') ): ?>

    <?php while ( have_rows('sub_page_content') ) : the_row(); ?>
        <?php if( get_row_layout() == 'sub_page_hero' ): ?>
          <?php if( isset($config['is_sidebar_thumb']) ): ?><a href = <?php the_permalink();?>><?php endif; ?>
          <div class="hero-unit <?php echo $container_fluid; ?> text-center padding-0" style="background-image:url('<?php the_sub_field('sub_page_hero_image'); ?>');">
            <div class="content">

            	<?php if( get_sub_field('sub_page_hero_title') ): ?>
          			<h1 class="white">
          				<?php the_sub_field('sub_page_hero_title'); ?>
          			</h1>
          		<?php endif; ?>

          		<?php if( get_sub_field('sub_page_hero_subtitle') ): ?>
          			<p class="lead white hidden-xs hidden-sm">
          				<?php the_sub_field('sub_page_hero_subtitle'); ?>
          			</p>
          		<?php endif; ?>

          	</div>
          	<div class="overlay"></div>
          </div>
          <?php if( isset($config['is_sidebar_thumb']) ): ?></a><?php endif; ?>
        <?php endif; ?>
    <?php endwhile; ?>
<?php endif; ?>
