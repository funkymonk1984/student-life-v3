<section class="sub-page-content entry-content" itemprop="articleBody">
  <div class = 'container'>
    <?php if( get_sub_field('title') ): ?>
        <h2 class="text-center">
          <?php the_sub_field('title'); ?>
        </h2>
    <?php endif; ?>
    <?php if( get_sub_field('subtitle') ): ?>
        <h4 class="red lead text-center">
          <?php the_sub_field('subtitle'); ?>
        </h4>
    <?php endif; ?>
  <?php
  $table = get_sub_field( 'table' );

  if ( $table ) {
      echo '<div class="table-responsive">';
      echo '<table class = "table" border="0">';

          if ( $table['header'] ) {

              echo '<thead>';

                  echo '<tr>';

                      foreach ( $table['header'] as $th ) {

                          echo '<th>';
                              echo $th['c'];
                          echo '</th>';
                      }

                  echo '</tr>';

              echo '</thead>';
          }

          echo '<tbody>';

              foreach ( $table['body'] as $tr ) {

                  echo '<tr>';

                      foreach ( $tr as $td ) {

                          echo '<td>';
                              echo $td['c'];
                          echo '</td>';
                      }

                  echo '</tr>';
              }

          echo '</tbody>';

      echo '</table>';
      echo '</div>';
  }
  ?>
  </div>
</section>
