<?php
$column_width = '';
$count = count( get_sub_field( 'sub_page_multi_column_column' ) );;
if ($count % 2 == 0){
  $column_width = 6;
} elseif ($count % 3 == 0) {
  $column_width = 4;
}
//echo $column_width;
?>


<section class="sub-page-content entry-content container" itemprop="articleBody">
	<div class="margin-vertical-2">
    <h2 class="text-center"><?php the_sub_field('sub_page_multi_column_header'); ?></h2>
    <?php if( get_sub_field('subtitle') ): ?>
      <h4 class="text-center"><?php the_sub_field('subtitle'); ?></h4>
    <?php endif ;?>
    <!--TODO might need to scrap row-->
    <div class = 'row'>
      <?php if( have_rows('sub_page_multi_column_column') ):?>
          <?php while ( have_rows('sub_page_multi_column_column') ) : the_row(); ?>
              <?php $image = get_sub_field('image');?>
              <div class = 'col-xs-12 col-lg-<?php echo $column_width;?>'>
                <?php if( !empty($image) ): ?>

                <?php
                $url = $image['url'];
                $alt = $image['alt'];
                ?>

                <div class = 'text-center'><img class = 'multi-column--image' src="<?php echo $url; ?>" alt="<?php echo $alt; ?>"></div>
                <?php endif; ?>

                <?php if( get_sub_field('multi_column_column_header') ): ?>
                <h3><?php the_sub_field('multi_column_column_header'); ?></h3>
                <?php endif; ?>

                <?php if( get_sub_field('multi_column_column_subheader') ): ?>
                <h5><?php the_sub_field('multi_column_column_subheader'); ?></h5>
                <?php endif; ?>

                <?php if( get_sub_field('multi_column_column_content') ): ?>
                <?php the_sub_field('multi_column_column_content'); ?>
                <?php endif; ?>

                <?php if( get_sub_field('button_text') ): ?>
                    <p class="text-left">
                      <a class = 'btn' href = "<?php the_sub_field('button_link'); ?>"><?php the_sub_field('button_text'); ?> &raquo;</a>
                    </p>
                <?php endif; ?>
              </div>
          <?php endwhile; ?>

      <?php else : ?>

          <p>no rows found</p>

      <?php endif; ?>
    </div><!--row-->
	</div><!--margin-vertical-2-->
</section>
