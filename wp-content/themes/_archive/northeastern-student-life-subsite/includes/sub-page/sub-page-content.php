<?php if( have_rows('sub_page_content') ): ?>
    <?php $accordion_count = 0; //used to give each accordion a unique id and target?>

    <?php while ( have_rows('sub_page_content') ) : the_row(); ?>

        <?php if( get_row_layout() == 'sub_page_full_width_text_block' ): ?>
        	<?php include('full-width-text-block.php'); ?>

        <?php elseif( get_row_layout() == 'sub_page_text_and_image_block' ): ?>
        	<?php include ('sub-page-text-and-image-block.php'); ?>

        <?php elseif( get_row_layout() == 'sub_page_multi_column' ): ?>
        	<?php include ('sub-page-multi-column.php'); ?>

        <?php elseif( get_row_layout() == 'table' ): ?>
        	<?php include ('sub-page-table.php'); ?>

        <?php elseif( get_row_layout() == 'display_staff_directory' ): ?>
          <?php
          display(
            INCLUDES.'/directory.php',
            array(),
            array('category' => 'staff')
          );
          ?>

        <?php elseif( get_row_layout() == 'display_other_directory' ): ?>
          <?php
          display(
            INCLUDES.'/directory.php',
            array(),
            array('category' => 'other-directory')
          );
          ?>

        <?php elseif( get_row_layout() == 'embed' ): ?>
          <?php include ('embed-and-text.php'); ?>

        <?php //elseif ( get_row_layout() == 'accordion' && have_rows( 'accordion_section' ) )?>
        <?php elseif( get_row_layout() == 'accordion' ): ?>
          <?php $accordion_count++; ?>
          <?php include ('accordion.php'); ?>

        <?php endif; ?>
    <?php endwhile; ?>
<?php endif; ?>
