<?php $image_alignment = get_sub_field('sub_page_text_and_image_block_orientation'); ?>

<section class="sub-page-content entry-content" itemprop="articleBody">
	<div class = "container">
		<div class="margin-vertical-2">

			<?php $selected = get_sub_field('sub_page_text_and_image_block_orientation'); ?>
			<?php if($image_alignment == 'Left') { ?>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5 pull-left">
			<?php } ?>
			<?php if($image_alignment == 'Right') { ?>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5 pull-right">
			<?php } ?>
	    		<img src="<?php the_sub_field('sub_page_text_and_image_block_image'); ?>">
	    	</div>

	    	<?php $selected = get_sub_field('sub_page_text_and_image_block_orientation'); ?>
			<?php if($image_alignment == 'Left') { ?>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-7 pull-right">
			<?php } ?>
			<?php if($image_alignment == 'Right') { ?>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-7 pull-left">
			<?php } ?>

			<?php if( get_sub_field('sub_page_text_and_image_block_title') ): ?>
	        	<h2>
	        		<?php the_sub_field('sub_page_text_and_image_block_title'); ?>
	        	</h2>
	    	<?php endif; ?>
	    	<?php if( get_sub_field('sub_page_text_and_image_block_subtitle') ): ?>
	        	<h4 class="theme-color lead">
	        		<?php the_sub_field('sub_page_text_and_image_block_subtitle'); ?>
	        	</h4>
	    	<?php endif; ?>
	    	<?php if( get_sub_field('sub_page_text_and_image_block_content') ): ?>
	        	<p>
	        		<?php the_sub_field('sub_page_text_and_image_block_content'); ?>
	        	</p>
	    	<?php endif; ?>
				<?php if( get_sub_field('button_text') ): ?>
						<p class="text-left">
							<a class = 'btn' href = "<?php the_sub_field('button_link'); ?>"><?php the_sub_field('button_text'); ?> &raquo;</a>
						</p>
				<?php endif; ?>
	    	</div>
		</div>
	</div>
</section>
