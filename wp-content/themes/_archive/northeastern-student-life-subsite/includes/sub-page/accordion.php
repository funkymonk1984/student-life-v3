<section class="sub-page-content entry-content" itemprop="articleBody">
<div class="container">

  <?php if( get_sub_field('title') ): ?>
	<h2 class = 'text-center'><?php the_sub_field('title'); ?></h2>
	<?php endif; ?>

	<?php if( get_sub_field('subtitle') ): ?>
	<h4 class="red lead text-center"><?php the_sub_field('subtitle'); ?></h4>
	<?php endif; ?>

	<div class="panel-group" id="accordion-<?php echo $accordion_count; ?>">

		<?php $panel_count = 0; ?>
		<?php while ( have_rows( 'accordion_section' ) ) : the_row(); ?>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse"
						   data-parent="#accordion-<?php echo $accordion_count; ?>"
						   href="#collapse-<?php echo $accordion_count; ?>-<?php echo $panel_count; ?>"
							<?php if ( $panel_count != 0 ) : ?>
								class="collapsed"
							<?php endif; ?>
							>
							<?php the_sub_field( 'title' ); ?>
						</a>
					</h4>
				</div>
				<div
					id="collapse-<?php echo $accordion_count; ?>-<?php echo $panel_count; ?>"
					class="panel-collapse collapse <?php if ( $panel_count == 0 ) echo 'in'; ?>"
					>
					<div class="panel-body">
						<?php the_sub_field( 'content' ); ?>
					</div>
				</div>
			</div>
			<?php $panel_count++; endwhile; ?>

	</div>
</div>
</section>
