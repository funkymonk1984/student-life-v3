<?php $image_alignment = get_sub_field('orientation'); ?>

<section class="sub-page-content entry-content" itemprop="articleBody">
	<div class = "container">
		<div class="margin-vertical-2">

			<?php $selected = get_sub_field('sub_page_text_and_image_block_orientation'); ?>
			<?php if(get_sub_field('title')) { ?>
				<?php if($image_alignment == 'Left') { ?>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5 pull-left">
				<?php } ?>
				<?php if($image_alignment == 'Right') { ?>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5 pull-right">
				<?php } ?>
			<?php } ?>
			<?php if(!get_sub_field('title')) { ?>
				<div class="col-xs-12 col-md-8 col-md-push-2">
			<?php } ?>
		    	<?php the_sub_field('embed_code'); ?>
		    </div>


			<?php if($image_alignment == 'Left'): ?>
		    	<?php $selected = get_sub_field('sub_page_text_and_image_block_orientation'); ?>
				<?php if($image_alignment == 'Left') { ?>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-7 pull-right">
				<?php } ?>
				<?php if($image_alignment == 'Right') { ?>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-7 pull-left">
				<?php } ?>

				<?php if( get_sub_field('title') ): ?>
		        	<h2>
		        		<?php the_sub_field('title'); ?>
		        	</h2>
		    	<?php endif; ?>
		    	<?php if( get_sub_field('subtitle') ): ?>
		        	<h4 class="lead">
		        		<?php the_sub_field('subtitle'); ?>
		        	</h4>
		    	<?php endif; ?>
		    	<?php if( get_sub_field('content') ): ?>
		        	<p>
		        		<?php the_sub_field('content'); ?>
		        	</p>
		    	<?php endif; ?>
					<?php if( get_sub_field('button_text') ): ?>
							<p class="text-left">
								<a class = 'btn' href = "<?php the_sub_field('button_link'); ?>"><?php the_sub_field('button_text'); ?> &raquo;</a>
							</p>
					<?php endif; ?>
		    	</div>
				<?php endif; //don't render the text column if no text has been entered?>
		</div>
	</div>
</section>
