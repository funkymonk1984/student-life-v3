<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Get Involved
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        <ul>
        <li><a href="#">Center of Community Service</a></li>
        <li><a href="#">Fraternity and Sorority Life</a></li>
        <li><a href="#">Leadership</a></li>
        <li><a href="#">Social Justice Resource Center</a></li>
        <li><a href="#">STEP Student Employment Program</a></li>
        <li><a href="#">Student Activities Business Office (SABO)</a></li>
        <li><a href="#">Student Clubs and Organizations</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingTwo">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Get Connected
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">
        <ul>
          <li><a href="#">Asian American Center</a></li>
          <li><a href="#">Center for Intercultural Engagement</a></li>
          <li><a href="#">Center for Spirituality, Dialogue and Service</a></li>
          <li><a href="#">John D. O&#39;Bryant African American Institute</a></li>
          <li><a href="#">Latino/a Student Cultural Center</a></li>
          <li><a href="#">LGBTQA Center</a></li>
          <li><a href="#">Office of Global Services</a></li>
          <li><a href="#">Parent and Family Programs</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Get Situated
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
          <ul>
            <li><a href="#">Housing and Residence Life</a></li>
            <li><a href="#">Husky Card Office</a></li>
            <li><a href="#">Living/Learning Communities (LLC)</a></li>
            <li><a href="#">New Student Orientation</a></li>
            <li><a href="#">Off Campus Student Services</a></li>
            <li><a href="#">Resnet Resource Center</a></li>
          </ul>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Get Support
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
        <ul>
          <li><a href="#">Disability Research Center (DRC)</a></li>
          <li><a href="#">Learning Disabilities Program</a></li>
          <li><a href="#">Office of Prevention and Education at Northeastern (OPEN)</a></li>
          <li><a href="#">Office of Student Conduct and Conflict Resolution (OSCCR)</a></li>
          <li><a href="#">Title IX</a></li>
          <li><a href="#">Violence Support, Intervention and Outreach Network (VISION)</a></li>
          <li><a href="#">We Care</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Get Healthy
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
        <ul>
        <li><a href="#">Athletics</a></li>
        <li><a href="#">Campus Recreation</a></li>
        <li><a href="#">Dining Services</a></li>
        <li><a href="#">Northeastern University Student Health Plan (NUSHP)</a></li>
        <li><a href="#">Sports Performance</a></li>
        <li><a href="#">University Health and Counseling Services (UHCS)</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
