<?php $directory_item_counter = 0; ?>
<section class="sub-page-content entry-content" itemprop="articleBody">
<div class="container padding-0 staff-block-container--default-page">

<?php if( get_sub_field('title') ): ?>
<h2 class="text-center"><?php the_sub_field('title'); ?></h2>
<?php endif; ?>

<?php if( get_sub_field('subtitle') ): ?>
<h4 class="col-xs-12 col-sm-10 col-md-8 col-lg-8 margin-0-auto lead text-center"><?php the_sub_field('subtitle'); ?></h4>
<?php endif; ?>

<!--required for spacing for some reason x_x-->
<p></p>

<?php
$config['columnClass'] = 'col-xs-6 col-sm-4 col-lg-3';
$postTypeVar = 'directory';
$args = array(
	'post_type' => 'directory',
	'tax_query' => array(
		array(
			'taxonomy' => 'directory_categories',
			'field'    => 'slug',
			'terms'    => $config['category'],
		),
	),
);
$loop = new WP_Query( $args );
if($loop->post_count == 1){
	$config['columnClass'] = 'col-xs-12';
}
if($loop->post_count == 2){
	$config['columnClass'] = 'col-xs-6 col-sm-4 col-sm-push-2 col-lg-3 col-lg-push-3';
}
if($loop->post_count == 3 || $loop->post_count == 5){
	$config['columnClass'] = 'col-xs-6 col-sm-4 col-lg-4';
}
if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();
$directory_item_counter++;
$config['directory_item_counter'] = $directory_item_counter;
display(
  INCLUDES.'/directory-item.php',
  array(),
  $config
);



endwhile;
endif;
wp_reset_query();
?>
<!--END WP QUERY -->
</div>
<div class = 'container'>
	<?php if(isset($config['isTeaser']) && get_sub_field('button')):?>
		<p class="text-center"><a href="<?php the_sub_field('button'); ?>" class="margin-vertical-2 btn"><?php the_sub_field('button_text'); ?> &raquo;</a></p>
	<?php endif; ?>
</div>
</section>
