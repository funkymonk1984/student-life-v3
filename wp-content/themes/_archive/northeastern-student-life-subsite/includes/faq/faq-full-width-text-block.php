<?php if( get_field('sub_page_full_width_text_block_title') ): ?>
  	<h2 class="text-center">
  		<?php the_field('sub_page_full_width_text_block_title'); ?>
  	</h2>
<?php endif; ?>

<?php if( get_field('sub_page_full_width_text_block_subtitle') ): ?>
  	<h4 class="red col-xs-12 col-sm-10 col-md-8 col-lg-8 margin-0-auto lead text-center">
  		<?php the_field('sub_page_full_width_text_block_subtitle'); ?>
  	</h4>
<?php endif; ?>

<?php if( get_field('sub_page_full_width_text_block_content') ): ?>
  	<p class="text-center">
  		<?php the_field('sub_page_full_width_text_block_content'); ?>
  	</p>
<?php endif; ?>

<?php if( get_field('button_text') ): ?>
		<p class="text-center">
			<a class = 'btn' href = "<?php the_field('button_link'); ?>"><?php the_field('button_text'); ?> &raquo;</a>
		</p>
<?php endif; ?>
