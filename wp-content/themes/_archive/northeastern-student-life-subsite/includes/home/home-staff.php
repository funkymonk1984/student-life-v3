<section class="home-staff container">
	<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">

		<?php if( get_sub_field('home_staff_title') ): ?>
			<h2 class="page-title">
	    		<?php the_sub_field('home_staff_title'); ?>
			</h2>
		<?php endif; ?>

		<?php if( get_sub_field('home_staff_subtitle') ): ?>
			<h3 class="lead text-left">
	    		<?php the_sub_field('home_staff_subtitle'); ?>
	    	</h3>
    	<?php endif; ?>

	</div>
	<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
		<?php if( have_rows('staff','option') ): ?>
		 	<?php while ( have_rows('staff','option') ) : the_row(); ?>
		 		<div class="staff-block-outer col-xs-12 col-sm-4 col-md-4 col-lg-3">
			 		<div class="staff-block">
			 			<div class="image" style="background-image:url('<?php the_sub_field('staff_photo'); ?>');"></div>

			        	<h4 class="text-center">
			        		<?php the_sub_field('staff_first_name'); ?><br />
			        		<span style="font-weight:700;">
			        			<?php the_sub_field('staff_last_name'); ?>
			        		</span>
			        	</h4>

			        </div>
		        </div>
		    <?php endwhile; ?>
		<?php endif; ?>
	</div>
	<div class="container">
		<a href="<?php the_sub_field('home_staff_button'); ?>" class="margin-vertical-2 pull-left btn">See All &raquo;</a>
	</div>
</section>
