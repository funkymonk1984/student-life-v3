<section class="home-image" style="background-image:url('<?php the_sub_field('home_image_hero'); ?>');">
	<div class="overlay">
		
		<?php if( get_sub_field('home_image_text') ): ?>
			<h3 class="handwritten">
				"<?php the_sub_field('home_image_text'); ?>"
			</h3>
		<?php endif; ?>
		
	</div>
</section>