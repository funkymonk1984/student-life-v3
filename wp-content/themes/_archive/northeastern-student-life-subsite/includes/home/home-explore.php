<section class="home-explore container-fluid">
	
	<?php if( get_sub_field('home_explore_title') ): ?>
		<h2 class="page-title text-center">
			<?php the_sub_field('home_explore_title'); ?>
		</h2>
	<?php endif; ?>

	<?php if( get_sub_field('home_explore_subtitle') ): ?>
		<h3 class="lead text-center col-xs-12 col-sm-12 col-md-8 col-lg-8 margin-0-auto">
			<?php the_sub_field('home_explore_subtitle'); ?>
		</h3>
	<?php endif; ?>

	<?php if( have_rows('student_life_organizations', 'option') ): ?>
		<?php while ( have_rows('student_life_organizations', 'option') ) : the_row(); ?>
			<?php if( get_row_layout() == 'student_life_organization_block'): ?>
				
				<div class="student-life-organization-block" style="background-color:<?php the_sub_field('student_life_organization_block_brand'); ?>;">
			        
			        <?php if( get_sub_field('student_life_organization_block_image') ): ?>
				        <div class="image">
				        	<img src="<?php the_sub_field('student_life_organization_block_image');?>">
				        </div>
			        <?php endif; ?>
			        
			        <div class="content container padding-0">
				        
				        <?php if( get_sub_field('student_life_organization_block_title') ): ?>
					        <h3 class="text-center styled-header student-life-organization-block-title">
					        	<?php the_sub_field('student_life_organization_block_title'); ?>
					        </h3>
				        <?php endif; ?>
				       
				       	<?php // $select = get_sub_field_object('student_life_organizations'); ?>
						<?php // echo $select['key']; ?>

				        <?php if( have_rows('student_life_organizations') ): ?>
				        	<ul class="text-left student-life-organization-block-list">
								<?php while ( have_rows('student_life_organizations') ) : the_row(); ?>
							        <li class="">

							        	<a target="_blank" href="<?php the_sub_field('student_life_organization_link'); ?>">
							        		<i class='fa fa-angle-right pull-left' aria-hidden='true'></i>
							        		<?php the_sub_field('student_life_organization_title'); ?>
							        	</a>
							        </li>
							    <?php endwhile; ?>
						    </ul>
						    <div class="clearfix"></div>  
						<?php endif; ?>


						<p class="hidden-xs student-life-organization-block-quote handwritten">
				        	<?php the_sub_field('student_life_organization_block_quote'); ?>
				        	
				        </p>
			        </div>
				</div>
		    <?php endif; ?>
		<?php endwhile; ?>
	<?php endif; ?>
</section>