<section class="sub-page-content entry-content container" itemprop="articleBody">
  <h2 class = 'text-center'>Map of Gender Neutral Bathrooms</h2>
	<div class="margin-vertical-2">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
        <iframe src="https://www.google.com/maps/d/u/0/embed?mid=12TH__Yi5hvouCg1Njat-tIK36JM" width="100%" height="480"></iframe>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
          <p>All gender restrooms are available in several buildings on campus. You can open the drawer to filter by building type</p>
          <p>Please email us at lgbtqa@northeastern.edu to send updates or revisions.</p>
          <p>We also have an excel version of the map available for download.</p>
          <a class = 'btn' href = 'http://dev.em.northeastern.edu/student-life/wp-content/uploads/2017/02/All-Gender-Restroom-List.xlsx' download>DOWNLOAD MAP</a>
      </div>
  </div>
</section>
