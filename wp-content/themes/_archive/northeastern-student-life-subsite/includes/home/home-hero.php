<div class="hidden-xs visible-sm-* visible-md-* visible-lg-* easyhtml5video video-hero" style="position:relative;max-width:1920px;">
		<?php if(get_sub_field('home_video_webm') || get_sub_field('home_video_mov') || get_sub_field('home_video_mp4')):?>
		<!--the video tag below will render the hero video on larger screen sizes-->
		<video muted="muted" loop="loop" autoplay="autoplay" style="width:100%" title="Northeastern unviersity Student Life">
			<?php if( get_sub_field('home_video_webm') ): ?>
				<source src="<?php the_sub_field('home_video_webm'); ?>" type="video/webm" />
			<?php endif; ?>

			<?php if( get_sub_field('home_video_mp4') ): ?>
				<source src="<?php the_sub_field('home_video_mp4'); ?>" type="video/mp4" />
			<?php endif; ?>

			<?php if( get_sub_field('home_video_mov') ): ?>
				<source src="<?php the_sub_field('home_video_mov'); ?>" type="video/mov" />
			<?php endif; ?>
		</video>
		<?php endif; ?>

		<!--the div below will render the hero image on larger screen sizes IF no video exists-->
		<?php if(!get_sub_field('home_video_webm') || !get_sub_field('home_video_mov') || !get_sub_field('home_video_mp4')): ?>
		<div class="visible-xs-* image-hero" style="background-image:url('<?php the_sub_field('home_image_hero'); ?>');"></div>
		<?php endif; ?>


	<div class="overlay">
		<div class="content carousel slide" data-ride="carousel">
			<div class="carousel-inner " role="listbox">

				<?php if( have_rows('home_hero_text') ): ?>
				    <?php while ( have_rows('home_hero_text') ) : the_row(); ?>

						<div class="item">

							<?php if( get_sub_field('home_hero_text_header') ): ?>
						        <h2 class="handwritten text-center">
						        	<?php the_sub_field('home_hero_text_header'); ?>
						        </h2>
					        <?php endif; ?>

					        <?php if( get_sub_field('home_hero_text_subheader') ): ?>
						        <h3 class="handwritten text-center">
						        	<?php the_sub_field('home_hero_text_subheader'); ?>
						        </h3>
					        <?php endif; ?>

				        </div>

				    <?php endwhile; ?>
					<?php else : ?>
				<?php endif; ?>

			</div>
		</div>
	</div>
</div>

<div class="visible-xs-* hidden-sm hidden-md hidden-lg image-hero easyhtml5video video-hero" style="background-image:url('<?php the_sub_field('home_image_hero'); ?>');">
	<div class="overlay">
		<div class="content carousel slide" data-ride="carousel">
			<div class="carousel-inner " role="listbox">

				<?php if( have_rows('home_hero_text') ): ?>
				    <?php while ( have_rows('home_hero_text') ) : the_row(); ?>

						<div class="item">

							<?php if( get_sub_field('home_hero_text_header') ): ?>
						        <h2 class="handwritten text-center">
						        	<?php the_sub_field('home_hero_text_header'); ?>
						        </h2>
					        <?php endif; ?>

					        <?php if( get_sub_field('home_hero_text_subheader') ): ?>
						        <h3 class="handwritten text-center">
						        	<?php the_sub_field('home_hero_text_subheader'); ?>
						        </h3>
					        <?php endif; ?>

				        </div>

				    <?php endwhile; ?>
					<?php else : ?>
				<?php endif; ?>

			</div>
		</div>
	</div>
</div>
