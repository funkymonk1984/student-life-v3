<?php get_field('sub_page_hero_image', 'option'); ?>
<?php if( get_field('sub_page_hero_title', 'option') ): ?>
<div class="hero-unit text-center padding-0" style="background-image:url('<?php the_field('sub_page_hero_image', 'option'); ?>');">
  <div class="content">

  	<?php if( get_field('sub_page_hero_title', 'option') ): ?>
			<h1 class="white">
				<?php the_field('sub_page_hero_title', 'option'); ?>
			</h1>
		<?php endif; ?>

		<?php if( get_field('sub_page_hero_subtitle', 'option') ): ?>
			<p class="lead white hidden-xs hidden-sm">
				<?php the_field('sub_page_hero_subtitle', 'option'); ?>
			</p>
		<?php endif; ?>

	</div>
	<div class="overlay"></div>
</div>
<?php endif; ?>
