<?php if( have_rows('blog_content','option') ): ?>
    <?php while ( have_rows('blog_content','option') ) : the_row(); ?>
        <?php if( get_row_layout() == 'blog_hero' ): ?>
        	
        	<div class="hero-unit container-fluid text-center padding-0" style="background-image:url('<?php the_sub_field('blog_hero_image'); ?>');">
        		<div class="content">
        			
        			<?php if( get_sub_field('blog_hero_title') ): ?>
						<h1 class="white">
							<?php the_sub_field('blog_hero_title'); ?>
						</h1>
					<?php endif; ?>
					
					<?php if( get_sub_field('blog_hero_subtitle') ): ?>
						<p class="lead white hidden-xs hidden-sm">
							<?php the_sub_field('blog_hero_subtitle'); ?>
						</p>
					<?php endif; ?>

				</div>
				<div class="overlay"></div>
			</div>
			
        <?php endif; ?>
    <?php endwhile; ?>
<?php endif; ?>