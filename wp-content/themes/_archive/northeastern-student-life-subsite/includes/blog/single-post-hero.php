<?php if( have_rows('single_post_content') ): ?>
    <?php while ( have_rows('single_post_content') ) : the_row(); ?>
        <?php if( get_row_layout() == 'single_post_hero' ): ?>
        	<div class="hero-unit container-fluid text-center padding-0" style="background-image:url('<?php the_sub_field('single_post_hero_image'); ?>');">
        		<div class="content">
        			<?php if( get_sub_field('single_post_hero_title') ): ?>
						<h1 class="white">
							<?php the_sub_field('single_post_hero_title'); ?>
						</h1>
					<?php endif; ?>
					<?php if( get_sub_field('single_post_hero_subtitle') ): ?>
						<p class="lead white">
							<?php the_sub_field('single_post_hero_subtitle'); ?>
						</p>
					<?php endif; ?>
				</div>
				<div class="overlay"></div>
			</div>
        <?php endif; ?>
    <?php endwhile; ?>
<?php else : ?>
<?php endif; ?>