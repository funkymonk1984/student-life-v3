<!--RELATED PAGES-->

<?php
if(!isset($config['show_related_pages'])|| $config['show_related_pages'] != false){
include (INCLUDES."/related-pages.php");
}
?>
			<footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
				<!-- <div class="footer-logo container-fluid padding-0">
					<div class="container">
						<img alt="Northeastern Logo" src="<?php the_field('footer_logo', 'option'); ?>">
					</div>
				</div> -->
				<div id="inner-footer" class="container-fluid">
					<div class="container-fluid footer-row">
						<div class="footer-widget footer-logo col-xs-12 col-sm-12 col-md-3 col-lg-4">
							<img src = "<?php the_field('footer_organization_logo', 'option');?>" alt = 'logo'>
							<?php if ( is_active_sidebar( 'footer-widget-1' ) ) : ?>
								<?php dynamic_sidebar( 'footer-widget-1' ); ?>
							<?php endif; ?>
						</div>
						<div class="footer-widget col-xs-12 col-sm-12 col-md-3 col-lg-4">
							<?php if ( is_active_sidebar( 'footer-widget-2' ) ) : ?>
								<?php dynamic_sidebar( 'footer-widget-2' ); ?>
							<?php endif; ?>
						</div>
						<div class="footer-widget col-xs-12 col-sm-12 col-md-3 col-lg-4">
							<?php //the_field('footer_logo', 'option'); ?><!--not-hardcoded footer logo code-->
							<!--<a href = 'http://studentlife.northeastern.edu'><img class="center-on-mobile" alt="Student Life Logo" src="<?//php echo IMAGES . 'student-life-logo.png'; ?>"></a>-->
							<div class = 'widget'><h4 class="widgettitle">Explore Student Life</h4></div>
							<!--TODO replace this with global super header-->
							<?php include (INCLUDES."/division-of-student-affairs-navigation.php");?>
							<div class="widget">
								<?php if( have_rows('social', 'option') ): ?>
									<h4 class="widgettitle">Social</h4>
									<?php while ( have_rows('social','option') ) : the_row(); ?>
										<div class="social row pull-left">
											<a class="pull-left" target="_blank" href="<?php the_sub_field('social_account_link'); ; ?>">
														<i class="fa <?php the_sub_field('social_account_icon'); ; ?>" aria-hidden="true"></i>
													</a>
												</div>
										<?php endwhile; ?>
								<?php else : ?>
								<?php endif; ?>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="universal-footer">
					<div class="wrapper container padding-0">
						<div class="left col-sm-12 col-md-4 col-lg-4">
							<a class="logo" href="http://www.northeastern.edu"></a>
						</div>
						<div class="right hidden-xs hidden-sm hidden-md col-sm-12 col-md-8 col-lg-8">
							<ul class="info">
								<li>
									<a href="http://myneu.neu.edu/cp/home/displaylogin" onclick="that=this;_gaq.push(['_trackEvent','outbound','click',that.href]);setTimeout(function() { location.href=that.href }, 200);return false;">MyNEU</a>
								</li>
								<li>
									<a href="http://www.northeastern.edu/findfacultystaff" onclick="that=this;_gaq.push(['_trackEvent','outbound','click',that.href]);setTimeout(function() { location.href=that.href }, 200);return false;">Find Faculty &amp; Staff</a>
								</li>
								<li>
									<a href="http://www.northeastern.edu/neuhome/adminlinks/findaz.html" onclick="that=this;_gaq.push(['_trackEvent','outbound','click',that.href]);setTimeout(function() { location.href=that.href }, 200);return false;">Find A-Z</a>
								</li>
								<li>
									<a href="http://www.northeastern.edu/emergency/index.html" onclick="that=this;_gaq.push(['_trackEvent','outbound','click',that.href]);setTimeout(function() { location.href=that.href }, 200);return false;">Emergency Information</a>
								</li>
								<li>
									<a href="http://www.northeastern.edu/search/index.html" onclick="that=this;_gaq.push(['_trackEvent','outbound','click',that.href]);setTimeout(function() { location.href=that.href }, 200);return false;">Search</a>
								</li>
								<li>
									<a href="http://assistive.usablenet.com/tt/http://www.northeastern.edu/index.html" onclick="that=this;_gaq.push(['_trackEvent','outbound','click',that.href]);setTimeout(function() { location.href=that.href }, 200);return false;">Text Only</a>
								</li>
								<li>
									<a href="http://www.northeastern.edu/privacy/index.html" onclick="that=this;_gaq.push(['_trackEvent','outbound','click',that.href]);setTimeout(function() { location.href=that.href }, 200);return false;">Privacy</a>
								</li>

							</ul>
							<div class="clearfix"></div>
							<p class="contact">360 Huntington Ave., Boston, Massachusetts 02115 · 617.373.2000 · TTY 617.373.3768<br> © <span class="the-year"><?php echo date("Y"); ?></span>&nbsp;Northeastern University</p>
						</div>
						<div class="clear"></div>
					</div>
				</div>

				<!-- <div class="copyright container-fluid padding-0">
					<p class="text-center source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>.</p>
				</div> -->

			</footer>
		</div>
		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>
	</body>
</html> <!-- end of site. what a ride! -->
