<?php if(get_field('newsletter_embed_code', 'option')): ?>
<section style="background: #009097;" class="sub-page-content entry-content" itemprop="articleBody">
	<div class="container">
    <div class = 'col-xs-12 col-sm-10 col-md-8 col-md-push-2'>
      <?php if(get_field('newsletter_title', 'option')): ?>
          <h2 class="white text-center"><?php the_field('newsletter_title', 'option'); ?></h2>
      <?php endif; ?>

      <?php if(get_field('newsletter_subtitle', 'option')): ?>
          <h4 class="white margin-0-auto lead text-center">
            <?php the_field('newsletter_subtitle', 'option'); ?>
          </h4>
      <?php endif; ?>

      <?php if(get_field('newsletter_embed_code', 'option')): ?>
          <?php the_field('newsletter_embed_code', 'option'); ?>
      <?php endif; ?>
    </div>
	</div>
</section>
<?php endif;?>
