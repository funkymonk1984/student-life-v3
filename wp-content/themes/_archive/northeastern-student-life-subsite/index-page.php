<?php
/*
 Template Name: Index Page
*/
?>
<?php get_header(); ?>

<?php
//DISPLAY PAGE VIEW
display(
  INCLUDES."/sub-page/page.php",
  array(),
  array()
);
?>
<?php
display(
  INCLUDES."/footer/footer.php",
  array(),
  array('show_related_pages' => false)
);
?>
