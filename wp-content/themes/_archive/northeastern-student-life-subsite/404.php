<?php get_header(); ?>
	<div id="content">
		<div id="inner-content" class="container">
			<main id="main" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
				<article id="post-not-found" class="hentry">
					<header class="article-header">
						<h1 class="text-center"><?php _e( 'Ruh Roh!', 'bonestheme' ); ?></h1>
					</header>
					<section class="entry-content">
						<p class="text-center">
							You seem to have stumbled across a page that has gone missing! Give it another shot, or reach out to our team for some help by emailing <a href = 'mailto:lgbtqa@northeastern.edu'>lgbtqa@northeastern.edu</a>
						</p>
					</section>
					<!-- <section class="search text-center">
						<p class="text-center">
							<?php get_search_form(); ?>
						</p>
					</section> -->
					<footer class="article-footer">
						<p class="text-center">
							<?php _e( '404', 'bonestheme' ); ?>
						</p>
					</footer>
				</article>
			</main>
		</div>
	</div>
<?php get_footer(); ?>
