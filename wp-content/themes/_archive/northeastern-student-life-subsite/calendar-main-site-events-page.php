<?php
/*
 Template Name: Calendar: Main Site Events Page
*/
?>
<?php get_header(); ?>
	<div id="content">

		<?php display("includes/sub-page/sub-page-content-hero.php");?>

		<div id="inner-content" class="container-fluid">

			<main id="main" class="padding-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
        <?php echo do_shortcode( '[tribe_events]' ); ?>
			</main>
		</div>
		<?php// get_sidebar('secondary-sidebar'); ?>
	</div>
<?php get_footer(); ?>
