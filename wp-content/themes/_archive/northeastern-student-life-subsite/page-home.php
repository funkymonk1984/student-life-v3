<?php
/*
 Template Name: Home
*/
?>

<?php get_header(); ?>

	<div id="content">
		<div class="hero">

			<?php if( have_rows('home_content') ): ?>
				<?php while ( have_rows('home_content') ) : the_row(); ?>
					<?php if( get_row_layout() == 'home_hero' ): ?>

						<?php include('includes/home/home-hero.php'); ?>

					<?php endif; ?>
				<?php endwhile; ?>
			<?php endif; ?>

			<div class="hero-inner container-fluid padding-0">
				<?php include ('includes/home/home-events.php'); ?>
			</div>
		</div>

		<div id="inner-content" class="container-fluid padding-0">
			<main id="main" class="padding-0 col-xs-12 col-sm-12 col-md-12 col-lg-12 container" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php if( have_rows('home_content') ): ?>
            <!--<div id='eventsIframe'></div>--><!--Pym will generate an iframe here-->
					    <?php while ( have_rows('home_content') ) : the_row(); ?>

									<?php if( get_row_layout() == 'sub_page_full_width_text_block' ): ?>

										<?php include('includes/sub-page/full-width-text-block.php'); ?>

					        <?php elseif( get_row_layout() == 'home_explore' ): ?>

					        	<?php include ('includes/home/home-explore.php'); ?>

					        <?php elseif( get_row_layout() == 'home_image' ): ?>

					        	<?php include ('includes/home/home-image.php'); ?>

					        <?php elseif( get_row_layout() == 'home_staff' ): ?>

										<?php
					          display(
					            INCLUDES.'/directory.php',
					            array(),
					            array(
												'category' => 'staff',
												'isTeaser' => true
												)
					          );
					          ?>

									<?php elseif( get_row_layout() == 'embed' ): ?>

					        	<?php include ('includes/sub-page/embed-and-text.php'); ?>
					        <?php endif; ?>
					    <?php endwhile; ?>
					<?php endif; ?>
				<?php endwhile; else : ?>
				<?php endif; ?>

			</main>

			<?php // get_sidebar(); ?>

		</div>
	</div>
	<script>
		/*
	  jQuery(document).ready(function(){
	    var pymParent = new pym.Parent('eventsIframe', "<?php// the_field('events_feed'); ?>", {});
	  });
		*/
	</script>
<?php
display(
  INCLUDES."/footer/footer.php",
  array(),
  array('show_related_pages' => false)
);
?>
