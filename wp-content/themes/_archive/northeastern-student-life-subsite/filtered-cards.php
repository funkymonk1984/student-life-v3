<?php
/*
 Template Name: Filtered Cards
*/
?>


<?php get_header(); ?>
	<div id="content">

		<?php include ("includes/sub-page/sub-page-content-hero.php");?>

		<div id="inner-content" class="container-fluid padding-0">
			<main id="main" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php if( have_rows('explore_page_content') ): ?>
					    <?php while ( have_rows('explore_page_content') ) : the_row(); ?>
					        <?php if( get_row_layout() == 'explore_featured_organization' ): ?>

					        	<?php include('explore/explore-featured-organization.php'); ?>

					        <?php endif; ?>
					    <?php endwhile; ?>
					<?php endif; ?>

					<?php include (INCLUDES.'/resources/resource-filter-buttons.php');?>
					<div class="clearfix"></div>

					<?php include (INCLUDES.'/resources/resource-category-introductions.php');?>

					<section class="student-life-organization container-fluid padding-0">

						<!--TODO CPT loop goes here-->
						<?php $j = 0?>
						<?php $loop = new WP_Query( array('post_type' => 'resources') );
						if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
						<?php $j++?>
						<div class="<?php echo $category; ?>-wrapper">
							 <?php
							 include('includes/explore/format-organization-title.php');
							 $color = get_field('resources_category_brand');
							 $image = get_field('resources_category_image');
							 ?>
							 <!--DISPLAY RESOURCE ITEMS TO SCREEN-->
							 <div class="content <?php echo $category; ?>-content padding-0">
								 <?php if( have_rows('resource_item') ): $i=0; ?>
									 <?php while ( have_rows('resource_item') ) : the_row(); $i++; ?>
										 <?php include (INCLUDES.'/resources/resource-item.php');?>
									 <?php endwhile; ?>
								 <?php endif; ?>
							 </div>
							 <div class="category-background-image <?php echo $category; ?>-background-image">
								 <img src="<?php the_field('resources_category_image');?>">
							 </div>
						 </div>
						 <!--END DISPLAY RESOURCE ITEMS TO SCREEN-->

							<?php $color = get_field('resources_category_brand'); ?>
							<?php if( have_rows('resource_item') ): $i=0; ?>
							<?php while ( have_rows('resource_item') ) : the_row(); $i++; ?>
									<?php include (INCLUDES.'/resources/resource-item--modal.php');?>
							<?php endwhile; endif; ?>

						<?php endwhile; endif; wp_reset_query(); ?>

					</section>
				<?php endwhile; else : ?>
				<?php endif; ?>
			</main>
		</div>
	</div>
<?php
display(
  INCLUDES."/footer/footer.php",
  array(),
  array('show_related_pages' => false)
);
?>
