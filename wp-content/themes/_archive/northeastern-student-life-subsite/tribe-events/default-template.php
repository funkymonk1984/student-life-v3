<?php
/**
 * Default Events Template
 * This file is the basic wrapper template for all the views if 'Default Events Template'
 * is selected in Events -> Settings -> Template -> Events Template.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/default-template.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

get_header();
?>
<div id='header-dest'></div><!--jQuery will load the subsite header here-->
<div id='hero-dest'></div><!--jQuery will load the subsite header here-->
<div id="tribe-events-pg-template" class='get-connected'>
	<?php tribe_events_before_html(); ?>
	<?php tribe_get_view(); ?>
	<?php tribe_events_after_html(); ?>
</div> <!-- #tribe-events-pg-template -->
<div id='footer-dest'></div><!--jQuery will load the subsite footer here-->

<?php
get_footer();
?>
