<?php
/*
 Template Name: News and Events
*/
?>
<?php get_header(); ?>
	<div id="content" class = 'blog'>

		<?php display("includes/sub-page/sub-page-content-hero.php");?>

		<div id="inner-content" class="container">
			<main id="main" class="col-xs-12 col-sm-12 col-md-8 col-md-push-2" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
				<h1>Latest Post</h1>
        <?php
        $temp = $wp_query; $wp_query= null;
    		$wp_query = new WP_Query(); $wp_query->query('posts_per_page=1');
        ?>
				<?php if ($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class( '' ); ?> role="article">

					<header class="article-header">
						<div class="row">
							<?php the_post_thumbnail('full'); ?>
						</div>
						<h1 class="h2 entry-title">
							<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
						</h1>
					</header>
					<section style="background:transparent;" class="entry-content">
						<?php the_excerpt(); ?>
					</section>
				</article>

				<?php endwhile; ?>
				<?php else : ?>
				<?php endif; ?>
				<h1>Upcoming Events</h1>
				<?php
				wp_reset_query();
				$args = array( 'post_type' => 'event', 'posts_per_page' => 10 );
				$loop = new WP_Query( $args );
				?>
				<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
						<article role="article">
							<header class="article-header">
								<!--TODO might just be able to scrap row -->
								<!--
								<div class="row">
									<?php //TODO put in the event thumbnail? the_post_thumbnail('full'); ?>
								</div>
								-->
								<h1 class="h2 entry-title">
									<a href="<?php the_field('link_to_event_in_neu_calendar');?>" rel="bookmark" title=""><?php the_title(); ?></a>
								</h1>
							</header>
							<section style="background:transparent;" class="entry-content">
								<ul>
									<li><?php the_field('date');?></li>
									<li><?php the_field('location');?></li>
								</ul>
								<?php the_field('teaser_text');?>
								<a href = '<?php the_field('link_to_event_in_neu_calendar');?>'>View Event on NEU Calendar &raquo;</a>
							</section>
						</article>
				<?php endwhile; ?>
			</main>
		</div>
	</div>
<?php get_footer(); ?>
