<?php
/*
 Template Name: Subdomain Home
*/
?>

<?php get_header(); ?>

	<?php include ("includes/sub-domain/sub-domain-style.php");?>

	<div id="content">

		<?php include ("includes/sub-domain/sub-domain-content-hero.php");?>

		<div id="inner-content" class="container-fluid padding-0">
			<main id="main" class="padding-0 col-xs-12 col-sm-12 col-md-12 col-lg-12" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
				
				<?php include ("includes/sub-domain/sub-domain-content.php");?>

			</main>
			<?php // get_sidebar(); ?>
		</div>
	</div>
<?php get_footer(); ?>
