<?php
/*
 Template Name: About
*/
?>

<?php get_header(); ?>
<div id="content">

<?php include ("includes/sub-page/sub-page-content-hero.php");?>

<div id="inner-content" class="container-fluid padding-0">
<main id="main" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<?php if( have_rows('about_page_content') ): ?>
	<?php while ( have_rows('about_page_content') ) : the_row(); ?>
	<?php if( get_row_layout() == 'about_staff_block' ): ?>

		<section class="about-page-content container-fluid" itemprop="articleBody">
			<div class="container">
				<?php if( get_sub_field('about_staff_block_title') ): ?>
				<h2 class="text-center"><?php the_sub_field('about_staff_block_title'); ?></h2>
				<?php endif; ?>
				<?php if( get_sub_field('about_staff_block_subtitle') ): ?>
				<h4 class="red col-xs-12 col-sm-10 col-md-8 col-lg-8 margin-0-auto lead text-center"><?php the_sub_field('about_staff_block_subtitle'); ?></h4>
				<?php endif; ?>
			</div>
		</section>

	<?php endif;?>
	<?php endwhile;?>
	<?php endif;?>

	<?php include ("includes/about/staff.php");?>
	<?php include ("includes/sub-page-content.php");?>

<?php endwhile; else : ?>
<?php endif; ?>

</main>
</div><!--end innner-content-->
</div><!--end content-->
<?php get_footer(); ?>
