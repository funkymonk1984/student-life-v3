<!doctype html>

<!--[if lt IE 7]>
	<html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7">
<![endif]-->

<!--[if (IE 7)&!(IEMobile)]>
	<html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8">
<![endif]-->

<!--[if (IE 8)&!(IEMobile)]>
	<html <?php language_attributes(); ?> class="no-js lt-ie9">
<![endif]-->

<!--[if gt IE 8]>
	<!--> <html <?php language_attributes(); ?> class="no-js">
<!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // force Internet Explorer to use the latest rendering engine available ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php wp_title(''); ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>

		<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-touch-icon.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
            <meta name="theme-color" content="#121212">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php // Fonts ?>
  		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
  		<link href="https://fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet">
  		<link href="https://fonts.googleapis.com/css?family=Gochi+Hand" rel="stylesheet">
  		<?php // End Fonts ?>

		<?php // wordpress head functions ?>
		<?php wp_head(); ?>
		<?php // end of wordpress head ?>

		<?php // drop Google Analytics Here ?>
		<?php // end analytics ?>

	</head>

	<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">

	<!-- Google Tag Manager -->
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WGQLLJ"
	height="0" width="0" style="display:none;visibility:hidden"></iframe>
	</noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','globalDataLayer','GTM-WGQLLJ');</script>
	<!-- End Google Tag Manager -->

		<div id="container">
			<header class="header" role="banner" itemscope itemtype="http://schema.org/WPHeader">
				<?php
				/**
				 * Student Life Global Drawer
				 */
				?>

				<div class="drawer container-fluid padding-0">
					<div class="drawer-inner padding-0">
						<div class="drawer-header padding-0">
							<div class="drawer-header-inner container">
								<nav class="navbar navbar-default">
									<div class="navbar-header">
										<a class="navbar-brand" href="http://www.northeastern.edu/" target="_blank">
											

											<?php if( get_field('northeastern_logo','option') ): ?>
												<?php the_field('northeastern_logo', 'option'); ?>
											<?php endif; ?>
											
											<img alt="Northeastern Logo" src="<?php echo IMAGES . 'northeastern-logo-large.png'; ?>">
										</a>
									</div>
									<div class = 'student-affairs-menu-bar'>
										<div class="drawer-handle">
	                    					<button style="display:block;" type="button" class="pull-right navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
												<span class="sr-only">Toggle navigation</span>
												<span class="icon-bar"></span>
												<span class="icon-bar"></span>
												<span class="icon-bar"></span>
											</button>
											<span style="display:none;" class="navbar-toggle glyphicon glyphicon-remove" aria-hidden="true"></span>
										</div>
										<span class="division-of-student-affairs">division of student affairs</span>
									</div>
								</nav>
							</div>
						</div>
						<div class="drawer-content container-fluid padding-0">
							<div class="drawer-content-inner container-fluid padding-0">
								<div class="logo container-fluid padding-0" style="background:#251F1F;">
									<div class="container padding-0">
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

											<?php
											/**
											 * Student Life Logo
											 * This is still being finalized, hence being commented out
											 * Managed through ACF "Options" page
											 */
											?>

											<!--<a class="center-on-mobile" href="<?php echo home_url(); ?>">
												<img class="center-on-mobile logo" alt="Northeastern Student Life Logo" src="<?php the_field('drawer_logo', 'option'); ?>">
											</a>-->
										</div>
										<div class="social hidden-xs hidden-sm col-md-6 col-lg-">

											<?php
											/**
											 * Student Life Social Accounts
											 * Managed through ACF "Options" page
											 */
											?>

											<?php if( have_rows('social', 'option') ): ?>
												<?php while ( have_rows('social','option') ) : the_row(); ?>
											 		<a class="pull-right" target="_blank" href="<?php the_sub_field('social_account_link'); ; ?>">
											        	<i class="fa <?php the_sub_field('social_account_icon'); ; ?>" aria-hidden="true"></i>
											        </a>
											    <?php endwhile; ?>
											<?php else : ?>
											<?php endif; ?>
										</div>
									</div>
								</div>
								<?php include_once( INCLUDES . '/division-of-student-affairs-navigation.php' );?>
							</div>
						</div>
					</div>
				</div>

				<?php
				/**
				 * Student Life Internal Nav
				 */
				?>


				<nav id = 'inner-header' class="navbar navbar-default">
				  <div class="container-fluid">
				    <!-- Brand and toggle get grouped for better mobile display -->
				    <div class="navbar-header">
				      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
				      <a class="navbar-brand" href="#">Brand</a>
				    </div>

				    <!-- Collect the nav links, forms, and other content for toggling -->
				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<?php wp_nav_menu(array(
			          'container' => false,
			          'container_class' => 'menu',
			          'menu' => __( 'The Main Menu', 'bonestheme' ),
			          'menu_class' => 'nav top-nav navbar-nav navbar-right',
			          'theme_location' => 'main-nav',
			          'before' => '',
			          'after' => '',
			          'link_before' => '',
			          'link_after' => '',
			          'depth' => 0,
			          'fallback_cb' => ''
			        )); ?>
				    </div><!-- /.navbar-collapse -->
				  </div><!-- /.container-fluid -->
				</nav>

			</header>
