<?php
/*
 Template Name: Calendar: Sub Site Events Page
*/
?>

<?php get_header(); ?>
	<div id="content">

  <?php display("includes/sub-page/sub-page-content-hero.php");?>

		<div id="inner-content" class="container-fluid">
			<main id="main" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
				<?php if( get_field('events_feed') ): ?>
					<div id='eventsIframe'></div><!--Pym will generate an iframe here-->
				<?php endif; ?>
			</main>
			<?php // get_sidebar(); ?>
		</div>
	</div>

<script type="text/javascript" src="https://pym.nprapps.org/pym.v1.min.js"></script>
<script>
  <?php
    $current_blog = get_blog_details(  get_current_blog_id(), 'path');
    $current_blog_path = trim($current_blog->path, "/");
  ?>
    var currentSlug = "<?php echo $current_blog_path; ?>"
  jQuery(document).ready(function(){
    var pymParent = new pym.Parent('eventsIframe', "<?php the_field('events_feed'); ?>?refsite=" + currentSlug, {});
  });
</script>
<?php
display(
  INCLUDES."/footer/footer.php",
  array(),
  array('show_related_pages' => false)
);
?>
