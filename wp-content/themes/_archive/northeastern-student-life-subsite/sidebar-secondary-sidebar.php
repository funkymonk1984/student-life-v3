<div id="sidebar2" class="container-fluid" role="complementary">
<?php if ( is_active_sidebar( 'secondary-sidebar' ) ) : ?>
<?php
$currentPageID = get_the_ID();
$my_wp_query = new WP_Query();
$all_wp_pages = $my_wp_query->query(array('post_type' => 'page'));
$children = get_page_children( wp_get_post_parent_id($post->ID), $all_wp_pages );
$columnPush = '';
if(count($children) == 2){
  $columnPush = 'col-md-push-4';
}
if(count($children) == 3){
  $columnPush = 'col-md-push-2';
}
?>

<h2 class="text-center">Explore More</h2>

<section class="student-life-organization student-life-organization--sidebar container-fluid padding-0">
<div class="<?php echo $category; ?>-wrapper">

<?php
//$color = get_sub_field('student_life_organization_block_brand');
//$image = get_sub_field('student_life_organization_image');
?>

<div class="content <?php echo $category; ?>-content padding-0">

<?php foreach ($children as $child): ?>
  <?php if($child->ID != $currentPageID):?>
    <?php $child_wp_query = new WP_Query( array( 'page_id' => $child->ID ) ); ?>
    <?php if ($child_wp_query->have_posts()) : while ($child_wp_query->have_posts()) : $child_wp_query->the_post(); ?>

      <!--START LOOPING THROUGH PAGE CONTENT-->
      <?php if( have_rows('sub_page_content') ): ?>
      <?php while ( have_rows('sub_page_content') ) : the_row(); ?>
      <?php if( get_row_layout() == 'sub_page_hero' ): ?>

        <?php if( get_sub_field('sub_page_hero_image') ):?>
        <div style="border-top:thick solid <?php //echo $color; ?>; background-image:url('<?php the_sub_field('sub_page_hero_image'); ?>');" class="block col-xs-12 col-sm-6 <?php echo $columnPush; ?> col-md-4 padding-0">
        <?php else: ?>
        <div style="background-color:<?php //echo $color; ?>;" class="block col-xs-12 col-sm-6 <?php echo $columnPush; ?> col-md-4 padding-0">
        <?php endif;?><!--render background image if one exists-->
          <!--TODO I think this is where the link to the page would go-->
          <a href="<?php the_permalink();?>">
            <h5 class="text-center styled-header">
              <!--TODO page title will go here-->
              <?php the_sub_field('sub_page_hero_title'); ?>
            </h5>
          </a>
          <a href="" class="overlay"></a>
        </div><!--END OF BLOCK BLOCK-->

      <?php endif; ?>
      <?php endwhile; ?>
      <?php endif; ?>
      <!--END LOOPING THROUGH PAGE CONTENT -->

    <?php endwhile; else : ?>
    <?php endif; ?>
  <?php endif; ?>

<?php endforeach; ?>

<?php else : ?>
<?php
  /*
   * This content shows up if there are no widgets defined in the backend.
  */
?>
<div class="no-widgets">
  <p><?php _e( 'This is a widget ready area. Add some and they will appear here.', 'bonestheme' );  ?></p>
</div>
<?php endif; ?>
</div>
<div class="category-background-image <?php echo $category; ?>-background-image">
  <img src="<?php the_sub_field('student_life_organization_block_image');?>">
</div><!--end of category-background-image-->
</div><!--END OF WRAPPER-->
</section>
</div><!--END OF SIDEBAR2-->
