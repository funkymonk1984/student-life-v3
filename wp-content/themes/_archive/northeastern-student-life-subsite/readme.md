# Student life Documentation

## Initial sub site setup

*The CIE instance is currently the most up to date blank setup*

### To create a new sub site

1. Go to the network level
2. Sites > add new site

### Set up a user for the client

For whatever reason the password email functionality doesn't work on this installation.
You'll have to create a new user, reset their password, and email that to them.

1. Go to the network level
2. Add a new user
3. Sites > The new sub site > add the new user to that site.

### Importing example content

There is a .zip file called `student-life-data-backups.zip` on the root level of the theme folder.

It contains 4 files that need to be imported into wordpress.

An explanation of how to import these files follows.

_Make sure you import the field student-life-ACF-field-groups.json first!_

#### 1. student-life-content.xml

This is the bulk of the content, navigation, footer, etc.

##### To import:

Use the standard WP import tool Tools > Import >  Wordpress

##### On media...
Be sure to check off Download Attachments box, it might take a little while, but it will import the media.
This doesn't _always_ work correctly, unfortunately.
Importing placeholder images is an issue currently.

##### Delete all the ACF field groups after you import the XML file!

This XML file contains dysfunctional ACF field groups, they'll be replaced with working ones in the next file.

#### 2. student-life-ACF-field-groups.json

Contains the ACF field groups.

##### To import:

Use the ACF importer:

Custom Fields > Tools > Import Field Groups

#### 3. student-life-options.json

Contains the data on the options pages (Everything in General: Staff, Filter Cards, etc)

Use the Options Importer Tools > Import >  Wordpress

<span style="color:red">IMPORTANT: When you import **DO NOT** select override existing options, you will **BREAK THE WP INSTALLATION**.</span>

#### 4. student-life-widgets.wie

Contains the data for widgets (In the footer and sidebar)

Use Widget Importer & Exporter - Tools > Widget Importer & Exporter

### Events

To set up the events for a new site, you a few things.

#### Create a category for the sub site within Tribe Calendar

From Admin Panel > Events > Event Categories > Add New Event Category

#### Create a page called _Site Name Events Teaser_

* Set the parent to Sub Site Events Teasers.
* Set the template to Calendar: Iframe Events Teaser.
* In the _Events Calendar Categories field_ - select the category for the subsite.

#### Create a page called _Site Name Events Page Iframe_

This page’s URL will get passed into the _events iframe partial URL_ field on the events page of a sub site.

* Set the parent to Sub Site Event Pages.
* Set the template to Calendar: Iframe Events Page.
* In the _Events Calendar Categories field_ - select the category for the subsite.

### Misc set up

1. Select the theme: `northeastern-student-life-subsite theme` (though eventually the sites will all use the same theme).
2. Set menu locations.
3. Set the front and blog pages in reading.
4. Set brand color in options > general.
5. Set title in options > super header (you have to uncheck use image for text).
6. Set the blog hero in options > blog.
7. The events page *must* be a top level page and be titled *events*, if you do not do this, the event single pages will not have a hero image.

## Custom Post Types

### Creating a new CPT

[This vimeo video](https://vimeo.com/32983918) does a good job of outlining how to make a new CPT.

Bones, boilerplate on which the theme is built, has a template for CPTs

#### Within `northeastern-student-life/library`

* `custom-post-type-example.php` - has the code to create a custom post with full functionality.
* `custom-post-type.php` - where you can create custom post types

#### The steps

1. Cut and paste `custom-post-type-example.php` and rename it after the CPT
2. Find and replace the name of the CPT - instructions to follow
3. Include it in functions.php around line 33.
4. Rename the function

## Templates

### News and Events

There are 2 templates depending on what an organization has

* news-and-events.php - an index that with pull in blog posts and tribe events
* blog.php - the blog.

## Helper functions

### The `display()` function

#### Description

You can call modules as functions, and not just partials.
This let’s you do some pretty cool stuff! Modifying partials to suit their context.

#### Usage

`display($template_path, $fields, config)`

#### Parameters

##### $template_path

(*string*) (*required*) The template path is absolute - _not_ relative, use the `INCLUDES` constant.

##### $fields

(*array*) (*optional*) You can use this to pass in content, tbqh `display()` comes from another project where that parameter was needed, I thought it might get mileage in this project, but it didn’t. Removing the parameter is more trouble than it is worth right now.

##### $config

(*array*) (*optional*) This is an associated array you can use to modify the partial you are calling. `‘Has_modal’ => false; for example.`

#### Example: Calling a partial with display

```
display(
  INCLUDES.'/directory.php',
  array(),
  array('category' => 'other-directory')
);
```

#### Example: Using `$config` within a partial to modify the partial

```
$backgroundColor = 'red';

if(isset($config['backgroundColor'])){
  $backgroundColor = $config['backgroundColor'];
}

<div class = '<?php echo $backgroundColor; ?>'>
```

#### On scope and `$config`
If you are going to use `display()` within a partial called by `display()`,
you must pass the `$config` variable to it, if you would like the `$config` var to be available to that inner partial.

You can also push elements onto `$config` before you pass it into the inner partial, if you need to add additional configuration parameters

##### Example
```
display(
  INCLUDES.'partial-folder/inner-partial.php',
  array(),
  $config //the $config param of you passed into the outer partial
);
```

## The tribe calendar

### The gist

The events portion of the Student Life utilizes the [_Tribe Calendar plugin._](https://theeventscalendar.com/knowledgebase/) All events are entered and housed on the main student life site. Pym.js is used to render those event views on sub sites, with dynamically resized iframes.

### Sub site partials

Those iframes link to pages on the main site, that are essentially partials, housing *only* the event component. `load()` is used to pull in the header, hero, and footer from the subsite on which the event lives. Tribe event views are pivoted automatically using a query string and some conditional statements.

### Templates

#### On main site

##### Calendar-iframe-events-teaser.php

Will render the tribe list widget (not be be confused with the tribe list view!) as a "partial" to be used in an iframe on the sub site home page.

##### Calendar-iframe-events-page.php

Will render the Tribe Calendar in list view as a standalone partial to be called on a subsite with pym.js

#### On sub site

##### Calendar-sub-site-events-page.php

Use this template for the events page on subsites

##### Calendar-main-site-events-page.php

Use this template for the events page on the main site

### How to alter the tribe partials

There is a folder on the top level of the theme called `tribe-events`
Follow [this guide](https://theeventscalendar.com/knowledgebase/themers-guide/) to override the various tribe templates.
Most of what you would need to override is already present in this folder.

### Pivoting color and _see all...link_ on `slref`

#### `studentLifeDictionary`

This is a JSON object that is the current `slref` is checked against.
Each one of its objects has an `organizations` property, an array of strings.
It's functional, but it's only really set up to work for LGBTQA,
because that's the only site we have thus far.
Nomenclature for this will have to be determined later.

#### `setBodyClass(currentOrganization)` it's not really working

This function is used _set the body class_ of event views and event pages.
unfortunately it isn't fully serving its intended purpose at the moment.

I had to pull this function outside of `jQuery(document).ready(function($) {`
To make it available for usage in `calendar-iframe-events-page.php` and `calendar-iframe-events-teaser.php`,
by passing in `$category->slug` on those pages, rather than passing in `referralSiteQuery` like in `scripts.js`

These pages cannot pivot on based on `slref`, because they are loaded in as iframes and wouldn't have knowledge of that.

However, this error happens on line 24 of `scripts.js`: `$ is not a function`.
Probably because `setBodyClass()` is defined outside of `jQuery(document).ready(function($) {`
I _think_ using the `context` parameter for the jQuery object might help `jQuery( selector [, context ] )`, but I'm unfamiliar with this.
I think this is going to lead down a scoping rabbit hole that I'm not really equipped to deal with as I'm leaving in a day at the time of me writing this!
